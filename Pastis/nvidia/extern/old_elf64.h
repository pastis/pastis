/*
 *	interface of old_elf module specialized for nvidia
 *
 *	This file is part of OTAWA
 *	Copyright (c) 2008, IRIT UPS.
 *
 *	GLISS is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	GLISS is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with OTAWA; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef GLISS_OLD_ELF_H
#define GLISS_OLD_ELF_H

#include "grt.h"
#include "mem.h"
#include "api.h"

#if defined(__cplusplus)
    extern  "C" {
#endif

#define GLISS_LOADER_STATE
#define GLISS_LOADER_INIT(s)
#define GLISS_LOADER_DESTROY(s)

#define CODE_BASE_ADDRESS	0x00000000ULL
#define DATA_BASE_ADDRESS	0x80000000ULL

/* nvidia_loader_t type */
typedef struct nvidia_loader_t nvidia_loader_t;

/* ELF definitions */


#define EI_MAG0 0	 /* File identification byte 0 index */
#define ELFMAG0 0x7f /* Magic number byte 0 */

#define EI_MAG1 1	/* File identification byte 1 index */
#define ELFMAG1 'E' /* Magic number byte 1 */

#define EI_MAG2 2	/* File identification byte 2 index */
#define ELFMAG2 'L' /* Magic number byte 2 */

#define EI_MAG3 3	/* File identification byte 3 index */
#define ELFMAG3 'F' /* Magic number byte 3 */

/* Conglomeration of the identification bytes, for easy testing as a word.  */
#define ELFMAG "\177ELF"
#define SELFMAG 4

#define EI_CLASS 4	   /* File class byte index */
#define ELFCLASSNONE 0 /* Invalid class */
#define ELFCLASS32 1   /* 32-bit objects */
#define ELFCLASS64 2   /* 64-bit objects */

#define EI_DATA 5	  /* Data encoding byte index */
#define ELFDATANONE 0 /* Invalid data encoding */
#define ELFDATA2LSB 1 /* 2's complement, little endian */
#define ELFDATA2MSB 2 /* 2's complement, big endian */

#define EI_VERSION 6 /* File version byte index */
					 /* Value must be EV_CURRENT */
#define EI_OSABI 7
#define ELFOSABI_SYSV 0
#define ELFOSABI_HPUX 1
#define ELFOSABI_STANDALONE 255

#define EI_ABIVERSION 8
#define EI_PAD 9 /* Byte index of padding bytes */
#define EI_NIDENT 16

/* Legal values for e_type (object file type).  */

#define ET_NONE 0		 /* No file type */
#define ET_REL 1		 /* Relocatable file */
#define ET_EXEC 2		 /* Executable file */
#define ET_DYN 3		 /* Shared object file */
#define ET_CORE 4		 /* Core file */
#define ET_NUM 5		 /* Number of defined types.  */
#define ET_LOPROC 0xff00 /* Processor-specific */
#define ET_HIPROC 0xffff /* Processor-specific */

#define SHT_NULL 0	   /* Section header table entry unused */
#define SHT_PROGBITS 1 /* Program data */
#define SHT_SYMTAB 2   /* Symbol table */
#define SHT_STRTAB 3   /* String table */
#define SHT_RELA 4	   /* Relocation entries with addends */
#define SHT_HASH 5	   /* Symbol hash table */
#define SHT_DYNAMIC 6  /* Dynamic linking information */
#define SHT_NOTE 7	   /* Notes */
#define SHT_NOBITS 8   /* Program space with no data (bss) */
#define SHT_REL 9	   /* Relocation entries, no addends */
#define SHT_SHLIB 10   /* Reserved */
#define SHT_DYNSYM 11  /* Dynamic linker symbol table */
#define SHT_NUM 12	   /* Number of defined types.  */

#define SHF_WRITE (1 << 0)		/* Writable */
#define SHF_ALLOC (1 << 1)		/* Occupies memory during execution */
#define SHF_EXECINSTR (1 << 2)	/* Executable */
#define SHF_MASKPROC 0xf0000000 /* Processor-specific */



#define PT_LOAD 1


/* auxiliary vector types */
#define AT_NULL 0
#define AT_IGNORE 1
#define AT_EXECFD 2
#define AT_PHDR 3
#define AT_PHENT 4
#define AT_PHNUM 5
#define AT_PAGESZ 6
#define AT_BASE 7
#define AT_FLAGS 8
#define AT_ENTRY 9
#define AT_DCACHEBSIZE 10
#define AT_ICACHEBSIZE 11
#define AT_UCACHEBSIZE 12

/*** symbol constants ***/
#define ELF64_ST_BIND(i) ((i) >> 4)
#define ELF64_ST_TYPE(i) ((i)&0xf)
#define ELF64_ST_INFO(b, t) (((b) << 4) + ((t)&0xf))

#define STB_LOCAL 0
#define STB_GLOBAL 1
#define STB_WEAK 2
#define STB_LOPROC 13
#define STB_HIPROC 15

#define STT_NOTYPE 0
#define STT_OBJECT 1
#define STT_FUNC 2
#define STT_SECTION 3
#define STT_FILE 4
#define STT_LOPROC 13
#define STT_HIPROC 15


typedef uint64_t Elf64_Addr;
typedef uint64_t Elf64_Off;
typedef uint16_t Elf64_Half;
typedef uint32_t Elf64_Word;
typedef int32_t Elf64_Sword;
typedef uint64_t Elf64_Xword;
typedef int64_t Elf64_Sxword;

typedef struct
{
	Elf64_Word sh_name;		  /* Section name (string tbl index) */
	Elf64_Word sh_type;		  /* Section type */
	Elf64_Xword sh_flags;	  /* Section flags */
	Elf64_Addr sh_addr;		  /* Section virtual addr at execution */
	Elf64_Off sh_offset;	  /* Section file offset */
	Elf64_Xword sh_size;	  /* Section size in bytes */
	Elf64_Word sh_link;		  /* Link to another section */
	Elf64_Word sh_info;		  /* Additional section information */
	Elf64_Xword sh_addralign; /* Section alignment */
	Elf64_Xword sh_entsize;	  /* Entry size if section holds table */
} Elf64_Shdr;

typedef struct
{
	Elf64_Word st_name;		/* Symbol name (string tbl index) */
	unsigned char st_info;	/* Symbol type and binding */
	unsigned char st_other; /* No defined meaning, 0 */
	Elf64_Half st_shndx;	/* Section index */
	Elf64_Addr st_value;	/* Symbol value */
	Elf64_Xword st_size;	/* Symbol size */
} Elf64_Sym;

typedef struct
{
	Elf64_Word p_type;	  /* Segment type */
	Elf64_Word p_flags;	  /* Segment flags */
	Elf64_Off p_offset;	  /* Segment file offset */
	Elf64_Addr p_vaddr;	  /* Segment virtual address */
	Elf64_Addr p_paddr;	  /* Segment physical address */
	Elf64_Xword p_filesz; /* Segment size in file */
	Elf64_Xword p_memsz;  /* Segment size in memory */
	Elf64_Xword p_align;  /* Segment alignment */
} Elf64_Phdr;

typedef struct
{
	unsigned char e_ident[EI_NIDENT]; /* Magic number and other info */
	Elf64_Half e_type;				  /* Object file type */
	Elf64_Half e_machine;			  /* Architecture */
	Elf64_Word e_version;			  /* Object file version */
	Elf64_Addr e_entry;				  /* Entry point virtual address */
	Elf64_Off e_phoff;				  /* Program header table file offset */
	Elf64_Off e_shoff;				  /* Section header table file offset */
	Elf64_Word e_flags;				  /* Processor-specific flags */
	Elf64_Half e_ehsize;			  /* ELF header size in bytes */
	Elf64_Half e_phentsize;			  /* Program header table entry size */
	Elf64_Half e_phnum;				  /* Program header table entry count */
	Elf64_Half e_shentsize;			  /* Section header table entry size */
	Elf64_Half e_shnum;				  /* Section header table entry count */
	Elf64_Half e_shstrndx;			  /* Section header string table index */
} Elf64_Ehdr;

typedef struct tables
{
	int32_t sechdr_tbl_size;
	Elf64_Shdr *sec_header_tbl;
	int32_t secnmtbl_ndx;
	char *sec_name_tbl;

	int32_t symtbl_ndx;
	Elf64_Sym *sym_tbl;
	char *symstr_tbl;

	int32_t dysymtbl_ndx;
	Elf64_Sym *dysym_tbl;
	char *dystr_tbl;

	int32_t hashtbl_ndx;
	Elf64_Word *hash_tbl;
	Elf64_Sym *hashsym_tbl;

	int32_t pgm_hdr_tbl_size;
	Elf64_Phdr *pgm_header_tbl;
} Elf_Tables;

struct text_secs
{
	char name[20];
	Elf64_Off offset;
	Elf64_Addr address;
	Elf64_Xword size;
	uint8_t *bytes;
	struct text_secs *next;
};

struct text_info
{
	Elf64_Half txt_index;
	Elf64_Addr address;
	Elf64_Xword size;
	Elf64_Addr txt_addr;
	Elf64_Xword txt_size;
	uint8_t *bytes;
	struct text_secs *secs;
};

struct data_secs
{
	char name[40];
	Elf64_Off offset;
	Elf64_Addr address;
	Elf64_Xword size;
	Elf64_Word type;
	Elf64_Word flags;
	uint8_t *bytes;
	struct data_secs *next;
};

struct data_info
{
	Elf64_Addr address;
	Elf64_Xword size;
	struct data_secs *secs;
};

struct gliss_loader_t
{
	Elf_Tables Tables;
	struct text_info Text;
	struct data_info Data;
	Elf64_Ehdr Ehdr;
	int Is_Elf_Little;
	int is_cubin;
};


/* loader management */
gliss_loader_t *gliss_loader_open(const char *path);
void gliss_loader_close(gliss_loader_t *loader);
void gliss_loader_load(gliss_loader_t *loader, gliss_platform_t *pf);
gliss_address_t gliss_loader_start(gliss_loader_t *loader);

      
/* system initialization (used internally during platform and state initialization) */
gliss_address_t gliss_brk_init(gliss_loader_t *loader);

/* section access */
typedef struct gliss_loader_sect_t {
	const char *name;
	gliss_address_t addr;
	int size;
	enum {
		GLISS_LOADER_SECT_UNKNOWN = 0,
		GLISS_LOADER_SECT_TEXT,
		GLISS_LOADER_SECT_DATA,
		GLISS_LOADER_SECT_BSS
	} type;
} gliss_loader_sect_t;
int gliss_loader_count_sects(gliss_loader_t *loader);
void gliss_loader_sect(gliss_loader_t *loader, int sect, gliss_loader_sect_t *data);

/* symbol access */
typedef struct {
	const char *name;
	gliss_address_t value;
	int size;
	int sect;
	enum {
		GLISS_LOADER_SYM_NO_TYPE,
		GLISS_LOADER_SYM_DATA,
		GLISS_LOADER_SYM_CODE
	} type;
	enum {
		GLISS_LOADER_NO_BINDING,
		GLISS_LOADER_LOCAL,
		GLISS_LOADER_GLOBAL,
		GLISS_LOADER_WEAK
	} bind;
} gliss_loader_sym_t;
int gliss_loader_count_syms(gliss_loader_t *loader);
void gliss_loader_sym(gliss_loader_t *loader, int sym, gliss_loader_sym_t *data);



#if defined(__cplusplus)
}
#endif

#endif	/* GLISS_OLD_ELF_H */
