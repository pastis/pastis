#ifndef CONSTANT_H
#define CONSTANT_H

#include <stdint.h>

#define NVIDIA_CONSTANT_STATE
#define NVIDIA_CONSTANT_INIT(s)
#define NVIDIA_CONSTANT_DESTROY(s)

#define constant_addr(bank,offset) (uint64_t)(((uint8_t*)NVIDIA_C)+bank*32+offset *4)
#define read_constant_32(bank,offset) *(uint32_t*)(((uint8_t*)NVIDIA_C)+bank*32+offset *4)


#endif