#ifndef MEMBAR_H
#define MEMBAR_H


#define NVIDIA_MEMBAR_STATE
#define NVIDIA_MEMBAR_INIT(s)
#define NVIDIA_MEMBAR_DESTROY(s)

#define read_membar() *(((uint64_t*)NVIDIA_MEMORY_BAR))


#endif