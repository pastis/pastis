/*
 *	interface of old_elf module specialized for nvidia
 *
 *	This file is part of OTAWA
 *	Copyright (c) 2010, IRIT UPS.
 *
 *	GLISS is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	GLISS is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with OTAWA; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <gliss/loader.h>

#ifndef NDEBUG
#define assertp(c, m)                                                          \
	if (!(c))                                                                  \
	{                                                                          \
		fprintf(stderr, "assertion failure %s:%d: %s", __FILE__, __LINE__, m); \
		abort();                                                               \
	}
#define TRACE /*fprintf(stderr, "%s:%d\n", __FILE__, __LINE__)*/
#else
#define assertp(c, m)
#define TRACE
#endif

#include <gliss/mem.h>
#if defined(GLISS_VFAST_MEM)

#include <gliss/config.h>
#define little 0
#define big 1

#ifndef TARGET_ENDIANNESS
#error "TARGET_ENDIANNESS must be defined !"
#endif

#ifndef HOST_ENDIANNESS
#error "HOST_ENDIANNESS must be defined !"
#endif

#endif



/*********************** ELF loader ********************************/



/* Global data */
static Elf_Tables Initial_Tables = {
	0,
	NULL,
	-1,
	NULL,
	-1,
	NULL,
	NULL,
	-1,
	NULL,
	NULL,
	-1,
	NULL,
	NULL,
	0,
	NULL};
static Elf_Tables Tables;
static struct text_info Text;
static struct data_info Data;
static Elf64_Ehdr Ehdr;
static int Is_Elf_Little = 0;

int is_host_little(void)
{
	uint32_t x;
	x = 0xDEADBEEF;
	return (((unsigned char)x) == 0xEF);
}

int16_t ConvertByte2(int16_t Word)
{
	union
	{
		unsigned char c[2];
		int16_t i;
	} w1, w2;
	w1.i = Word;
	w2.c[0] = w1.c[1];
	w2.c[1] = w1.c[0];
	return (w2.i);
}

int32_t ConvertByte4(int32_t Dword)
{
	union
	{
		unsigned char c[4];
		int32_t i;
	} dw1, dw2;
	dw1.i = Dword;
	dw2.c[0] = dw1.c[3];
	dw2.c[1] = dw1.c[2];
	dw2.c[2] = dw1.c[1];
	dw2.c[3] = dw1.c[0];
	return (dw2.i);
}

int64_t ConvertByte8(int64_t Dword)
{
	union
	{
		unsigned char c[8];
		int64_t i;
	} dw1, dw2;
	dw1.i = Dword;
	dw2.c[0] = dw1.c[7];
	dw2.c[1] = dw1.c[6];
	dw2.c[2] = dw1.c[5];
	dw2.c[3] = dw1.c[4];
	dw2.c[4] = dw1.c[3];
	dw2.c[5] = dw1.c[2];
	dw2.c[6] = dw1.c[1];
	dw2.c[7] = dw1.c[0];
	return (dw2.i);
}

void ConvertElfHeader(Elf64_Ehdr *Ehdr)
{
	Ehdr->e_type = ConvertByte2(Ehdr->e_type);
	Ehdr->e_machine = ConvertByte2(Ehdr->e_machine);
	Ehdr->e_version = ConvertByte4(Ehdr->e_version);
	Ehdr->e_entry = ConvertByte8(Ehdr->e_entry);
	Ehdr->e_phoff = ConvertByte8(Ehdr->e_phoff);
	Ehdr->e_shoff = ConvertByte8(Ehdr->e_shoff);
	Ehdr->e_flags = ConvertByte4(Ehdr->e_flags);
	Ehdr->e_ehsize = ConvertByte2(Ehdr->e_ehsize);
	Ehdr->e_phentsize = ConvertByte2(Ehdr->e_phentsize);
	Ehdr->e_phnum = ConvertByte2(Ehdr->e_phnum);
	Ehdr->e_shentsize = ConvertByte2(Ehdr->e_shentsize);
	Ehdr->e_shnum = ConvertByte2(Ehdr->e_shnum);
	Ehdr->e_shstrndx = ConvertByte2(Ehdr->e_shstrndx);
}

int ElfReadHeader(int fd, Elf64_Ehdr *Ehdr)
{
	int foffset;
	TRACE;
	foffset = lseek(fd, 0, SEEK_CUR);
	lseek(fd, 0, SEEK_SET);
	if (read(fd, Ehdr, sizeof(Elf64_Ehdr)) != sizeof(Elf64_Ehdr))
		return -1;
	if (memcmp(Ehdr->e_ident, ELFMAG, 4))
	{
		errno = EBADF;
		return -1;
	}
	if (Ehdr->e_ident[EI_DATA] == ELFDATA2LSB)
		Is_Elf_Little = 1;
	else if (Ehdr->e_ident[EI_DATA] == ELFDATA2MSB)
		Is_Elf_Little = 0;
	else
	{
		errno = EBADF;
		return -1;
	}
	if (Ehdr->e_ident[EI_CLASS] != ELFCLASS64)
	{
		errno = EBADF;
		return -1;
	}
	lseek(fd, foffset, SEEK_SET);
	if (is_host_little() != Is_Elf_Little)
		ConvertElfHeader(Ehdr);
	return 0;
}

int ElfCheckExec(const Elf64_Ehdr *Ehdr)
{
	TRACE;
	if (Ehdr->e_type == ET_EXEC)
		return 0;
	else
	{
		errno = EBADF;
		return -1;
	}
}

void ConvertPgmHeader(Elf64_Phdr *Ephdr)
{
	Ephdr->p_type = ConvertByte4(Ephdr->p_type);
	Ephdr->p_flags = ConvertByte4(Ephdr->p_flags);
	Ephdr->p_offset = ConvertByte8(Ephdr->p_offset);
	Ephdr->p_vaddr = ConvertByte8(Ephdr->p_vaddr);
	Ephdr->p_paddr = ConvertByte8(Ephdr->p_paddr);
	Ephdr->p_filesz = ConvertByte8(Ephdr->p_filesz);
	Ephdr->p_memsz = ConvertByte8(Ephdr->p_memsz);
	Ephdr->p_align = ConvertByte8(Ephdr->p_align);
}

int ElfReadPgmHdrTbl(int fd, const Elf64_Ehdr *Ehdr)
{
	int32_t i;
	TRACE;
	if (Ehdr->e_phoff == 0)
	{
		errno = EBADF;
		return -1;
	}
	lseek(fd, Ehdr->e_phoff, SEEK_SET);
	Tables.pgm_hdr_tbl_size = Ehdr->e_phnum;
	Tables.pgm_header_tbl = (Elf64_Phdr *)malloc(Ehdr->e_phnum * sizeof(Elf64_Phdr));
	if (Tables.pgm_header_tbl == NULL)
	{
		errno = ENOMEM;
		return -1;
	}
	if (read(fd, Tables.pgm_header_tbl, (Ehdr->e_phnum * sizeof(Elf64_Phdr))) != (Ehdr->e_phnum * sizeof(Elf64_Phdr)))
	{
		errno = EBADF;
		return -1;
	}
	if (is_host_little() != Is_Elf_Little)
		for (i = 0; i < Ehdr->e_phnum; ++i)
			ConvertPgmHeader(&Tables.pgm_header_tbl[i]);
	return 0;
}

void ConvertSecHeader(Elf64_Shdr *Eshdr)
{
	Eshdr->sh_name = ConvertByte4(Eshdr->sh_name);
	Eshdr->sh_type = ConvertByte4(Eshdr->sh_type);
	Eshdr->sh_flags = ConvertByte8(Eshdr->sh_flags);
	Eshdr->sh_addr = ConvertByte8(Eshdr->sh_addr);
	Eshdr->sh_offset = ConvertByte8(Eshdr->sh_offset);
	Eshdr->sh_size = ConvertByte8(Eshdr->sh_size);
	Eshdr->sh_link = ConvertByte4(Eshdr->sh_link);
	Eshdr->sh_info = ConvertByte4(Eshdr->sh_info);
	Eshdr->sh_addralign = ConvertByte8(Eshdr->sh_addralign);
	Eshdr->sh_entsize = ConvertByte8(Eshdr->sh_entsize);
}

int ElfReadSecHdrTbl(int fd, const Elf64_Ehdr *Ehdr)
{
	int32_t i, foffset;
	TRACE;
	if (Ehdr->e_shoff == 0)
	{
		errno = EBADF;
		return -1;
	}
	foffset = lseek(fd, 0, SEEK_CUR);
	lseek(fd, Ehdr->e_shoff, SEEK_SET);
	Tables.sechdr_tbl_size = Ehdr->e_shnum;
	Tables.sec_header_tbl = (Elf64_Shdr *)malloc(Ehdr->e_shnum * sizeof(Elf64_Shdr));
	if (Tables.sec_header_tbl == NULL)
	{
		errno = ENOMEM;
		return -1;
	}
	if (read(fd, Tables.sec_header_tbl, (Ehdr->e_shnum * sizeof(Elf64_Shdr))) != (Ehdr->e_shnum * sizeof(Elf64_Shdr)))
	{
		errno = EBADF;
		return -1;
	}
	if (is_host_little() != Is_Elf_Little)
		for (i = 0; i < Ehdr->e_shnum; ++i)
			ConvertSecHeader(&Tables.sec_header_tbl[i]);
	lseek(fd, foffset, SEEK_SET);
	return 0;
}

int ElfReadSecNameTbl(int fd, const Elf64_Ehdr *Ehdr)
{
	int foffset;
	Elf64_Shdr Eshdr;
	TRACE;
	if (Ehdr->e_shoff == 0 || Tables.secnmtbl_ndx == 0)
	{
		errno = EBADF;
		return -1;
	}
	if (Tables.secnmtbl_ndx > 0)
		return 0;
	Tables.secnmtbl_ndx = Ehdr->e_shstrndx;
	foffset = lseek(fd, 0, SEEK_CUR);
	lseek(fd, Ehdr->e_shoff + Ehdr->e_shstrndx * Ehdr->e_shentsize, SEEK_SET);
	if (read(fd, &Eshdr, sizeof(Eshdr)) != sizeof(Eshdr))
	{
		errno = EBADF;
		return -1;
	}
	if (is_host_little() != Is_Elf_Little)
		ConvertSecHeader(&Eshdr);
	Tables.sec_name_tbl = (char *)malloc(Eshdr.sh_size);
	if (Tables.sec_name_tbl == NULL)
	{
		errno = ENOMEM;
		return -1;
	}
	lseek(fd, Eshdr.sh_offset, SEEK_SET);
	if (read(fd, Tables.sec_name_tbl, Eshdr.sh_size) != Eshdr.sh_size)
	{
		errno = EBADF;
		return -1;
	}
	lseek(fd, foffset, SEEK_SET);
	return 0;
}

void ConvertSymTblEnt(Elf64_Sym *Esym)
{
	Esym->st_name = ConvertByte4(Esym->st_name);
	Esym->st_shndx = ConvertByte2(Esym->st_shndx);
	Esym->st_value = ConvertByte8(Esym->st_value);
	Esym->st_size = ConvertByte8(Esym->st_size);
}

int ElfReadSymTbl(int fd, const Elf64_Ehdr *Ehdr)
{
	int32_t i, j, foffset;
	TRACE;
	if (Ehdr->e_shoff == 0)
	{
		errno = EBADF;
		return -1;
	}
	if (Tables.symtbl_ndx == 0)
	{
		errno = EBADF;
		return -1;
	}
	if (Tables.symtbl_ndx > 0)
		return 0; /* already done */
	foffset = lseek(fd, 0, SEEK_CUR);
	lseek(fd, Ehdr->e_shoff, SEEK_SET);
	for (i = 0; i < Ehdr->e_shnum; ++i)
		if (Tables.sec_header_tbl[i].sh_type == SHT_SYMTAB)
			break;
	if (Ehdr->e_shnum == i)
	{
		errno = EBADF;
		return -1;
	}
	Tables.symtbl_ndx = i;
	lseek(fd, Tables.sec_header_tbl[i].sh_offset, SEEK_SET);
	Tables.sym_tbl = (Elf64_Sym *)malloc(Tables.sec_header_tbl[i].sh_size);
	if (Tables.sym_tbl == NULL)
	{
		errno = ENOMEM;
		return -1;
	}
	if (read(fd, Tables.sym_tbl, Tables.sec_header_tbl[i].sh_size) != Tables.sec_header_tbl[i].sh_size)
	{
		errno = EBADF;
		return -1;
	}
	if (is_host_little() != Is_Elf_Little)
		for (j = 0; j < (Tables.sec_header_tbl[i].sh_size / Tables.sec_header_tbl[i].sh_entsize); ++j)
			ConvertSymTblEnt(&Tables.sym_tbl[j]);
	/* Got Symbol table now reading string table for it */
	i = Tables.sec_header_tbl[i].sh_link;
	Tables.symstr_tbl = (char *)malloc(Tables.sec_header_tbl[i].sh_size);
	if (Tables.symstr_tbl == NULL)
	{
		errno = ENOMEM;
		return -1;
	}
	lseek(fd, Tables.sec_header_tbl[i].sh_offset, SEEK_SET);
	if (read(fd, (char *)Tables.symstr_tbl, Tables.sec_header_tbl[i].sh_size) != Tables.sec_header_tbl[i].sh_size)
	{
		errno = EBADF;
		return -1;
	}
	lseek(fd, foffset, SEEK_SET);
	return 0;
}

void LoadString(char *buf, char *src, int n)
{
	strncpy(buf, src, n - 1);
	buf[n - 1] = '\0';
}

int ElfReadTextSecs(int fd, const Elf64_Ehdr *Ehdr)
{
	int32_t i, foffset;
	struct text_secs *txt_sec, **ptr, *ptr1;
	TRACE;
	if (Ehdr->e_shoff == 0)
	{
		errno = EBADF;
		return -1;
	}
	foffset = lseek(fd, 0, SEEK_CUR);
	lseek(fd, Ehdr->e_shoff, SEEK_SET);

	TRACE;
	for (i = 0; i < Ehdr->e_shnum; ++i)
	{
		if ((Tables.sec_header_tbl[i].sh_type == SHT_PROGBITS) && (Tables.sec_header_tbl[i].sh_flags & SHF_ALLOC) && (Tables.sec_header_tbl[i].sh_flags & SHF_EXECINSTR))
		{
			txt_sec = (struct text_secs *)malloc(sizeof(struct text_secs));
			if (txt_sec == NULL)
			{
				errno = ENOMEM;
				return -1;
			}
			LoadString(txt_sec->name, &Tables.sec_name_tbl[Tables.sec_header_tbl[i].sh_name], sizeof(txt_sec->name));
			if (!strcmp(txt_sec->name, ".text"))
				Text.txt_index = i;
			txt_sec->offset = Tables.sec_header_tbl[i].sh_offset;
			txt_sec->address = Tables.sec_header_tbl[i].sh_addr;
			txt_sec->size = Tables.sec_header_tbl[i].sh_size;
			txt_sec->next = NULL;
			txt_sec->bytes = (uint8_t *)malloc(txt_sec->size * sizeof(uint8_t));
			if (txt_sec->bytes == NULL)
			{
				free(txt_sec);
				errno = ENOMEM;
				return -1;
			}
			lseek(fd, txt_sec->offset, SEEK_SET);
			if (read(fd, txt_sec->bytes, txt_sec->size) != txt_sec->size)
			{
				free(txt_sec->bytes);
				free(txt_sec);
				errno = EBADF;
				return -1;
			}
			/* set next ptr */
			ptr = &Text.secs;
			while (*ptr != NULL)
			{
				if ((*ptr)->address > txt_sec->address)
				{
					txt_sec->next = *ptr;
					*ptr = txt_sec;
					break;
				}
				ptr = &((*ptr)->next);
			}
			if (*ptr == NULL)
				*ptr = txt_sec;
		}
	}
	if (Text.secs == NULL)
	{
		errno = EBADF;
		return -1;
	}

	/* ??? */
	TRACE;
	Text.address = Text.secs->address;
	ptr1 = Text.secs;
	while (ptr1->next != NULL)
		ptr1 = ptr1->next;
	Text.size = ptr1->address + ptr1->size - Text.address;
	Text.bytes = (uint8_t *)malloc(Text.size);
	if (Text.bytes == NULL)
	{
		errno = ENOMEM;
		return -1;
	}
	memset(Text.bytes, 0, Text.size);

	TRACE;
	ptr1 = Text.secs;
	while (ptr1 != NULL)
	{
		if (!strcmp(ptr1->name, ".text"))
		{
			Text.txt_addr = ptr1->address;
			Text.txt_size = ptr1->size;
		}
		lseek(fd, ptr1->offset, SEEK_SET);
		if (read(fd, &Text.bytes[ptr1->address - Text.address], ptr1->size) != ptr1->size)
		{
			errno = EBADF;
			return -1;
		}
		ptr1 = ptr1->next;
	}
	lseek(fd, foffset, SEEK_SET);
	Text.txt_addr = Ehdr->e_entry;

	TRACE;
	return 0;
}

int ElfInsertDataSec(const Elf64_Shdr *hdr, int fd)
{
	struct data_secs *data_sec, **ptr;
	data_sec = (struct data_secs *)malloc(sizeof(struct data_secs));
	if (data_sec == NULL)
	{
		errno = ENOMEM;
		return -1;
	}
	LoadString(data_sec->name, &Tables.sec_name_tbl[hdr->sh_name], sizeof(data_sec->name));
	data_sec->offset = hdr->sh_offset;
	data_sec->address = hdr->sh_addr;
	data_sec->size = hdr->sh_size;
	data_sec->type = hdr->sh_type;
	data_sec->flags = hdr->sh_flags;
	data_sec->next = NULL;
	data_sec->bytes = (uint8_t *)malloc(data_sec->size);
	if (data_sec->bytes == NULL)
	{
		free(data_sec);
		errno = ENOMEM;
		return -1;
	}
	if (
		(strcmp(data_sec->name, ".bss") != 0  || 
		 strcmp(data_sec->name, ".nv") != 0 ) && strcmp(data_sec->name, ".sbss") == 0)
	{
		lseek(fd, data_sec->offset, SEEK_SET);
		if (read(fd, data_sec->bytes, data_sec->size) != data_sec->size)
		{
			free(data_sec->bytes);
			free(data_sec);
			errno = EBADF;
			return -1;
		}
	}
	else
		memset(data_sec->bytes, 0, data_sec->size);
	ptr = &Data.secs;
	while (*ptr != NULL)
	{
		if ((*ptr)->address > data_sec->address)
		{
			data_sec->next = *ptr;
			*ptr = data_sec;
			break;
		}
		ptr = &((*ptr)->next);
	}
	if (*ptr == NULL)
		*ptr = data_sec;
	return 0;
}

int ElfReadDataSecs(int fd, const Elf64_Ehdr *Ehdr)
{
	int32_t i, foffset;
	foffset = lseek(fd, 0, SEEK_CUR);
	for (i = 0; i < Ehdr->e_shnum; ++i)
	{
		int res = 0;
		
		char buff[60];
		LoadString(buff,&Tables.sec_name_tbl[Tables.sec_header_tbl[i].sh_name],40);
		//printf("%s, type : %x\n",buff,Tables.sec_header_tbl[i].sh_type);
		if (Tables.sec_header_tbl[i].sh_type == SHT_PROGBITS)
		{
			
			if (Tables.sec_header_tbl[i].sh_flags == (SHF_ALLOC | SHF_WRITE))
				res = ElfInsertDataSec(&Tables.sec_header_tbl[i], fd);
			else if (Tables.sec_header_tbl[i].sh_flags == (SHF_ALLOC))
				res = ElfInsertDataSec(&Tables.sec_header_tbl[i], fd);
		}
		else if (Tables.sec_header_tbl[i].sh_type == SHT_NOBITS && Tables.sec_header_tbl[i].sh_flags == (SHF_ALLOC | SHF_WRITE))
			res = ElfInsertDataSec(&Tables.sec_header_tbl[i], fd);
		if (res != 0)
			return -1;
	}
	if (Data.secs != NULL)
		Data.address = Data.secs->address;
	else
		Data.address = 0;
	lseek(fd, foffset, SEEK_SET);
	return 0;
}

int ElfRead(int elf)
{
	int cond = ElfReadHeader(elf, &Ehdr) == 0; 
	cond &= ElfCheckExec(&Ehdr) == 0;
	cond &= ElfReadPgmHdrTbl(elf, &Ehdr) == 0;
	cond &= ElfReadSecHdrTbl(elf, &Ehdr) == 0;
	cond &= ElfReadSecNameTbl(elf, &Ehdr) == 0;
	cond &= ElfReadSymTbl(elf, &Ehdr) == 0;
	cond &= ElfReadTextSecs(elf, &Ehdr) == 0;
	cond &= ElfReadDataSecs(elf, &Ehdr) == 0;
	if (cond)
		return 0;
	else
		return -1;
}

void ElfCleanup(void)
{
	struct text_secs *curt, *nextt;
	struct data_secs *curd, *nextd;

	/* free static tables */
	if (Tables.pgm_header_tbl != NULL)
	{
		free(Tables.pgm_header_tbl);
		Tables.pgm_header_tbl = NULL;
	}
	if (Tables.sec_header_tbl != NULL)
	{
		free(Tables.sec_header_tbl);
		Tables.sec_header_tbl = NULL;
	}
	if (Tables.sec_name_tbl != NULL)
	{
		free(Tables.sec_name_tbl);
		Tables.sec_name_tbl = NULL;
	}
	if (Tables.sym_tbl != NULL)
	{
		free(Tables.sym_tbl);
		Tables.sym_tbl = NULL;
	}
	if (Tables.symstr_tbl != NULL)
	{
		free(Tables.symstr_tbl);
		Tables.symstr_tbl = NULL;
	}
	if (Text.bytes != NULL)
	{
		free(Text.bytes);
		Text.bytes = NULL;
	}

	/* free text sections */
	for (curt = Text.secs; curt != NULL; curt = nextt)
	{
		nextt = curt->next;
		free(curt->bytes);
		free(curt);
	}
	Text.secs = NULL;

	/* free data sections */
	for (curd = Data.secs; curd != NULL; curd = nextd)
	{
		nextd = curd->next;
		free(curd->bytes);
		free(curd);
	}
	Data.secs = NULL;
}

void ElfReset(void)
{
	memcpy(&Tables, &Initial_Tables, sizeof(Tables));
	memset(&Text, 0, sizeof(Text));
	memset(&Data, 0, sizeof(Data));
	memset(&Ehdr, 0, sizeof(Ehdr));
	Is_Elf_Little = 0;
}

/*********************** loader interface ********************************/

/* types */

/**
 * Open an ELF file.
 * @param path	Path to the file.
 * @return		ELF handler or null if there is an error. Error code in errno.
 * 				Error code may any one about file opening and
 *				ENOMEM (for lack of memory) or EBADF (for ELF format error).
 */
gliss_loader_t *gliss_loader_open(const char *path)
{
	gliss_loader_t *loader;
	int elf, res;
	assert(path);

	/* open the file */
	TRACE;
#if defined(__WIN32) || defined(__WIN64)
	elf = open(path, O_RDONLY | O_BINARY);
#else
	elf = open(path, O_RDONLY);
#endif
	if (elf == -1)
		return NULL;

	/* allocate handler */
	TRACE;
	loader = (gliss_loader_t *)malloc(sizeof(gliss_loader_t));
	if (loader == NULL)
	{
		errno = ENOMEM;
		close(elf);
		return NULL;
	}

	/* is it .cubin? */
	int ns = strlen(path);
	loader->is_cubin = ns > 6 && strcmp(path + ns - 6, ".cubin") == 0;

	/* load the ELF */
	TRACE;
	ElfReset();
	res = ElfRead(elf);
	assert(Text.secs != NULL);
	close(elf);
	if (res != 0)
	{
		//!!DEBUG!!
		//printf("res!=0\n");
		ElfCleanup();
		free(loader);
		return NULL;
	}

	/* record data */
	TRACE;
	loader->Tables = Tables;
	loader->Text = Text;
	loader->Data = Data;
	loader->Ehdr = Ehdr;
	loader->Is_Elf_Little = Is_Elf_Little;

	/* !!TODO!! after data stored into loader,
	clean the globals of Elf_Tables (just a shallow clean,
	real clean will be done by loader when destroyed) */
	ElfReset();

	return loader;
}

/**
 * Close the given opened file.
 * @param loader	Loader to work on.
 */
void gliss_loader_close(gliss_loader_t *loader)
{
	assert(loader);
	/*gliss_loader_t *backup = malloc(sizeof(gliss_loader_t));*/

	/* backup globals */
	/*backup->Tables = Tables;
	backup->Text = Text;
	backup->Data = Data;
	backup->Ehdr = Ehdr;
	backup->Is_Elf_Little = Is_Elf_Little;*/

	/* replace globals by loader's data */
	Tables = loader->Tables;
	Text = loader->Text;
	Data = loader->Data;
	Ehdr = loader->Ehdr;
	Is_Elf_Little = loader->Is_Elf_Little;

	/* destroy loader's data */
	ElfCleanup();

	/* restore previous globals */
	/*Tables = backup->Tables;
	Text = backup->Text;
	Data = backup->Data;
	Ehdr = backup->Ehdr;
	Is_Elf_Little = backup->Is_Elf_Little;*/

	/* destroy loader */
	free(loader);
}

/**
 * Load the opened ELF program into the given platform (main memory chosen).
 * @param loader	program ELF loader
 * @param memory	memory to load in
 */
void gliss_loader_load(gliss_loader_t *loader, gliss_platform_t *pf)
{
	struct data_secs *ptr;
	struct text_secs *ptr_tex;
	uint8_t *rbuffer;
	unsigned int i;
	assert(loader->Text.secs != NULL);

	/* adapt the next line if you have an harvard mem archi */
	gliss_memory_t *memory = gliss_get_memory(pf, GLISS_MAIN_MEMORY);

	/* load text part */
	TRACE;
	Elf64_Addr base = CODE_BASE_ADDRESS;
	ptr_tex = loader->Text.secs;
	while (ptr_tex != NULL)
	{
		TRACE;
		if (ptr_tex->size)
			gliss_mem_write(memory,
							loader->is_cubin ? base : ptr_tex->address,
							ptr_tex->bytes, ptr_tex->size);
		base += ptr_tex->size;
		ptr_tex = ptr_tex->next;
	}

	/* load data part */
	TRACE;
	base = DATA_BASE_ADDRESS;
	ptr = loader->Data.secs;
	while (ptr != NULL)
	{
		TRACE;
		if (ptr->size)
			gliss_mem_write(memory,
							loader->is_cubin ? base : ptr->address,
							ptr->bytes, ptr->size);
		base += ptr->size;
		ptr = ptr->next;
	}

	/* initialize BSS part */
#ifdef GLISS_NOBITS_INIT
	for (i = 0; i < loader->Tables.sechdr_tbl_size; i++)
	{
		Elf64_Shdr *sec = &loader->Tables.sec_header_tbl[i];
		if (sec->sh_type == SHT_NOBITS)
		{
			int j;
			Elf64_Addr p = loader->is_cubin ? base : sec->sh_addr;
			for (j = 0; j < sec->sh_size; j += sizeof(uint32_t))
				gliss_mem_write32(memory, p + j, 0);
			base += sec->sh_size;
		}
	}
#endif
}

/**
 * Get the address of the entry point of the program.
 * @param loader	Used loader.
 * @return			entry point address.
 */
gliss_address_t gliss_loader_start(gliss_loader_t *loader)
{
	assert(loader);
	return loader->Text.txt_addr;
}

/**
 * Get the number of sections in the executable.
 * @param loader	Current loader.
 * @return			Number of sections.
 */
int gliss_loader_count_sects(gliss_loader_t *loader)
{
	return loader->Tables.sechdr_tbl_size;
}

/**
 * Get information about a section.
 * @param loader	Current loader.
 * @param sect		Section index (starting from 0).
 * @param data		Data structure to store information in.
 */
void gliss_loader_sect(gliss_loader_t *loader, int sect, gliss_loader_sect_t *data)
{
	Elf64_Shdr *s;

	/* get the section */
	assert(sect < loader->Tables.sechdr_tbl_size);
	s = &loader->Tables.sec_header_tbl[sect];

	/* fill address and size */
	data->addr = s->sh_addr;
	data->size = s->sh_size;

	/* get the type */
	switch (s->sh_type)
	{
	case SHT_PROGBITS:
		if (s->sh_flags & SHF_ALLOC)
		{
			if (s->sh_flags & SHF_EXECINSTR)
				data->type = GLISS_LOADER_SECT_TEXT;
			else
				data->type = GLISS_LOADER_SECT_DATA;
		}
		else
			data->type = GLISS_LOADER_SECT_BSS;
		break;
	default:
		data->type = GLISS_LOADER_SECT_UNKNOWN;
		break;
	}

	/* get the name */
	data->name = loader->Tables.sec_name_tbl + loader->Tables.sec_header_tbl[sect].sh_name;
}

/**
 * Get the number of symbols in the given loader.
 * @param loader	Current loader.
 * @return			Number of symbols.
 */
int gliss_loader_count_syms(gliss_loader_t *loader)
{
	int i = loader->Tables.symtbl_ndx;
	return loader->Tables.sec_header_tbl[i].sh_size / loader->Tables.sec_header_tbl[i].sh_entsize;
}

/**
 * Get information about a symbol.
 * @param loader	Current loader.
 * @param sym		Symbol index.
 * @param data		Dat structure to store information in.
 */
void gliss_loader_sym(gliss_loader_t *loader, int sym, gliss_loader_sym_t *data)
{
	assert(sym < gliss_loader_count_syms(loader));
	Elf64_Sym *s;

	/* get the descriptor */
	assert(sym < gliss_loader_count_syms(loader));
	s = &loader->Tables.sym_tbl[sym];

	/* get name */
	data->name = loader->Tables.symstr_tbl + loader->Tables.sym_tbl[sym].st_name;

	/* get value and section */
	data->value = s->st_value;
	data->sect = s->st_shndx;
	data->size = s->st_size;

	/* get the binding */
	switch (ELF64_ST_BIND(s->st_info))
	{
	case STB_LOCAL:
		data->bind = GLISS_LOADER_LOCAL;
		break;
	case STB_GLOBAL:
		data->bind = GLISS_LOADER_GLOBAL;
		break;
	case STB_WEAK:
		data->bind = GLISS_LOADER_WEAK;
		break;
	default:
		data->bind = GLISS_LOADER_NO_BINDING;
		break;
	}

	/* get the type */
	switch (ELF64_ST_TYPE(s->st_info))
	{
	case STT_FUNC:
		data->type = GLISS_LOADER_SYM_CODE;
		break;
	case STT_OBJECT:
		data->type = GLISS_LOADER_SYM_DATA;
		break;
	default:
		data->type = GLISS_LOADER_SYM_NO_TYPE;
		break;
	}
}

/* MEMORY_PAGE_SIZE gotten from mem.c,
!!WARNING!! value could change between archis and between systems */
#define MEMORY_PAGE_SIZE 4096

/**
 * get the initial value of the brk address
 * @param    loader  Loader containing code and data size informations.
 * @return	     the initial brk value
 */
gliss_address_t gliss_brk_init(gliss_loader_t *loader)
{
	int n = loader->Tables.pgm_hdr_tbl_size;
	Elf64_Phdr *seg_tbl = loader->Tables.pgm_header_tbl;
	gliss_address_t brk_init = 0;
	int i;

	/* brk will be the highest (vaddr + memsz) value
	among all loadable segments */

	for (i = 0; i < n; i++)
	{
		if (seg_tbl[i].p_type == PT_LOAD)
		{
			gliss_address_t new_brk = seg_tbl[i].p_vaddr + seg_tbl[i].p_memsz;
			if (new_brk > brk_init)
				brk_init = new_brk;
		}
	}

	/* MEMORY_PAGE_SIZE gotten from mem.h */
	return (brk_init + MEMORY_PAGE_SIZE - 1) & ~(MEMORY_PAGE_SIZE - 1);
}

//unsigned int readG32(long long int pointeur){
//  return *((unsigned int *)pointeur );
//}
