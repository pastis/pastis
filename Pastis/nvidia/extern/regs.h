#include <stdint.h>

#define NVIDIA_REGS_STATE
#define NVIDIA_REGS_INIT(s)
#define NVIDIA_REGS_DESTROY(s)

#define read_reg(x, num) *(x*)(NVIDIA_R + num)

#define write_reg(x, num, value) *(x*)(NVIDIA_R + num) = (num != 255 ) ? value : 0

#define read_regs_u8(num) read_reg(uint8_t, num)
#define read_regs_u16(num) read_reg(uint16_t, num)
#define read_regs_u32(num) read_reg(uint32_t, num)
#define read_regs_u64(num) read_reg(uint64_t, num)

#define read_regs_f32(num) read_reg(float, num)
#define read_regs_f64(num) read_reg(double, num);

#define write_regs_u32(num,value) write_reg(uint32_t,num,value)
#define write_regs_s32(num,value) write_reg(uint32_t,num,value)
#define write_regs_u64(num,value) write_reg(uint64_t,num,value)

#define write_regs_f32(num,value) write_reg(float,num,value)
#define write_regs_f64(num,value) write_reg(double,num,value)
