#include <stdint.h>

#define NVIDIA_SHARED_STATE
#define NVIDIA_SHARED_INIT(s)
#define NVIDIA_SHARED_DESTROY(s)

#define read_shared(x, addr) *(x*)(((uint8_t*)NVIDIA_S)+addr)
#define write_shared(x, addr, value) *(x*)(((uint8_t*)NVIDIA_S)+addr) = value

// Read 
#define read_shared_s8(addr) read_shared(int8_t, addr)
#define read_shared_u8(addr) read_shared(uint8_t, addr)
#define read_shared_s16(addr) read_shared(int16_t, addr)
#define read_shared_u16(addr) read_shared(uint16_t, addr)
#define read_shared_32(addr) read_shared(uint32_t, addr)

// Write
#define write_shared_8(addr, value) write_shared(uint8_t, addr, value)
#define write_shared_16(addr, value) write_shared(uint16_t, addr, value)
#define write_shared_32(addr, value) write_shared(uint32_t, addr, value)