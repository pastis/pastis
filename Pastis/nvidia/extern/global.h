#ifndef GLOBAL_H
#define GLOBAL_H

#include <stdint.h>

#if defined(__cplusplus)
extern "C"
{
#endif

#define NVIDIA_GLOBAL_STATE
#define NVIDIA_GLOBAL_INIT(s)
#define NVIDIA_GLOBAL_DESTROY(s)

#define read_global(x, addr) *((x *)addr)
#define write_global(x, addr, value) *((x *)addr) = value

#define read_global_s8(addr) read_global(int8_t, addr)
#define read_global_u8(addr) read_global(uint8_t, addr)
#define read_global_s16(addr) read_global(int16_t, addr)
#define read_global_u16(addr) read_global(uint16_t, addr)
#define read_global_32(addr) read_global(uint32_t, addr)

#define write_global_8(addr, value) write_global(uint8_t, addr, value)
#define write_global_16(addr, value) write_global(uint16_t, addr, value)
#define write_global_32(addr, value) write_global(uint32_t, addr, value)

#if defined(__cplusplus)
}
#endif

#endif
