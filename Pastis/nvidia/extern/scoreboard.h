#include <stdint.h>

#define NVIDIA_SCOREBOARD_STATE
#define NVIDIA_SCOREBOARD_INIT(s)
#define NVIDIA_SCOREBOARD_DESTROY(s)

#define read_sb(x) *(((uint8_t*)NVIDIA_SCOREBOARD)+x)
