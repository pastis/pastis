// //#include "read32G.h"
// #include <stdint.h>
// #include <stdio.h>

// #define TRACE(x) 

// void test_position(void ){
//   int x=3;
// }

// /*uint32_t readG32E(unsigned int* pointeur){
//   return *pointeur;
//   //return *((unsigned int *)pointeur );
//   }*/
// uint32_t readG32E(uint64_t pointeur){
//   TRACE(printf("Call to readG32E: address = %lu (0x%lx)\n", pointeur, pointeur);)
//   return *(uint32_t *)pointeur;
//   //return *((unsigned int *)pointeur );
// }

// uint32_t readC32E(uint64_t pointeur){
//   return *(uint32_t *)pointeur;
//   //return *((unsigned int *)pointeur );
// }

// //Same thing as for readG32E except platform.nmp defined parameter as 32 bits
// uint32_t readG32(uint32_t pointeur){
//   TRACE(printf("Call to readG32: address = %u (0x%x)\n", pointeur, pointeur);)
//   return *(uint32_t *)pointeur;
// }

// uint16_t readG16E(uint64_t pointeur){
//   return (*(uint16_t *)pointeur);
//   //return *((unsigned int *)pointeur );
// }
// //Same thing as for readG32E except platform.nmp defined parameter as 32 bits
// uint16_t readG16(uint32_t pointeur){
//   return (*(uint16_t*)pointeur);
// }

// uint8_t readG8E(uint64_t pointeur){
//   return (*(uint8_t*)pointeur);
//   //return *((unsigned int *)pointeur );
// }
// //Same thing as for readG32E except platform.nmp defined parameter as 32 bits
// uint8_t readG8(uint32_t pointeur){
//   return (*(uint8_t*)pointeur);
// }

// void writeG32E(uint64_t pointeur, uint32_t val){//unsigned int val){
//   //printf("pointeur = %p\n", pointeur);
//   //printf("taille pointeur=%d\n", sizeof(pointeur));
//   *(uint32_t *)pointeur = val;
// }

// void writeG32(uint32_t pointeur, uint32_t val){//unsigned int val){
//   //printf("pointeur = %p\n", pointeur);
//   //printf("taille pointeur=%d\n", sizeof(pointeur));
//   *(uint32_t *)pointeur = val;
// }

// void writeG16E(uint64_t pointeur, uint16_t val){//unsigned int val){
//   //printf("pointeur = %p\n", pointeur);
//   //printf("taille pointeur=%d\n", sizeof(pointeur));
//   *(uint16_t *)pointeur = val;
// }

// void writeG16(uint32_t pointeur, uint16_t val){//unsigned int val){
//   //printf("pointeur = %p\n", pointeur);
//   //printf("taille pointeur=%d\n", sizeof(pointeur));
//   *(uint16_t *)pointeur = val;
// }

// void writeG8E(uint64_t pointeur, uint8_t val){//unsigned int val){
//   //printf("pointeur = %p\n", pointeur);
//   //printf("taille pointeur=%d\n", sizeof(pointeur));
//   *(uint8_t *)pointeur = val;
// }

// void writeG8(uint32_t pointeur, uint8_t val){//unsigned int val){
//   //printf("pointeur = %p\n", pointeur);
//   //printf("taille pointeur=%d\n", sizeof(pointeur));
//   *(uint8_t *)pointeur = val;
// }
