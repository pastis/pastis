#include <stdint.h>

#define NVIDIA_LOCAL_STATE
#define NVIDIA_LOCAL_INIT(s)
#define NVIDIA_LOCAL_DESTROY(s)

#define read_local(x, addr) *(x*)(((uint8_t*)NVIDIA_L)+addr)
#define write_local(x, addr, value) *(x*)(((uint8_t*)NVIDIA_L)+addr) = value

// Read 
#define read_local_s8(addr) read_local(int8_t, addr)
#define read_local_u8(addr) read_local(uint8_t, addr)
#define read_local_s16(addr) read_local(int16_t, addr)
#define read_local_u16(addr) read_local(uint16_t, addr)
#define read_local_32(addr) read_local(uint32_t, addr)

// Write
#define write_local_8(addr, value) write_local(uint8_t, addr, value)
#define write_local_16(addr, value) write_local(uint16_t, addr, value)
#define write_local_32(addr, value) write_local(uint32_t, addr, value)