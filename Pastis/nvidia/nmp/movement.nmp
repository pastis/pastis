// ========= MOV =====================================

op mov_reg_reg(src: reg_t, dest: reg_t, guard: guard_t)
    syntax = format("%s MOV %s, %s", guard.syntax, dest.syntax, src.syntax)
    image = format("010111001001 1XXXXXXX X1111XXX XXXXXXXX %s %s XXXXXXXX %s", src, guard, dest)
    action = {
        UNIT = 1;
        if (guard) then
            EXECUTED =1;
            "write_regs_u32"(dest, "read_regs_u32"(src));
        endif;
    }

op mov_reg_const(offset: card(14), bank: card(5), dest: reg_t, guard: guard_t)
    syntax = format("%s MOV %s, c[0x%x][0x%x]", guard.syntax, dest.syntax, bank, offset<<2)
    image = format("010011001001 1XXXXXXX X1111 %5b %14b %s XXXXXXXX %s", bank, offset, guard, dest)
    action = {
        UNIT = 1;
        if (guard) then
            EXECUTED =1;
            MEMORY_TYPE =4;
            ADDR = "constant_addr"(bank,offset);
            let r = "read_constant_32"(bank,offset);
            "write_regs_u32"(dest, r);
        endif;
    }

op mov32i_reg_lit(value: card(32), dest: reg_t, guard: guard_t)
    syntax =  format("%s MOV32I %s, 0x%x", guard.syntax, dest.syntax, value)
    image = format("0000 0001 0000 %32b %s 1111 XXXX %s", value, guard, dest)
    action = {
            UNIT = 1;
		if (guard) then
            EXECUTED =1;
			"write_regs_u32"(dest, value);
		endif;
    }

// ========= SEL =====================================

op sel_reg_reg_reg(rs1: reg_t,rs2: reg_t, rd: reg_t,guard: guard_t,condition:sel_cond_t)
    syntax = format("%s SEL %s, %s, %s, %s", guard.syntax, rd.syntax, rs1.syntax, rs2.syntax, condition.syntax)
    image = format("0101110010100 XXXXXXXX %s XXXXXXXXXXX %s %s %s %s  ",condition ,rs2, guard , rs1, rd)
    action = {
            UNIT = 1;
        if (guard) then
            EXECUTED =1;
            if (condition) then 
                "write_regs_u32"(rd, "read_regs_u32"(rs1));
            else 
                "write_regs_u32"(rd, "read_regs_u32"(rs2));
            endif;
        endif;
    }

op sel_reg_reg_imm(rs1: reg_t,imm: card(19), rd: reg_t,guard: guard_t,condition:sel_cond_t)
    syntax = format("%s SEL %s, %s, 0x%x, %s", guard.syntax, rd.syntax, rs1.syntax, imm, condition.syntax)
    image = format("0011100010100 XXXXXXXX %s %19b %s %s %s  ",condition ,imm, guard , rs1, rd)
    action = {
            UNIT = 1;
        if (guard) then
            EXECUTED =1;
            if (condition) then 
                "write_regs_u32"(rd, "read_regs_u32"(rs1));
            else 
                "write_regs_u32"(rd, imm);
            endif;
        endif;
    }

op sel_reg_reg_const(rd: reg_t,rs : reg_t,bank:card(5),offset:card(19),guard: guard_t,condition:sel_cond_t)
  syntax = format("%s SEL %s, %s, c[0x%x][0x%x], %s", guard, rd, rs,bank,offset, condition)
  image = format("0100 1100 1010 0 %s %5b 001 %19b %s %s %s",condition,bank ,offset, guard , rs, rd)
  action = {
        UNIT = 1;
        if (guard) then
            EXECUTED =1;
            MEMORY_TYPE =3;
            ADDR = "constant_addr"(bank,offset);
            if (condition) then 
                "write_regs_u32"(rd, "read_regs_u32"(rs));
            else 
                "write_regs_u32"(rd, "read_constant_32"(bank,offset));
            endif;
        endif;
    }