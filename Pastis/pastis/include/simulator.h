#pragma once

#include <array>
#include <string>

#include "nvidia.h"

struct Simulator {
    std::array<uint8_t,32 * (1 << 16)> constantMemory;
    
    
    nvidia_address_t address_start;
    nvidia_address_t address_exit;
    nvidia_platform_t *platform;

    explicit Simulator(std::string cubin_file_name);

    template<class T>
    void affect_c_mem(int bank, int offset, T value) {
        *(T *) (&this->constantMemory[bank * 32 + offset]) = value;
    }
};
