#pragma once

#include <cstdint>

enum SimMode {
    FAST,
    DEBUG,
    QUIT
};

enum SchedulingAlgorithm {
    STANDARD
};

enum CacheAlgorithm {
    DIRECT_MAPPED,
    PLRU,
    CLRU
};

struct CacheConfig {
    uint32_t line_size;
    uint32_t associativity;
    uint32_t cache_size;
    CacheAlgorithm algorithm;
};

// struct SMPConfig {
//     SchedulingAlgorithm scheduling_algorithm;
// };

struct SMConfig {
    CacheConfig i_cache;
    CacheConfig l1_cache;
    CacheConfig op_cache;
    //SMPConfig smp_config;
};

struct DeviceConfig {
    CacheConfig l2_cache;
    SMConfig sm_config;
};

struct Config {
    DeviceConfig device_config;
    SimMode sim_mode;
    bool trace_enable;
    uint32_t trace_level;
    Config() = default;
};

struct Tx2Config : public Config {
    Tx2Config(bool trace = true, uint32_t trace_level = 1, SimMode mode = DEBUG);
};