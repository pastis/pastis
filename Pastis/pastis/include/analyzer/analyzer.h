#pragma once
#include <iostream>
#include <array>

#include "grid/warp.h"

#include "device/constants.h"
#include "device/types.h"

#include "memories/memory_op.h"
#include "memories/memory_type.h"

#include "caches/cache.h"

#include "analyzer/stats.h"

struct Analyzer {
    std::array<uint64_t,SM_PER_DEV * SMP_PER_SM> cycles{};
    std::array<CacheStats,L2_PER_DEV> l2{};
    std::array<CacheStats,L1_PER_SM * SM_PER_DEV> l1{};

    size_t l_read,l_write;    // local memory
    size_t g_read,g_write;    // global memory
    size_t c_read;            // constant memory
    size_t s_read,s_write;    // shared memory

    Analyzer();
    
    void postAnalysis(Warp *warp, SMP *smp);
    void printAnalysis();
    void dumpAnalysis(std::ostream &out);
    void access(MemoryType t, MemoryOp o, size_t n = 1);
    void stepCycle(size_t idx_smp);
    void cacheAccess(CacheAnswer a, CacheLevel l, size_t cache_idx = 0);
};

