#pragma once

#include <cstdint>

struct CacheStats
{
    uint32_t miss = 0, hit = 0;
};
