#pragma once

#include <cstdint>

struct dim3 {
    uint32_t x;
    uint32_t y;
    uint32_t z;

    dim3(uint32_t x, uint32_t y = 1, uint32_t z = 1)
        : x { x }
        , y { y }
        , z { z }
    {
    };
};