#pragma once

enum ParameterType {
    INT32,
    INT64,
    FLOAT,
    DOUBLE,
    PTR
};

struct Parameter {
    union {
        uint32_t i;
        uint64_t l;
        float f;
        double d;
        void* ptr;
    } val;
    ParameterType kind;
};