#pragma once

#include "device/device.h"

void pastisMemCopy(Device& device, void* address, size_t s);

void pastisMalloc(void **ptr, size_t s);

void pastisFree(void *ptr);
