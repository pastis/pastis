#pragma once

#include <cstdint>
#include <string>

#include "device/device.h"

#include "config.h"

#include "pastis/parameter.h"

void simulate(Device &device, std::string cubin_file_name, dim3 gridDim, dim3 blockDim, size_t params_num, Parameter *params, Config &config);