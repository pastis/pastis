#pragma once

#include <regex>

#include <cstdbool>


#include "commands/manager.h"

struct CommandManager {
    std::regex help{" *[hH] *$"};
    std::regex quit{" *[qQ] *$"};
    std::regex step{" *[sS] *$"};
    std::regex fast{" *[fF] *$"};
    std::regex fast_to{" *[fF] *0x([A-Fa-f0-9]+)"};
    std::regex watch{" *[wW]([0-9]+)"};
    std::regex r_read{" *[rR]([0-9]+)"};
    std::regex r_write{" *[rR]([0-9]+) *= *(0[bx])?([A-Fa-f0-9]+)"};
    std::regex sr_read{" *[sS][rR]([0-9]+)"};
    std::regex sr_write{" *[sS][rR]([0-9]+) *= *(0[bx])?([A-Fa-f0-9]+)"};
    std::regex p_read{" *[pP] *$"};
    std::regex p_write{" *[pP]([0-6]+) *= *([0-1])"};
    std::regex cc_read{" *[cC][cC] *$"};
    std::regex of_write{" *[oO][fF] *= *([0-1])"};
    std::regex cf_write{" *[cC][fF] *= *([0-1])"};
    std::regex sf_write{" *[sS][fF] *= *([0-1])"};
    std::regex zf_write{" *[zZ][fF] *= *([0-1])"};
    std::regex c_mem_read{" *[cC] *0x([A-Fa-f0-9]+),0x([A-Fa-f0-9]+)"};
    std::regex c_mem_write{" *[cC] *0x([A-Fa-f0-9]+),0x([A-Fa-f0-9]+) *= *(0[bx])?([A-Fa-f0-9]+)"};
    std::regex g_mem_read{" *[mM] *0x([A-Fa-f0-9]+)"};
    std::regex g_mem_read_r{" *[mM] *[rR]([A-Fa-f0-9]+)"};
    std::regex mask_stack{" *[sS][iI][mM][tT]"};
    std::regex rpc{" *[rR][pP][cC]"};
    std::regex s_mem_read{" *[sS][hH][rR][dD] *0x([A-Fa-f0-9]+)"};
    std::regex sbr{" *[sS][bB][rR]"};
    std::regex clk{" *[cC][lL][kK]"};
    size_t  watched_thread; 
    uint64_t breakpoint{};
    SimMode state;
    std::string old_command{"h"};
    bool breakpoint_enabled;
    
    explicit CommandManager(SimMode mode);
    bool nextCommand(Warp *warp, SMP *smp);
};

