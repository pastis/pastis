#pragma once

#include <iostream>

#include "config.h"


#define INIT 0
#define INFO 1
#define DEBUGING 2

void init_trace(Config& config);

void init(const char* format, ...);
void info(const char* format, ...);
void debug(const char* format, ...);
