#pragma once

#include "analyzer/analyzer.h"
#include "device/types.h"
#include "grid/warp.h"
#include "nvidia.h"

enum class LDGFlags {
    CA = 0,
    CG = 1,
    CI = 2,
    CV = 3
};

enum class STGFlags {
    WB = 0,
    CG = 1,
    CS = 2,
    WT = 3
};

extern uint32_t scheme_access[4];

struct GlobalMemory {

    unsigned int access(Warp* warp, Analyzer& analyser, nvidia_state_t* state, SMP* smp);
};
