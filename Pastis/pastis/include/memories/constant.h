#pragma once

#include "nvidia.h"

#include "grid/types.h"

#include "analyzer/types.h"

#include "device/types.h"

struct ConstantMemory {
    unsigned int access(Warp* warp, Analyzer& analyzer, nvidia_state_t* state, SMP* smp);
};