#pragma once

enum MemoryType {
    NONE     = 0,
    Global   = 1, 
    Shared   = 2,
    Local    = 3,
    Constant = 4
};