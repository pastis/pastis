#pragma once

enum MemoryOp {
    Read = 1,
    Write = 2
};