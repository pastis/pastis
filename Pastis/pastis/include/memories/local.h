#pragma once

#include "nvidia.h"

#include "grid/types.h"

#include "analyzer/types.h"

struct LocalMemory {
    unsigned int access(Warp* warp);
};