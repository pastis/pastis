#pragma once

#include "grid/warp.h"

#include "device/smp.h"

#include "analyzer/analyzer.h"

#include "memories/shared.h"
#include "memories/global.h"
#include "memories/local.h"
#include "memories/constant.h"

struct Memories {
    GlobalMemory global_memory;
    SharedMemory shared_memory;
    LocalMemory local_memory;
    ConstantMemory constant_memory;
    void access(Warp* warp, SMP* smp, Analyzer& analyzer);
};


size_t nb_32bit_acceded(size_t size_field);
