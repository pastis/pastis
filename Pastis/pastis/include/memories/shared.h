#pragma once

#include "analyzer/types.h"

#include "device/constants.h"

class SharedMemory {
    std::array<std::array<char, 48 * 1024>, SM_PER_DEV> shared_memories {};

public:
    unsigned int access(Warp* warp, nvidia_state_t* state);
    char *operator[](size_t index);
};
