#pragma once

#include <array>

#include <memory>

#include <cstdint>

#include "utils/bascule.h"

#include "device/constants.h"
#include "device/device.h"
#include "device/dispatcher.h"
#include "device/types.h"
#include "device/simt_stack.h"
#include "utils/circular_buffer.h"

#include "grid/warp.h"

#include "caches/cache.h"

#include "schedule/instruction.h"

#include "commands/manager.h"

#include "analyzer/analyzer.h"



struct SMP {

    // In GPU location attributs

    size_t index;

    size_t sm_index;

    Device* parent_device;

    // Warps attributs
    std::array<SimtStack, WARPS_PER_SMP> stacks;
    std::array<Warp*, WARPS_PER_SMP> warps;
    uint16_t active_warps;

    // Fetch logic attributs
    std::array<uint64_t, WARPS_PER_SMP> buffers_pc;
    size_t phase_counter;
    uint64_t request_latency;
    size_t current_warp_requested;
    Bascule<uint16_t> valid_buffers;
    bool instruction_fetched;
    //Bascule<uint16_t> valid_buffers;

    // Operand collector_stage

    CircularBuffer<5> operand_collectors_art;

    CircularBuffer<5> operand_collectors_lsu;

    // Dispatch attributs

    std::array<unsigned int, WARPS_PER_SMP> sched_latencies;
    Dispatcher dispatcher_art;
    Dispatcher dispatcher_lsu;

    // Execution side attributs

    InstructionSlot core;
    InstructionSlot dpu;

    CircularBuffer<LSU_DEPTH> lsu_buffer;
    InstructionSlot lsu;

    // Associated Cache

    Cache* l1; // L1 Data

    Cache* ic; // L1 Inst

    // SMP Clock

    uint64_t clock;

    // Constructor

    SMP(size_t index, size_t sm_index, Device* d, Cache* ic, Cache* l1_cache);

    // Function

    void step(Analyzer& analyzer, CommandManager& cm);

    void affectWarp(Warp* warp);

    Warp* nextWarp();

    void stepCycle();

    void updateBuffer();

    uint64_t requestWarpInstr(uint64_t pc);

    void fecth();

    void sendToDispatcher(Warp* warp);

    void dispatch();

    void operand_collection();

    void buffer();

    void commit();
};
