#pragma once 

#include <cstdint>

#include <array>

#include "device/constants.h"

struct SimtStackEntry {
    uint64_t npc;
    uint64_t rpc;
    uint32_t mask;
};



struct SimtStack {
    std::array<SimtStackEntry,SIMT_STACK_SIZE> stack;

    int8_t tos;

    SimtStack();

    bool isEmpty();

    void dump();

    void readTop(uint64_t *npc, uint64_t *rpc, uint32_t *mask);

    void writeTop(uint64_t npc, uint64_t rpc, uint32_t mask);

    void push(uint64_t npc, uint64_t rpc, uint32_t mask);

    void markThreadDone(uint64_t num_thread);

    uint64_t pop();

    uint32_t lastMask();

    uint64_t lastNpc();

    uint64_t lastRpc();

    SimtStackEntry& operator[](uint8_t idx) {
        return this->stack[idx];
    }
};