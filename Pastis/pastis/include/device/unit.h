#pragma once

enum Unit {
    NO_UNIT = 0,
    CUDA_CORES = 1,
    SFU = 2,
    LSU = 3,
    DPU = 4,
    S2R = 5,
    BRA = 6
};