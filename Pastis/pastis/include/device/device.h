#pragma once

#include <array>
#include <memory>

#include "caches/cache.h"

#include "grid/grid.h"

#include "device/constants.h"

#include "memories/memories.h"

#include "device/sm.h"

#include "config.h"

struct Device {
    // l2 cache shared across all sm
    std::array<std::unique_ptr<Cache>, L2_PER_DEV> l2Caches;
    // in function of the target device we can have multiple SM actually we are not able to do that
    std::array<std::unique_ptr<SM>, SM_PER_DEV> SMs;

    Memories memories;

    explicit Device(Config& config);

    void map(Grid& grid);
};
