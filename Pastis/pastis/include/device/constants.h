#pragma once
// according to pascal 6.2 (tx2) occupancy spread sheet
// https://docs.nvidia.com/cuda/cuda-occupancy-calculator/index.html



#define MAX_WARPS_PER_MULTIPROCESSOR 64
#define MAX_THREADS_BLOCK_PER_MULTIPROCESSOR 32
#define MAX_THREADS_PER_MULTIPROCESSOR 2048

#define REGISTERS_PER_MULTIPROCESSOR 65536
#define MAX_REGISTERS_PER_THREAD_BLOCK 65536
#define MAX_REGISTERS_PER_THREAD 255
#define SHARED_MEMORY_PER_MULTIPROCESSOR 65536
#define MAX_SHARED_MEMORY_PER_BLOCK 49152
#define REGISTER_ALLOCATION_UNIT_SIZE 256
#define SHARED_MEMORY_ALLOCATION_UNIT_SIZE 256
#define WARP_ALLOCATION_GRANULARITY 4

// According to findings on Pascal architecture
#define SM_PER_DEV 2
#define SMP_PER_SM 4
#define L1_PER_SM 2
#define L2_PER_DEV 4
#define LSU_DEPTH 20
#define WARPS_PER_SMP 16 // We assume that number, since an SM can handle a total of 64 warps and has 4 partition (SMP)

#define SIMT_STACK_SIZE 16