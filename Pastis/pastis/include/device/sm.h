#pragma once

#include <memory>
#include <vector>

#include "device/constants.h"
#include "device/types.h"
#include "grid/block.h"

#include "analyzer/types.h"
#include "caches/cache.h"
#include "commands/manager.h"

struct  SM {
    // 1 l1 cache for 2 smp according to nvidia pascal SM representation
    std::array<std::unique_ptr<Cache>, L1_PER_SM> l1_caches {};
    
    std::array<uint32_t, MAX_THREADS_BLOCK_PER_MULTIPROCESSOR> memory_barriers {};
    
    
    // link to parent device (l2 access)
    size_t sm_id;
    
    // block executed on this SM
    Block* blocks;
    
    std::unique_ptr<Cache> is_cache{};
    std::unique_ptr<Cache> op_cache {};

    std::array<std::unique_ptr<SMP>, SMP_PER_SM> smps {};
    
    SM(size_t id, Device* d, SMConfig& config);

    void dispatchBlock();
    
    void step(Analyzer& analyzer, CommandManager& cm);

    void arbiter();
};
