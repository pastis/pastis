#pragma once

enum PipelineLocation {
    DISPATCHER = 0,
    OPERAND_COLLECTOR = 1,
    BUFFER = 2,
    EXECUTION = 3,
    COMMIT = 4,
};
