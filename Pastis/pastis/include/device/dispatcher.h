#pragma once

#include <cstdint>

#include <array>

#include "schedule/instruction_slot.h"

#include "device/constants.h"

struct Dispatcher {

    std::array<InstructionSlot, WARPS_PER_SMP> stations;
    Dispatcher();
    InstructionSlot& operator[](size_t idx);
};
