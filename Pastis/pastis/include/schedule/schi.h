#pragma once

#include <stdint.h>

#include "nvidia.h"

#include "grid/warp.h"

#define SCHED_INST_DATA_PER_INSTRUCTION 6

struct SchiField {
    uint32_t remaining_data;
    uint8_t barrier;
    size_t anti_dep;
    size_t true_dep;
    bool yield_hint_flag;
    uint32_t number_of_cycles;

    SchiField(Instruction& instr, int base_index);
    SchiField(Warp* warp, int delta = 0);

    void fillFromInstruction(Instruction& instr, size_t base_index);
    void fillFromPC(Warp* warp, uint64_t delta);
    void print();
};