#pragma once

#include "nvidia.h"

#include "grid/types.h"


struct Instruction {
    nvidia_inst_t* instr;
    Instruction(Warp* warp, uint64_t pc);
    ~Instruction();
};


