#pragma once

#include "nvidia.h"

#include "grid/types.h"

#include "device/pipeline_location.h"

#include "device/unit.h"

struct InstructionSlot {
    // keep tracking the warp of this instruction to update the scoreboard
    
    size_t id_instruction;
    
    Warp* warp;
    
    Unit unit;
    
    PipelineLocation location;
    
    // latency due to the execution of the instruction useful when using unit other than core (LSU, Double precision, SFU)
    uint64_t instruction_latency;
    uint64_t dispatch_latency;
    uint64_t operand_collection_latency;
    
    size_t barrier_1;
    size_t barrier_2;
    
    bool valid;

    InstructionSlot();

    void fill(uint64_t id, Warp* warp, Unit unit, size_t b1, size_t b2, uint32_t instruction_latency, uint32_t dispatch_latency,bool valid = true);
    void reset();
    void decWARBarrier();
    void decRAWBarrier();

    InstructionSlot dispatch();
    InstructionSlot operand_collection_done();

    void commit();

    void stepCycle();

};