#pragma once

#include <array>
#include <memory>

#include "device/constants.h"
#include "schedule/instruction_slot.h"

template <size_t N>
struct CircularBuffer {
    std::array<InstructionSlot, N> buffer;
    size_t start, end, counter;

    CircularBuffer()
        : start { 0 }
        , end { 0 }
        , counter { 0 } {};

    bool is_full()
    {
        return this->counter == N;
    }

    void remove_front()
    {
        this->front().reset();
        this->start = (this->start + 1) % N;
        this->counter--;
    }

    InstructionSlot& front()
    {
        return this->buffer[this->start];
    }

    InstructionSlot& insert_back(InstructionSlot& is)
    {
        this->counter++;
        InstructionSlot& buffer_slot = this->buffer[this->end];
        buffer_slot = is;
        buffer_slot.valid = true;
        this->end = (this->end + 1) % N;
        return buffer_slot;
    }
};
