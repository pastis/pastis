#pragma once

template <typename T>
struct Bascule {
    T in;
    T out;

    void stepCycle()
    {
        out = in;
    }
};