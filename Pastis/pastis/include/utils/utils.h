#pragma once

#include <cstdint>

#include <trace.h>


template <class T>
consteval size_t bits()
{
    return sizeof(T) * 8;
}

template <class T>
void printBinary(T val)
{
    for (size_t i = 0; i < bits<T>(); i++) {
        int shift = (bits<T>() - 1 - i);
        info("%d", (val & (1 << shift)) >> shift);
    }
}

template <class T>
T ror(T value, size_t count)
{
    return (value >> count) | (value << (bits<T>() - count));
}

// Counting leading zeros
template <class T>
size_t clz(T value)
{
    T count = 0;
    for (T i = 0; i < bits<T>(); i++, count++) {
        if ((value >> i) & 1)
            break;
    }
    return count;
}

// Counting leading ones
template <class T>
size_t clo(T value)
{
    size_t count = 0;
    for (size_t i = 0; i < bits<T>(); i++, count++) {
        if (!((value >> i) & 1))
            break;
    }
    return count;
}

template <class T>
T setBit(T value, size_t n)
{
    return value | (1 << n);
}

template <class T>
T disableBit(T value, size_t n)
{
    return value & ~(1 << n);
}

template <class T>
bool bitIsSet(T value, size_t n)
{
    return value & (1 << n);
}