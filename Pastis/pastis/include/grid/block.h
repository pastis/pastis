#pragma once

#include <bitset>
#include <array>
#include <memory>

#include "nvidia.h"

#include "grid/types.h"

#include "grid/constants.h"
#include "grid/warp.h"

#include "pastis/dim.h"
#include "simulator.h"


struct Block {

    std::bitset<32> valid_warp;
    uint64_t memory_bar;
    ExecutionState state;
     // keep the pointer to free the memory once the thread is ended
    Block *next;

    std::array<std::unique_ptr<Warp>, MAX_THREAD_BLOCK_SIZE / THREADS_PER_WARPS> warps{}; // all warp from this blocks

    Block(dim3 block_coord, dim3 blockDim, Simulator &simulator);
};

