#pragma once 

#include <memory>
#include <list>
#include "pastis/dim.h"

#include "simulator.h"

#include "grid/state.h"
#include "grid/block.h"


struct Grid
{

    ExecutionState state;
    std::list<std::unique_ptr<Block>> blocks;
    Grid(dim3 gridDim, dim3 blockDim, Simulator &simulator);
};