
#pragma once

#include <array>

#include "grid/state.h"

#include "pastis/dim.h"

#include "simulator.h"

struct Thread
{
    std::array<char,524287> local_memory{}; // keep the pointer to free the memory once the thread is finished
    ExecutionState state;
    nvidia_sim_t* sim;
    Thread(Simulator &simulator, dim3 thread_coord, dim3 block_coord);
    ~Thread();
};
