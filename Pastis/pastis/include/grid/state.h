#pragma once

enum ExecutionState {
    RUNNING,
    WAITING,
    MEM_BAR,
    DONE
};
