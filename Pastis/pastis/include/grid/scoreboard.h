#pragma once

#include <cstdint>

#include <array>

#include "nvidia.h"

#include "grid/constants.h"

struct ScoreboardEntry {
    uint8_t value;
    uint8_t immediate_dep_bar;
};

struct Scoreboard {

    std::array<ScoreboardEntry,SIZE_SCOREBOARD> scoreboard_entries{};
    Scoreboard();
    ScoreboardEntry& operator[](size_t idx);
};
