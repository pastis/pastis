#pragma once

#include <array>
#include <cstdbool>
#include <cstdint>
#include <memory>
#include <vector>

#include "grid/scoreboard.h"
#include "grid/thread.h"

#include "device/smp.h"
#include "device/simt_stack.h"

#include "analyzer/analyzer.h"
#include "device/constants.h"


struct Warp {

    size_t id_in_smp;
    uint64_t sched_inst_latency;
    uint64_t instruction_latency;
    uint64_t additional_latency;
    uint64_t dispatch_latency;
    size_t num_threads;
    uint32_t starting_mask;
    bool is_busy;
    bool valid;

    // Useful information for the scheduler
    Scoreboard scoreboard;
    ExecutionState state;

    uint64_t* memory_bar;
    SimtStack* stack;

    size_t deb_bar;

    std::array<std::unique_ptr<Thread>, THREADS_PER_WARPS> threads;

    Warp(Scoreboard** sb, uint64_t* memory_bar);

    void activeThread(uint8_t i);

    void setStack(SimtStack *stack);

    void updateScoreboard(size_t true_dep, size_t anti_dep);

    void showNextInst();

    void memBarPut();

    void memBarPop();

    void specialFunction();

    bool scoreboardBlocked();

    bool depBarBlocked();

    nvidia_address_t nextPC(uint64_t seq_pc, uint64_t branch_pc, uint64_t sync_pc, uint64_t ssy_address, uint32_t div_mask, bool sync);

    nvidia_address_t step(Analyzer& analyzer, SMP* smp);

    nvidia_state_t* getState();

    nvidia_inst_t* getInst(uint64_t delta);
};