#pragma once

#include <cstdint>

#include "caches/tag_entry.h"

#include "caches/cache.h"

struct DirectMapped : Cache {
  explicit DirectMapped(CacheConfig &config);
  ~DirectMapped() override;
  std::tuple<CacheAnswer,TagEntry,TagEntry*> hit(uint64_t address) override;
};