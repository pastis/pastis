#pragma once

#include "caches/cache.h"

struct PseudoLRU : Cache {
    std::vector<std::vector<bool>> trees;
    size_t tree_depth;
    // Get the index in the cache set from the PLRU tree
    size_t getIndex(std::vector<bool>& tree);
    // next function useful when a miss happen because it reverses all bits in the path
    void next(std::vector<bool>& tree);
    // update function useful when a hit happen, it updates the path to the line specified in parameter
    void update(std::vector<bool>& tree, size_t line);

    explicit PseudoLRU(CacheConfig& config);
    ~PseudoLRU() override;
    std::tuple<CacheAnswer, TagEntry, TagEntry*> hit(uint64_t address) override;
};
