#pragma once


#include <cstdint>
#include <memory>
#include <vector>

#include <tuple>

#include "tag_entry.h"

#include "config.h"


enum CacheAnswer {
    Hit,
    Miss
};

enum CacheLevel {
    L1,
    L2
};


struct Cache {

    size_t cache_size; 
    size_t line_size;

    size_t associativity;
    size_t nb_lines;
    size_t nb_sets;
    size_t index_width;
    size_t offset_width;
    std::vector<TagEntry> tag_entries;

    explicit Cache(CacheConfig& config);

    virtual ~Cache() = default;
    virtual std::tuple<CacheAnswer,TagEntry,TagEntry*>  hit(uint64_t address) = 0;

    [[nodiscard]] int index(uint64_t address) const;

    static std::unique_ptr<Cache> cacheFactory(CacheConfig& config);
};