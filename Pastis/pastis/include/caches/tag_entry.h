#pragma once

#include <cstdint>

struct TagEntry
{

    uint64_t age;
    bool dirty;
    bool valid;
    bool evict_first;
    uint64_t address;
    uint64_t tag;


    TagEntry();
    void invalid();
    bool isValid();
    void markDirty();
    bool isDirty();
    void resetAge();
    void incAge();
    void setEntry(uint64_t tag,uint64_t address,bool evictFirst = false);
};