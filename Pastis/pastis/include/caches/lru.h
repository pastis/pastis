#pragma once

#include <cstdint>
#include <tuple>

#include "caches/cache.h"

struct LRU : Cache {
    explicit LRU(CacheConfig& config);
    ~LRU() override;
    std::tuple<CacheAnswer, TagEntry, TagEntry*> hit(uint64_t address) override;
};