#include "config.h"

Tx2Config::Tx2Config(bool trace,uint32_t trace_level, SimMode mode)
    : Config()
{
    this->trace_enable = trace;
    this->trace_level = trace_level;
    this->sim_mode = mode;
    this->device_config.l2_cache.algorithm = CLRU;
    this->device_config.l2_cache.associativity = 128;
    this->device_config.l2_cache.cache_size = 524288;
    this->device_config.l2_cache.line_size = 64;
    this->device_config.sm_config.op_cache.algorithm = DIRECT_MAPPED;
    this->device_config.sm_config.op_cache.associativity = 1;
    this->device_config.sm_config.op_cache.cache_size = 8192;
    this->device_config.sm_config.op_cache.line_size = 32;
    this->device_config.sm_config.l1_cache.algorithm = CLRU;
    this->device_config.sm_config.l1_cache.associativity = 128;
    this->device_config.sm_config.l1_cache.cache_size = 24576;
    this->device_config.sm_config.l1_cache.line_size = 32;
    this->device_config.sm_config.i_cache.algorithm = DIRECT_MAPPED;
    this->device_config.sm_config.i_cache.associativity = 1;
    this->device_config.sm_config.i_cache.cache_size = 24576;
    this->device_config.sm_config.i_cache.line_size = 256;
    // this->device_config.sm_config.smp_config.scheduling_algorithm = STANDARD;
}