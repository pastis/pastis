#include <iostream>
#include <regex>

#include "grid/block.h"
#include "grid/warp.h"

#include "schedule/instruction.h"

#include "commands/manager.h"
#include "utils/utils.h"

#include "trace.h"

template <typename T>
T get_value(const std::ssub_match pm, int base)
{
    return (T)stol(pm.str(), nullptr, base);
}

inline size_t get_base(const std::ssub_match pm)
{
    std::string tmp = pm.str();
    if (!tmp.empty()) {
        if ((tmp[0] == '0') && (tmp[2] == 0)) {
            if ((tmp[1] == 'x') || (tmp[2] == 'X'))
                return (16);
            if ((tmp[1] == 'b') || (tmp[2] == 'B'))
                return (2);
        }
    }
    return (10);
}

//--------------------------------------------------------------
//---------------- initCommandManager --------------------------
//--------------------------------------------------------------

CommandManager::CommandManager(SimMode mode)
{
    this->state = mode;
    this->watched_thread = 0;
    this->breakpoint_enabled = false;
}
//--------------------------------------------------------------
//-------------------- nextCommand -----------------------------
//--------------------------------------------------------------

bool CommandManager::nextCommand(Warp* warp, SMP* smp)
{
    std::string command;
    std::smatch pieces_match;

    bool return_value = false;

    SimtStack& stack = smp->stacks[warp->id_in_smp];

    if ((stack.lastNpc() == this->breakpoint) && this->breakpoint_enabled) {
        this->state = DEBUG;
        this->breakpoint_enabled = false;
    }
    if (this->state == FAST)
        return true;
    info("SM[%ld] SMP[%ld]:\n", smp->sm_index, smp->index);
    warp->showNextInst();
    std::cout << ">";
    getline(std::cin, command);

    if (command.empty())
        command = this->old_command;
    else
        this->old_command = command;

    if (std::regex_match(command, help) || command.empty()) {
        printf("Available commands:\n");
        printf("\th\thelp -- get list of commands\n");
        printf("\tq\tquit\n");
        printf("\ts\tstep -- execute one instruction in the current warp ( with the timing module step one cycle )\n");
        printf("\tsi\tStep to the Next PC -- execute one instruction in the current warp\n");
        printf("\tf\tfast -- execute fast until the end\n");
        printf("\tf 0x<address>\tfast to -- execute fast until address <address>\n");
        printf("\tw<n>\twatch -- watch thread n (w%d to watch all active threads)\n", THREADS_PER_WARPS);
        printf("\tr<n>\tread register R[n] for watched thread(s)\n");
        printf("\tr<n> = <val>\twrite val into register R[n] for the watched thread (must be unique)\n");
        printf("\tsr<n>\tread register SR[n] for watched thread(s)\n");
        printf("\tsr<n> = <val>\twrite val into register SR[n]\n");
        printf("\tp\tread predicate registers\n");
        printf("\tr<n> = <val>\twrite val into register P[n]\n");
        printf("\tcc\tread the condition code register CC\n");
        printf("\tOF|CF|SF|ZF = <val>\twrite val into condition flag OF or CF or SF pr ZF\n");
        printf("\tc 0x<bank>,0x<offset>\tread constant memory\n");
        printf("\tc 0x<bank>,0x<offset> = <val>\twrite constant memory\n");
        printf("\tg 0x<address>\tread main memory from address <address>\n");
        printf("\tg r<n> <size>\tread main memory from address R[<n>]-R[<n+1>]\n");
        printf("\tm simt\tread SIMT state: current mask and SIMT stack\n");
        printf("\tm rpc\tread rpc (re convergence PCs) stack\n");
        printf("\tshared 0x<address>\tshow shared memory\n");
    }

    if (std::regex_match(command, quit)) {
        this->state = QUIT;
    } else if (std::regex_match(command, step)) {
        return_value = true;
    } else if (std::regex_match(command, sbr)) {
        for (size_t i = 0; i < warp->scoreboard.scoreboard_entries.size(); i++) {
            printf("\tSB%ld = %d\n", i, warp->scoreboard.scoreboard_entries[i].value);
        }
    } else if (std::regex_match(command, pieces_match, fast_to)) {
        this->state = FAST;
        this->breakpoint_enabled = true;
        this->breakpoint = get_value<uint32_t>(pieces_match[1], 16);
        printf("\tfast to 0x%lx\n", this->breakpoint);
    } else if (std::regex_match(command, fast)) {
        this->state = FAST;
    } else if (std::regex_match(command, mask_stack)) {
        printf("\tSIMT mask: ");
        printBinary(stack.lastMask());
        printf("\t(%d active threads)\n", std::popcount(stack.lastMask()));
        printf("\tSIMT stack:\n");
        printf("\t#\tmask\tnpc\trpc\n");
        for (int8_t i = 0; i <= stack.tos; i++) {
            SimtStackEntry& entry = stack[i];
            printf("\t%d\t", i);
            printBinary(entry.mask);
            printf("\t%016lx", entry.npc);
            printf("\t%016lx\n", entry.rpc);
        }
    } else if (std::regex_match(command, rpc)) {
        if (this->watched_thread < THREADS_PER_WARPS) {
            Thread* thread = warp->threads[this->watched_thread].get();
            uint32_t rpc_index = thread->sim->state->RPC_INDEX;
            printf("\t[thread %ld] RPC_INDEX =%d\n", this->watched_thread, rpc_index);
            printf("\t\t\tRPC = \n");
            for (size_t i = 0; i <= rpc_index; i++)
                printf("\t\t\t%016lx\n", thread->sim->state->RPC[i]);
        } else {
            printf("\tError: first select a thread with the watch command\n");
        }
    } else if (std::regex_match(command, pieces_match, watch)) {
        uint32_t num = get_value<uint32_t>(pieces_match[1], 10);
        if (num > THREADS_PER_WARPS)
            num = THREADS_PER_WARPS;
        this->watched_thread = num;
    } else if (std::regex_match(command, pieces_match, sr_write)) {
        if (this->watched_thread < THREADS_PER_WARPS) {
            uint32_t num = get_value<uint32_t>(pieces_match[1], 10);
            size_t base = get_base(pieces_match[2]);
            uint32_t val = get_value<uint32_t>(pieces_match[3], base);
            Thread* thread = warp->threads[this->watched_thread].get();
            thread->sim->state->SR[num] = val;
            printf("\t[thread %ld] SR%d=0x%08x (%d)\n", this->watched_thread, num,
                (uint32_t)thread->sim->state->SR[num], (uint32_t)thread->sim->state->SR[num]);
        } else
            printf("\tError: first select a thread with the watch command\n");
    } else if (std::regex_match(command, pieces_match, sr_read)) {
        auto num = get_value<uint32_t>(pieces_match[1], 10);
        if (this->watched_thread < THREADS_PER_WARPS) {
            Thread* thread = warp->threads[this->watched_thread].get();
            printf("\t[thread %ld] SR%d=0x%08x (%d)\n", this->watched_thread, num,
                (uint32_t)thread->sim->state->SR[num], (uint32_t)thread->sim->state->SR[num]);
        } else {
            uint32_t mask = stack.lastMask();
            size_t index = 0;
            while (mask) {
                if (mask & 1) {
                    Thread* thread = warp->threads[index].get();
                    printf("\t[thread %ld] SR%d=0x%08x (%d)\n", index, num,
                        (uint32_t)thread->sim->state->SR[num], (uint32_t)thread->sim->state->SR[num]);
                }
                index++;
                mask >>= 1;
            }
        }
    } else if (std::regex_match(command, pieces_match, r_write)) {
        if (this->watched_thread < THREADS_PER_WARPS) {
            uint32_t num = get_value<uint32_t>(pieces_match[1], 10);
            size_t base = get_base(pieces_match[2]);
            uint32_t val = get_value<uint32_t>(pieces_match[3], base);
            Thread* thread = warp->threads[this->watched_thread].get();
            thread->sim->state->R[num] = val;
            printf("\t[thread %ld] R%d=0x%08x (%d)\n", this->watched_thread, num,
                thread->sim->state->R[num], thread->sim->state->R[num]);
        } else
            printf("\tError: first select a thread with the watch command\n");
    } else if (std::regex_match(command, pieces_match, r_read)) {
        auto num = get_value<uint32_t>(pieces_match[1], 10);
        if (this->watched_thread < THREADS_PER_WARPS) {
            Thread* thread = warp->threads[this->watched_thread].get();
            printf("\t[thread %ld] R%d=0x%08x (%d)\n", this->watched_thread, num,
                thread->sim->state->R[num], thread->sim->state->R[num]);
        } else {
            uint32_t mask = stack.lastMask();
            size_t index = 0;
            while (mask) {
                if (mask & 1) {
                    Thread* thread = warp->threads[index].get();
                    printf("\t[thread %ld] R%d=0x%08x (%d)\n", index, num,
                        thread->sim->state->R[num], thread->sim->state->R[num]);
                }
                index++;
                mask >>= 1;
            }
        }
    } else if (std::regex_match(command, pieces_match, p_write)) {
        if (this->watched_thread < THREADS_PER_WARPS) {
            uint32_t num = get_value<uint32_t>(pieces_match[1], 10);
            uint32_t val = get_value<uint32_t>(pieces_match[2], 10);
            Thread* thread = warp->threads[this->watched_thread].get();
            thread->sim->state->P[num] = val;
            printf("\t[thread %ld] P0-7 = ", this->watched_thread);
            for (size_t i = 0; i < 7; i++) {
                printf("%d ", thread->sim->state->P[i]);
            }
            printf("%%\n");
        } else
            printf("\tError: first select a thread with the watch command\n");
    } else if (std::regex_match(command, p_read)) {
        if (this->watched_thread < THREADS_PER_WARPS) {
            Thread* thread = warp->threads[this->watched_thread].get();
            printf("\t[thread %ld] P0-7 = ", this->watched_thread);
            for (size_t i = 0; i < 7; i++) {
                printf("%d ", thread->sim->state->P[i]);
            }
            printf("%%\n");
        } else {
            uint32_t mask = stack.lastMask();
            size_t index = 0;
            while (mask) {
                if (mask & 1) {
                    Thread* thread = warp->threads[index].get();
                    printf("\t[thread %ld] P0-7 = ", index);
                    for (size_t i = 0; i < 7; i++) {
                        printf("%d ", thread->sim->state->P[i]);
                    }
                    printf("%%\n");
                }
                index++;
                mask >>= 1;
            }
        }
    } else if (std::regex_match(command, cc_read)) {
        if (this->watched_thread < THREADS_PER_WARPS) {
            Thread* thread = warp->threads[this->watched_thread].get();
            printf("\t[thread %ld] OF=%d\tCF=%d\tSF=%d\tZF=%d\n", this->watched_thread,
                (thread->sim->state->CC[3]),
                (thread->sim->state->CC[2]),
                (thread->sim->state->CC[1]),
                (thread->sim->state->CC[0]));
        } else {
            uint32_t mask = stack.lastMask();
            size_t index = 0;
            while (mask) {
                if (mask & 1) {
                    Thread* thread = warp->threads[index].get();
                    printf("\t[thread %ld] OF=%d\tCF=%d\tSF=%d\tZF=%d\n", index,
                        (thread->sim->state->CC[3]),
                        (thread->sim->state->CC[2]),
                        (thread->sim->state->CC[1]),
                        (thread->sim->state->CC[0]));
                }
                index++;
                mask >>= 1;
            }
        }
    } else if (std::regex_match(command, pieces_match, of_write)) {
        if (this->watched_thread < THREADS_PER_WARPS) {
            Thread* thread = warp->threads[this->watched_thread].get();
            uint32_t val = get_value<uint32_t>(pieces_match[1], 10);
            thread->sim->state->CC[3] = (val) ? 1 : 0;

            printf("\tOF=%d\tCF=%d\tSF=%d\tZF=%d\n",
                (thread->sim->state->CC[3]),
                (thread->sim->state->CC[2]),
                (thread->sim->state->CC[1]),
                (thread->sim->state->CC[0]));
        } else
            printf("\tError: first select a thread with the watch command\n");
    } else if (std::regex_match(command, pieces_match, cf_write)) {
        if (this->watched_thread < THREADS_PER_WARPS) {
            Thread* thread = warp->threads[this->watched_thread].get();
            uint32_t val = get_value<uint32_t>(pieces_match[1], 10);
            thread->sim->state->CC[2] = (val) ? 1 : 0;

            printf("\tOF=%d\tCF=%d\tSF=%d\tZF=%d\n",
                (thread->sim->state->CC[3]),
                (thread->sim->state->CC[2]),
                (thread->sim->state->CC[1]),
                (thread->sim->state->CC[0]));
        } else
            printf("\tError: first select a thread with the watch command\n");
    } else if (std::regex_match(command, pieces_match, sf_write)) {
        if (this->watched_thread < THREADS_PER_WARPS) {
            Thread* thread = warp->threads[this->watched_thread].get();
            uint32_t val = get_value<uint32_t>(pieces_match[1], 10);
            thread->sim->state->CC[1] = (val) ? 1 : 0;

            printf("\tOF=%d\tCF=%d\tSF=%d\tZF=%d\n",
                (thread->sim->state->CC[3]),
                (thread->sim->state->CC[2]),
                (thread->sim->state->CC[1]),
                (thread->sim->state->CC[0]));
        } else
            printf("\tError: first select a thread with the watch command\n");
    } else if (std::regex_match(command, pieces_match, zf_write)) {
        if (this->watched_thread < THREADS_PER_WARPS) {
            Thread* thread = warp->threads[this->watched_thread].get();
            uint32_t val = get_value<uint32_t>(pieces_match[1], 10);
            thread->sim->state->CC[0] = (val) ? 1 : 0;

            printf("\tOF=%d\tCF=%d\tSF=%d\tZF=%d\n",
                (thread->sim->state->CC[3]),
                (thread->sim->state->CC[2]),
                (thread->sim->state->CC[1]),
                (thread->sim->state->CC[0]));
        } else
            printf("\tError: first select a thread with the watch command\n");
    } else if (std::regex_match(command, pieces_match, g_mem_read)) {
        auto sub_match_ptr = pieces_match[1];
        uint32_t* ptr = get_value<uint32_t*>(sub_match_ptr, 16);
        uint32_t val = *ptr;
        printf("\tM[%p] = 0x%8x (%d)\n", (void*)ptr, val, val);
    } else if (std::regex_match(command, pieces_match, g_mem_read_r)) {
        auto num = get_value<uint32_t>(pieces_match[1], 10);
        if (num < 254) {
            Thread* thread = warp->threads[this->watched_thread].get();
            uint32_t* ptr = (uint32_t*)((uint64_t)thread->sim->state->R[num] | (uint64_t)((uint64_t)thread->sim->state->R[num + 1] << 32));
            uint32_t val = *ptr;
            printf("\tM[R[%d]] =  M[%p] = 0x%08x (%d)\n", num, (void*)ptr, val, val);
        } else {
            std::cerr << "\tRegister index should be lower or equal to 253 [0-253]" << std::endl;
        }
    } else if (std::regex_match(command, pieces_match, c_mem_write)) {
        uint32_t bank = get_value<uint32_t>(pieces_match[1], 16);
        uint32_t offset = get_value<uint32_t>(pieces_match[2], 16);
        size_t base = get_base(pieces_match[3]);
        uint32_t val = get_value<uint32_t>(pieces_match[4], base);
        uint32_t* ptr = (uint32_t*)(warp->threads[0]->sim->state->C + bank * 32 + offset);
        *ptr = val;
        printf("C[0x%x][0x%x]=%08x (%d)\n", bank, offset, val, val);
    } else if (std::regex_match(command, pieces_match, c_mem_read)) {
        uint32_t bank = get_value<uint32_t>(pieces_match[1], 16);
        uint32_t offset = get_value<uint32_t>(pieces_match[2], 16);
        uint32_t* ptr = (uint32_t*)(warp->threads[0]->sim->state->C + bank * 32 + offset);
        printf("C[0x%x][0x%x]=%08x (%d)\n", bank, offset, *ptr, *ptr);
    } else if (std::regex_match(command, pieces_match, s_mem_read)) {
        uint64_t address = get_value<uint64_t>(pieces_match[1], 16);
        uint8_t* data_address = (uint8_t*)(warp->getState()->S) + address;
        uint8_t v8 = *data_address;
        uint16_t v16 = *(uint16_t*)(data_address);
        uint32_t v32 = *(uint32_t*)(data_address);
        uint64_t v64 = *(uint64_t*)(data_address);
        printf("Shared[0x%lu] :\n", address);
        printf("uint8_t\t=\t%u\n", v8);
        printf("uint16_t\t=\t%u\n", v16);
        printf("uint32_t\t=\t%u\n", v32);
        printf("uint64_t\t=\t%lu\n", v64);
    } else if (std::regex_match(command, clk)) {
        uint64_t clk = smp->clock;
        printf("SM[%lu].SMP[%lu] clock = %lu\n", smp->sm_index, smp->index, clk);
    } else {
        printf("command unknown ... use 'h' to get the list of commands\n");
    }
    return return_value;
}
