#include <cstdio>

#include <cstdarg>

#include "config.h"
#include "trace.h"

bool trace_enable = true;
uint32_t trace_level = INFO;

void init_trace(Config& config)
{
    trace_enable = config.trace_enable;
    trace_level = config.trace_level;
}

void trace(uint32_t level, const char* format, va_list arg_ptr)
{
    if (trace_enable && level <= trace_level) {
        vprintf(format, arg_ptr);
    }
}

void init(const char* format, ...)
{
    va_list arg_ptr;
    va_start(arg_ptr, format);
    trace(INIT, format, arg_ptr);
    va_end(arg_ptr);
}
void info(const char* format, ...)
{
    va_list arg_ptr;
    va_start(arg_ptr, format);
    trace(INFO, format, arg_ptr);
    va_end(arg_ptr);
}
void debug(const char* format, ...)
{
    va_list arg_ptr;
    va_start(arg_ptr, format);
    trace(DEBUGING, format, arg_ptr);
    va_end(arg_ptr);
}
