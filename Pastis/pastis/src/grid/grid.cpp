#include <memory>
#include "grid/scoreboard.h"
#include "grid/thread.h"
#include "grid/warp.h"
#include "grid/block.h"
#include "grid/grid.h"
#include "pastis/dim.h"

Grid::Grid(dim3 gridDim, dim3 blockDim, Simulator &simulator) {
    this->state = RUNNING;
    for (uint32_t i = 0; i < gridDim.x; i++) {
        for (uint32_t j = 0; j < gridDim.y; j++) {
            for (uint32_t k = 0; k < gridDim.z; k++) {
                dim3 blockCoord {i, j, k};
                this->blocks.emplace_back(std::make_unique<Block>(blockCoord, blockDim, simulator));
            }
        }
    }
}
