#include <cassert>
#include <cstdlib>

#include "device/smp.h"
#include "grid/block.h"
#include "grid/constants.h"
#include "grid/warp.h"
#include "memories/memories.h"
#include "schedule/schi.h"
#include "trace.h"
#include "utils/utils.h"

Warp::Warp(Scoreboard** sb, uint64_t* memory_bar)
{
    this->is_busy = false;
    this->num_threads = THREADS_PER_WARPS;
    this->state = RUNNING;
    this->id_in_smp = 0;
    this->valid = true;
    this->sched_inst_latency = 0;
    this->deb_bar = 7;
    for (std::unique_ptr<Thread>& thread : this->threads) {
        thread = nullptr;
    }
    this->starting_mask = 0;
    this->memory_bar = memory_bar;
    *sb = &(this->scoreboard);
}

void Warp::activeThread(uint8_t i)
{
    this->starting_mask = setBit(this->starting_mask, i);
}

nvidia_state_t* Warp::getState()
{
    nvidia_state_t* executed_state = nullptr;
    if (this->stack->isEmpty())
        return executed_state;

    uint32_t mask = this->stack->lastMask();
    if (mask) {
        for (size_t i = 0; i < THREADS_PER_WARPS; i++) {
            if ((mask >> i & 0x1) == 1) {
                if (this->threads[i]->sim->state->EXECUTED == 1) {
                    return this->threads[i]->sim->state;
                }
                executed_state = this->threads[i]->sim->state;
            }
        }
    }
    return executed_state;
}

void Warp::setStack(SimtStack* stack)
{
    this->stack = stack;
}

nvidia_inst_t* Warp::getInst(uint64_t pc)
{
    uint32_t mask = this->stack->lastMask();
    if (mask) {
        size_t t = 0;
        while ((mask & 1) == 0) {
            mask = mask >> 1;
            t++;
        }
        nvidia_sim_t* sim = this->threads[t]->sim;
        return nvidia_decode(sim->decoder, pc);
    }
    return nullptr;
}

void Warp::updateScoreboard(size_t true_dep, size_t anti_dep)
{
    Scoreboard& sb = this->scoreboard;
    if (true_dep != 7)
        sb[true_dep].value += 1;

    if (anti_dep != 7)
        sb[anti_dep].value += 1;
}

nvidia_address_t Warp::step(Analyzer& analyzer, SMP* smp)
{
    uint64_t npc, rpc, seq_pc, thread_npc, sync_pc = 0, branch_pc = 0, ssy_address = 0;
    uint32_t mask, tmp_mask, div_mask = 0;
    bool sync = 0;
    bool bra = 0;

    // Read current information
    this->stack->readTop(&npc, &rpc, &mask);
    // else if the warp is not "busy" seq_pc = npc + 8
    seq_pc = npc + 8;
    // process every active thread;
    tmp_mask = mask;
    // When we step a warp we reset teh additional latency
    this->additional_latency = 0;

    this->instruction_latency = 0;
    for (size_t i = 0; i < THREADS_PER_WARPS; i++) {
        if (tmp_mask & (1 << i)) { // thread t is active
            Thread* thread = this->threads[i].get();
            nvidia_state_t* thread_state = thread->sim->state;
            *(uint64_t*)(thread_state->SR + 80) = smp->clock;
            assert(nvidia_next_addr(thread->sim) == npc);
            nvidia_step(thread->sim);
            if (thread_state->DONE) {
                this->stack->markThreadDone(i);
            } else {
                thread_npc = thread_state->PC;
                bra |= thread_state->BRA;
                if (thread_state->SYNC) {
                    sync = true;
                    if (sync_pc != 0) {
                        assert(sync_pc == thread_npc);
                    } else {
                        sync_pc = thread_npc;
                    }
                    div_mask = div_mask | (1 << i);
                } else {
                    ssy_address = thread_state->SSY_ADDR;
                    if (thread_state->BAR_SYNC) {
                        this->state = WAITING;
                    } else if (thread_state->MEMBAR) {
                        this->state = MEM_BAR;
                    } else if (thread_state->DEPBAR != 7) {
                        this->deb_bar = thread_state->DEPBAR;
                        this->scoreboard.scoreboard_entries[this->deb_bar].immediate_dep_bar = thread_state->DEPBAR_IMM;
                    } else if (thread_npc != seq_pc) {
                        if (branch_pc != 0) {
                            assert(thread_npc == branch_pc);
                        } else {
                            branch_pc = thread_npc;
                        }
                        div_mask = div_mask | (1 << i);
                    }
                }
            }
        }
    }

    if (bra) {
        this->additional_latency = 6 + (branch_pc % 64) / 16;
    }

    this->specialFunction();
    smp->parent_device->memories.access(this, smp, analyzer);
    analyzer.postAnalysis(this, smp);
    return this->nextPC(seq_pc, branch_pc, sync_pc, ssy_address, div_mask, sync);
}

nvidia_address_t Warp::nextPC(uint64_t seq_pc, uint64_t branch_pc,
    uint64_t sync_pc, uint64_t ssy_address,
    uint32_t div_mask, bool sync)
{

    uint64_t npc, rpc;
    uint32_t mask;
    this->stack->readTop(&npc, &rpc, &mask);

    if (mask == 0) {
        info("top mask is empty\n");
        npc = this->stack->pop();
        if (npc == UINT64_MAX) { // SIMT stack is empty
            info("warp is done\n");
            this->state = DONE;
            return -1;
        }
        return npc;
    }

    if (ssy_address != 0) {
        info("ssy executed\n");
        this->stack->writeTop(ssy_address, rpc, mask);
        this->stack->push(seq_pc, ssy_address, mask);
        return seq_pc;
    }

    if (sync) {
        info("sync executed\n");
        info("mask=");
        printBinary(mask);
        info(" - div_mask=");
        printBinary(div_mask);
        info("\n");
        if (mask != div_mask) // Divergence
        {
            this->stack->pop();
            this->stack->push(seq_pc, sync_pc, mask & ~div_mask);
            return seq_pc;
        } else {
            return this->stack->pop(); // return le last pc pushed
        }
    }

    if (branch_pc) {
        info("branch executed\n");
        if (div_mask == mask) {
            info("all active threads take the branch\n");
            // all active threads take the branch = no divergence
            this->stack->writeTop(branch_pc, rpc, mask);
        } else {
            // active threads diverge
            info("active threads diverge\n");
            this->stack->writeTop(seq_pc, rpc, mask & ~div_mask);
            this->stack->push(branch_pc, rpc, div_mask);
        }
        return branch_pc;
    }

    // default: no branch, no sync, no ssy
    info("no divergence, no branch, no sync\n");
    if (seq_pc == rpc) {
        info("reached convergence point\n");
        // reached convergence point
        return this->stack->pop();
    } else {
        info("continue in to seq_pc\n");
        this->stack->writeTop(seq_pc, rpc, mask);
        return seq_pc;
    }
}

void Warp::showNextInst()
{
    char buffer[128];
    uint64_t pc = this->stack->lastNpc();
    uint32_t mask = this->stack->lastMask();
    size_t t = 0;
    while ((mask & (1 << t)) == 0) {
        t++;
    }
    nvidia_inst_t* inst = nvidia_next_inst(this->threads[t]->sim);
    nvidia_disasm(buffer, inst);
    info("PC=%016lx(%ld): %s\n", pc, pc, buffer);
    nvidia_free_inst(inst);
}

void Warp::memBarPut()
{
    this->memory_bar++;
}

void Warp::memBarPop()
{

    this->memory_bar -= (this->memory_bar != 0) ? 1 : 0;
}
void Warp::specialFunction()
{
    uint32_t executed_mask = this->stack->lastMask();
    for (size_t i = 0; i < THREADS_PER_WARPS; i++) {
        if ((1 << i) & executed_mask) {
            Thread* thread = this->threads[i].get();
            if (thread) {
                nvidia_state_t* state = thread->sim->state;
                if (state->VOTE_ANY)
                    state->R[state->REG] = executed_mask;
            }
        }
    }
}

bool Warp::scoreboardBlocked()
{
    SchiField sched_inst(this, 8);
    Scoreboard& sb = this->scoreboard;
    bool blocked = false;
    for (size_t i = 0; i < SIZE_SCOREBOARD; i++) {
        ScoreboardEntry& sb_entry = sb[i];
        blocked |= ((sb_entry.value != 0) && ((sched_inst.barrier >> i) & 0x1));
    }
    return blocked;
}

bool Warp::depBarBlocked()
{
    bool blocked = false;
    if (this->deb_bar != 7) {
        ScoreboardEntry& sb_entry = this->scoreboard[this->deb_bar];
        blocked = sb_entry.value > sb_entry.immediate_dep_bar;
        if (!blocked) {
            this->deb_bar = 7;
        }
    }
    return blocked;
}
