#include "grid/block.h"
#include "grid/scoreboard.h"
#include "grid/thread.h"
#include "grid/warp.h"
#include "pastis/dim.h"
#include "trace.h"
#include "utils/utils.h"

Block::Block(dim3 block_coord, dim3 blockDim, Simulator& simulator)
{
    Warp* warp = nullptr;

    Scoreboard* scoreboard_ptr;

    size_t thread_index = 0;
    size_t id_warp = 0;

    this->state = RUNNING;
    this->valid_warp = 0;
    this->memory_bar = 0;

    for (uint32_t k = 0; k < blockDim.z; k++) {
        for (uint32_t j = 0; j < blockDim.y; j++) {
            for (uint32_t i = 0; i < blockDim.x; i++) {
                dim3 threadCoord { i, j, k };
                std::unique_ptr<Thread> thread = std::make_unique<Thread>(simulator, threadCoord, block_coord);
                if (thread_index == 0) {
                    this->warps[id_warp] = std::make_unique<Warp>(&scoreboard_ptr, &this->memory_bar);
                    warp = this->warps[id_warp].get();
                    this->valid_warp |= (0x1 << id_warp);
                    id_warp++;
                }
                warp->activeThread(thread_index);
                thread->sim->state->SCOREBOARD = (uint64_t)scoreboard_ptr;
                thread->sim->state->MEMORY_BAR = (uint64_t)&this->memory_bar;

                warp->threads[thread_index] = std::move(thread);



                thread_index = (thread_index + 1) % THREADS_PER_WARPS;
            }
        }
    }
    this->next = nullptr;
}