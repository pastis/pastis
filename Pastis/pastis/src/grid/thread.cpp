#include "grid/scoreboard.h"
#include "grid/thread.h"
#include "pastis/dim.h"

Thread::Thread(Simulator &simulator, dim3 thread_coord, dim3 block_coord)  {
    nvidia_state_t *thread_state = nvidia_new_state(simulator.platform); // nvidia/src/api.c
    this->sim = nvidia_new_sim(thread_state, simulator.address_start, simulator.address_exit);
    this->state = RUNNING;
    this->sim->state->C = (uint64_t) simulator.constantMemory.data();
    this->sim->state->L = (uint64_t) this->local_memory.data();
    this->sim->state->RPC_INDEX = -1;
    this->sim->state->SR[33] = thread_coord.x;
    this->sim->state->SR[34] = thread_coord.y;
    this->sim->state->SR[35] = thread_coord.z;
    this->sim->state->SR[37] = block_coord.x;
    this->sim->state->SR[38] = block_coord.y;
    this->sim->state->SR[39] = block_coord.z;
}

Thread::~Thread() {
    if (this->sim)
        nvidia_delete_sim(this->sim);
}