// Created by michael on 12/01/2022.
#include "nvidia.h"

#include "device/sm.h"

#include "device/smp.h"

#include "schedule/instruction.h"

#include "trace.h"

#include "commands/manager.h"

#include "analyzer/analyzer.h"

#include "schedule/schi.h"

#include "utils/utils.h"

SMP::SMP(size_t index, size_t sm_index, Device* d, Cache* ic, Cache* l1_cache)
{
    this->index = index;
    this->clock = 0;
    this->sm_index = sm_index;
    this->parent_device = d;
    this->l1 = l1_cache;
    this->phase_counter = 0;
    this->ic = ic;
    this->active_warps = 0;
    this->valid_buffers.in = 0;
    this->valid_buffers.out = 0;
    this->request_latency = 0;
    this->current_warp_requested = 0;
    this->warps.fill(nullptr);
    this->buffers_pc.fill(0x0);
    this->instruction_fetched = false;
}

void SMP::affectWarp(Warp* warp)
{
    size_t index_warp = clo(this->active_warps);
    this->warps[index_warp] = warp;
    warp->setStack(&this->stacks[index_warp]);
    warp->stack->push(0, (uint64_t)-1, warp->starting_mask);
    this->active_warps = setBit(this->active_warps, index_warp);
    for (std::unique_ptr<Thread>& thread_ptr : warp->threads) {
        Thread* thread = thread_ptr.get();
        if (thread) {
            thread->sim->state->S = (uint64_t)this->parent_device->memories.shared_memory[this->sm_index];
        }
    }
}

void SMP::updateBuffer()
{
    for (size_t i = 0; i < WARPS_PER_SMP; i++) {
        bool is_warp_active = bitIsSet(this->active_warps, i) & true;
        if (is_warp_active) {
            uint64_t warp_pc = this->stacks[i].lastNpc();
            uint64_t buff_pc = this->buffers_pc[i];
            if (!(warp_pc >= buff_pc && warp_pc < (buff_pc + 0x40))) {
                this->valid_buffers.in = disableBit(this->valid_buffers.in, i);
            }
        }
    }
}

uint64_t SMP::requestWarpInstr(uint64_t pc)
{
    auto [ic_answer, old_e, new_e] = this->ic->hit(pc);
    if (ic_answer == Hit) {
        info("\tInstruction Cache L1 : Hit\n");
        return 1;
    }
    info("\tInstruction Cache L1 : Miss\n");
    Cache* l2 = this->parent_device->l2Caches[0].get();
    CacheAnswer l2_answer = std::get<0>(l2->hit(pc));
    if (l2_answer == Hit) {
        info("\tInstruction Cache L2 : Hit\n");
        return 91;
    }
    info("\tInstruction Cache L2 : Miss\n");
    return 91;
}

void SMP::fecth()
{
    if (this->request_latency == 0 && !this->instruction_fetched) {
        for (size_t i = 0; i < WARPS_PER_SMP; i++) {
            size_t phased_idx = (i + this->phase_counter) % WARPS_PER_SMP;
            bool is_active = bitIsSet(this->active_warps, phased_idx);
            bool is_valid = bitIsSet(this->valid_buffers.in, phased_idx);
            if (is_active & !is_valid) {
                info("Warp %d selected to fetch\n", phased_idx);
                uint64_t pc = this->stacks[phased_idx].lastNpc();
                this->current_warp_requested = phased_idx;
                this->request_latency = this->requestWarpInstr(pc);
                this->buffers_pc[phased_idx] = pc;
                this->instruction_fetched = true;
                break;
            }
        }
    } else {
        this->request_latency--;
    }

    if (this->request_latency == 0 && this->instruction_fetched) {

        this->valid_buffers.in = setBit(this->valid_buffers.in, this->current_warp_requested);
        this->instruction_fetched = false;
    }

    this->phase_counter = (this->phase_counter + 1) % WARPS_PER_SMP;
}

void SMP::step(Analyzer& analyzer, CommandManager& cm)
{
    Warp* warp;

    this->updateBuffer();
    this->fecth();

    if ((warp = this->nextWarp())) {
        while (!cm.nextCommand(warp, this)) { }
        warp->showNextInst();
        // Functional execution
        warp->step(analyzer, this);

        // If the warp has a schi instruction
        // so exit the function
        nvidia_state_t* state = warp->getState();
        if (state != nullptr) {
            if ((state->IADDR & 0x1F) == 0)
                return;
            // Send the instruction into a dispatch slot
            this->sendToDispatcher(warp);
        }
    }
    this->dispatch();
    this->operand_collection();
    this->buffer();
    this->commit();
    this->stepCycle();
}

void SMP::sendToDispatcher(Warp* warp)
{
    static uint64_t id = 0;
    nvidia_state_t* state = warp->getState();
    // if it's not a scheduling instruction
    if ((state->OLD_PC & 0x1F) != 0) {
        SchiField s(warp, 0);
        if (state->UNIT == LSU || state->UNIT == S2R || state->UNIT == BRA) {
            InstructionSlot& is = this->dispatcher_lsu[warp->id_in_smp];
            is.fill(id++, warp, (Unit)state->UNIT, s.true_dep, s.anti_dep, warp->instruction_latency, warp->dispatch_latency);
        } else if (state->UNIT == CUDA_CORES || state->UNIT == NO_UNIT) {
            InstructionSlot& is = this->dispatcher_art[warp->id_in_smp];
            is.fill(id++, warp, (Unit)state->UNIT, s.true_dep, s.anti_dep, warp->instruction_latency, warp->dispatch_latency);
        }
        warp->updateScoreboard(s.true_dep, s.anti_dep);
        warp->sched_inst_latency = s.number_of_cycles + warp->additional_latency;
        warp->additional_latency = 0;
        warp->dispatch_latency = 0;
        warp->instruction_latency = 0;
        if (s.number_of_cycles == 0) {
            this->clock--;
        }
    }
}

void SMP::dispatch()
{
    // part core functional unit
    for (size_t i = 0; i < WARPS_PER_SMP; i++) {
        InstructionSlot& slot = this->dispatcher_art[i];
        if (slot.valid && !this->operand_collectors_art.is_full() && slot.dispatch_latency == 0) {
            InstructionSlot n_slot = slot.dispatch();
            this->operand_collectors_art.insert_back(n_slot);
            break;
        }
    }
    // part lsu
    for (size_t i = 0; i < WARPS_PER_SMP; i++) {
        InstructionSlot& slot = this->dispatcher_lsu[i];
        if (slot.valid && !this->operand_collectors_lsu.is_full() && slot.dispatch_latency == 0) {
            InstructionSlot n_slot = slot.dispatch();
            this->operand_collectors_lsu.insert_back(n_slot);
            break;
        }
    }
}

void SMP::operand_collection()
{
    // Check if there is an instruction
    // Is this instruction done fetching his operands
    // if so remove it and place it into the execution stage
    // This is executed for both dispatcher

    InstructionSlot& slot_art = this->operand_collectors_art.front();
    if (slot_art.valid && !this->core.valid) {
        if (slot_art.operand_collection_latency == 0) {
            this->core = slot_art.operand_collection_done();
            this->operand_collectors_art.remove_front();
        } else {
            info("Instruction CUDA CORES %lu stuck in operand collector\n", slot_art.id_instruction);
        }
    }

    InstructionSlot& slot_lsu = this->operand_collectors_lsu.front();
    if (slot_lsu.valid) {
        if (slot_lsu.operand_collection_latency == 0) {
            if (!this->lsu_buffer.is_full() && slot_lsu.unit == LSU) {
                InstructionSlot is = slot_lsu.operand_collection_done();
                this->lsu_buffer.insert_back(is);
                this->operand_collectors_lsu.remove_front();
            } else if (slot_lsu.unit == S2R and !this->lsu.valid) {
                this->lsu = slot_lsu.operand_collection_done();
                this->operand_collectors_lsu.remove_front();
            } else if (slot_lsu.unit == BRA) {
                this->operand_collectors_lsu.remove_front();
            }
        }
    }
}

void SMP::buffer()
{
    // Handle the buffer
    // if the lsu is empty (does not have a valid instruction)
    if (!this->lsu.valid) {
        InstructionSlot& slot = this->lsu_buffer.front();
        // if there is a front
        if (slot.valid) {
            this->lsu = slot; //  load the output instruction of the buffer onto the lsu
            this->lsu.location = EXECUTION;
            this->lsu_buffer.remove_front();
        }
    }
}

void SMP::commit()
{
    this->core.commit();
    this->dpu.commit();
    this->lsu.commit();
}

void SMP::stepCycle()
{
    this->clock++;
    for (size_t i = 0; i < WARPS_PER_SMP; i++) {
        InstructionSlot& slot_art = this->dispatcher_art[i];
        InstructionSlot& slot_lsu = this->dispatcher_lsu[i];
        if (this->warps[i]) {
            if (!slot_art.valid && !slot_lsu.valid) {
                auto& lat = this->warps[i]->sched_inst_latency;
                if (lat > 0) {
                    lat--;
                }
            }
        }
        slot_art.stepCycle();
        slot_lsu.stepCycle();
    }
    for (InstructionSlot& is : this->operand_collectors_art.buffer) {
        is.stepCycle();
    }
    for (InstructionSlot& is : this->operand_collectors_lsu.buffer) {
        is.stepCycle();
    }

    this->core.stepCycle();
    this->lsu.stepCycle();

    this->valid_buffers.stepCycle();
}

#define couple(x) #x, x

Warp* SMP::nextWarp()
{
    debug("=== Get warp to execute on SM[%d] SMP[%d] ===\n", this->sm_index, this->index);
    for (size_t i = 0; i < WARPS_PER_SMP; i++) {
        Warp* warp = this->warps[i];
        if (warp) {
            debug("there is a warp in slot %d\n", i);
            bool is_running = warp->state == RUNNING;
            bool is_valid = warp->valid;
            bool is_buffer_valid = bitIsSet(this->valid_buffers.out, i);
            bool blocked_by_scoreboard = warp->scoreboardBlocked();
            bool blocked_by_depbar = warp->depBarBlocked();
            bool dispatcher_art_free = !this->dispatcher_art[i].valid;
            bool dispatcher_lsu_free = !this->dispatcher_lsu[i].valid;
            bool can_dispatch = warp->sched_inst_latency == 0;

            debug("%s := %d\n", couple(is_running));
            debug("%s := %d\n", couple(is_valid));
            debug("%s := %d\n", couple(is_buffer_valid));
            debug("%s := %d\n", couple(blocked_by_scoreboard));
            debug("%s := %d\n", couple(blocked_by_depbar));
            debug("%s := %d\n", couple(dispatcher_art_free));
            debug("%s := %d\n", couple(dispatcher_lsu_free));
            debug("%s := %d\n", couple(can_dispatch));
            // Check if the warp is in running state
            if (is_running && is_valid && is_buffer_valid && can_dispatch && !blocked_by_scoreboard && !blocked_by_depbar && dispatcher_art_free && dispatcher_lsu_free) {
                return warp;
            }
        }
    }
    return nullptr;
}