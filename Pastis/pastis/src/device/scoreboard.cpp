//
// Created by michael.adalbert on 29/10/2021.
//

#include <cstdint>
#include "grid/scoreboard.h"

Scoreboard::Scoreboard()
{
    for (ScoreboardEntry& entry : this->scoreboard_entries) {
        entry.immediate_dep_bar = 0;
        entry.value = 0;
    }
}

ScoreboardEntry& Scoreboard::operator[](size_t idx)
{
    return this->scoreboard_entries[idx];
}
