#include "grid/warp.h"
#include "trace.h"
#include <memory>

#include "device/constants.h"
#include "device/device.h"
#include "device/sm.h"
#include "device/smp.h"

#include "analyzer/analyzer.h"

SM::SM(size_t id, Device* d, SMConfig& config)
{
    this->op_cache = Cache::cacheFactory(config.op_cache);
    this->is_cache = Cache::cacheFactory(config.i_cache);
    this->sm_id = id;
    this->blocks = nullptr;

    for (std::unique_ptr<Cache>& l1 : this->l1_caches) {
        l1 = Cache::cacheFactory(config.l1_cache);
    }

    for (size_t i = 0; i < SMP_PER_SM; i++) {
        this->smps[i] = std::make_unique<SMP>(i, this->sm_id, d, this->is_cache.get(), this->l1_caches[i / L1_PER_SM].get());
    }
}

void SM::dispatchBlock()
{

    Block* current_block = this->blocks;
    size_t selected_smp = 0;
    if (current_block) {
        for (size_t i = 0; i < 32 && ((current_block->valid_warp[i]) != 0); i++) {
            Warp* warp = current_block->warps[i].get();
            this->smps[selected_smp]->affectWarp(warp);
            selected_smp = (selected_smp + 1) % SMP_PER_SM;
        }
    }
}

// check if all warp of the bloc are in the same state.
bool all_valid_warp_in_state(Block* b, ExecutionState state)
{
    for (size_t i = 0; i < 32; i++) {
        bool is_valid = b->valid_warp[i];
        if (is_valid) {
            Warp* warp = b->warps[i].get();
            if (warp->state != state) {
                return false;
            }
        }
    }
    return true;
}

void set_valid_warps_state(Block* b, ExecutionState state)
{
    for (size_t i = 0; i < 32; i++) {
        bool is_valid = b->valid_warp[i];
        if (is_valid) {
            b->warps[i]->state = state;
        }
    }
}

void SM::arbiter()
{

    Block* b = this->blocks;
    if (all_valid_warp_in_state(b, WAITING)) {
        info("check sync\n");
        set_valid_warps_state(b, RUNNING);
    } else if (all_valid_warp_in_state(b, MEM_BAR) && b->memory_bar == 0) {
        info("check mem bar\n");
        set_valid_warps_state(b, RUNNING);
    } else if (all_valid_warp_in_state(b, DONE)) {
        info("check done\n");
        this->blocks = b->next;
        this->dispatchBlock();
        info("%p\n", this->blocks);
    }
}

// Step a sm
void SM::step(Analyzer& analyser, CommandManager& cm)
{
    for (size_t i = 0; i < SMP_PER_SM; i++) {
        this->smps[i]->step(analyser, cm);
        analyser.stepCycle(this->sm_id * SMP_PER_SM + i);
    }
    this->arbiter();
}
