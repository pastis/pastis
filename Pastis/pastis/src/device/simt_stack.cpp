#include "device/simt_stack.h"

#include "grid/constants.h"

#include "trace.h"

#include "utils/utils.h"

SimtStack::SimtStack()
{
    tos = -1;
    for (SimtStackEntry& entry : stack) {
        entry = { 0, (uint64_t)0xffffffffffffffff, (uint32_t)((uint64_t)(1UL << THREADS_PER_WARPS) - 1) };
    }
}

bool SimtStack::isEmpty()
{
    return (this->tos == -1);
}

void SimtStack::dump()
{
    info("\t%2s\t%32s\t%16s\t%16s\n", "#", "mask", "npc", "rpc");
    for (int64_t i = 0; i <= this->tos; i++) {
        SimtStackEntry& entry = this->stack[i];
        info("\t%02d\t", i);
        printBinary(entry.mask);
        info("\t%016lx", entry.npc);
        info("\t%016lx\n", entry.rpc);
    }
}

void SimtStack::readTop(uint64_t* npc, uint64_t* rpc, uint32_t* mask)
{
    if (this->tos > -1) {
        SimtStackEntry& entry = this->stack[tos];
        *mask = entry.mask;
        *npc = entry.npc;
        *rpc = entry.rpc;
        info("read_simt_top() returns npc=%016lx, rpc=%016lx, mask=", *npc, *rpc);
        printBinary(*mask);
        info("\n");
    }
}

void SimtStack::writeTop(uint64_t npc, uint64_t rpc, uint32_t mask)
{
    if (this->tos > -1) {
        SimtStackEntry& entry = this->stack[tos];
        entry.mask = mask;
        entry.npc = npc;
        entry.rpc = rpc;
        info("write_simt_top(npc=%016lx, rpc=%016lx, mask=", npc, rpc);
        printBinary(mask);
        info("\n");
        this->dump();
    }
}

void SimtStack::push(uint64_t npc, uint64_t rpc, uint32_t mask)
{
    if (this->tos + 1 < 16) {
        this->tos += 1;
        info("push_simt() => ");
        this->stack[tos] = { 0, 0, 0 };
        this->writeTop(npc, rpc, mask);
    }
}

void SimtStack::markThreadDone(uint64_t num_thread)
{
    for (SimtStackEntry& entry : this->stack) {
        entry.mask = disableBit(entry.mask, num_thread);
    }
}

uint64_t SimtStack::pop()
{

    this->tos--;
    while ((!this->isEmpty()) ? this->stack[tos].mask == 0 : false)
        this->tos--;
    info("pop_simt()\n");
    this->dump();
    return (!this->isEmpty()) ? this->stack[tos].npc : -1;
}


uint32_t SimtStack::lastMask() {
    if(this->tos > -1) {
        return this->stack[this->tos].mask;
    }
    return 0;
}

uint64_t SimtStack::lastNpc() {
    if(this->tos > -1) {
        return this->stack[this->tos].npc;
    }
    return 0;
}

uint64_t SimtStack::lastRpc() {
    if(this->tos > -1) {
        return this->stack[this->tos].rpc;
    }
    return 0;
}