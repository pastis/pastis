#include <cstdint>

#include "grid/types.h"
#include "grid/grid.h"

#include "device/dispatcher.h"
#include "device/device.h"
#include "device/sm.h"

#include "caches/cache.h"

#include "config.h"

Device::Device(Config &config) {

    DeviceConfig &d_config = config.device_config;
    for (size_t i = 0; i < SM_PER_DEV; i++) {
        this->SMs[i] = std::make_unique<SM>(i,this, d_config.sm_config);
    }
    for (size_t i = 0; i < L2_PER_DEV; i++) {
        this->l2Caches[i] = Cache::cacheFactory(d_config.l2_cache);
    }

}

void Device::map(Grid &grid) {
    size_t i = 0;
    Block *current_block = grid.blocks.front().get(), *tmp, **last;
    while (current_block) {
        last = &this->SMs[i].get()->blocks;
        while (*last) {
            last = &(*last)->next;
        }
        *last = current_block;
        i = (i + 1) % SM_PER_DEV;
        tmp = current_block;
        current_block = current_block->next;
        tmp->next = nullptr;
    }
}

