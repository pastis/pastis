#include "nvidia.h"
#include <algorithm>
#include <array>
#include <iostream>
#include <set>

#include "analyzer/analyzer.h"
#include "grid/thread.h"
#include "grid/warp.h"
#include "memories/memories.h"
#include "memories/shared.h"
#include "trace.h"

uint32_t shared_collision(size_t group, size_t group_size, Warp* warp)
{
    uint32_t mask = warp->stack->lastMask();
    uint32_t mask_group = ((1L << group_size) - 1) << (group * group_size);
    uint32_t mask_to_do = mask & mask_group;

    std::array<std::set<uint8_t>, 32> bank_sets {};

    for (std::set<uint8_t>& e : bank_sets)
        e.clear();

    for (size_t j = group * group_size; j < group * group_size + group_size; j++) {
        if ((mask_to_do >> j) & 0x1) { // Si le thread doit être exécuté
            nvidia_state_t* thread_state = warp->threads[j]->sim->state;
            if (thread_state->EXECUTED) // si instruction exécutée
            {
                uint64_t addr = thread_state->ADDR;
                size_t bank = (addr >> 2) & 0x1f;
                bank_sets[bank].insert(addr >> 7);
            }
        }
    }

    size_t max_collision = 0;
    for (std::set<uint8_t>& bank_set : bank_sets) {
        if (bank_set.size() > max_collision) {
            max_collision = bank_set.size();
        }
    }

    return (max_collision == 0) ? max_collision : max_collision - 1;
}

unsigned int SharedMemory::access(Warp* warp, nvidia_state_t* state)
{
    info("=== SHARED MEMORY ACCESS ===\n");

    size_t group_number = nb_32bit_acceded(state->SIZE);
    size_t group_size = THREADS_PER_WARPS / group_number;
    size_t total_access = group_number;
    uint32_t cycles = 22 + ((group_size == THREADS_PER_WARPS) ? 1 : 4 * group_number);
    for (size_t i = 0; i < group_number; i++) {
        size_t collision_number = shared_collision(i, group_size, warp);
        info("Nombre de collision pour le groupe de thread %ld : %ld\n", i, collision_number);
        total_access += collision_number;
        cycles += collision_number * 2;
    }
    info("number of accesses : %d\n", total_access);

    warp->instruction_latency = cycles;
    info("=== ====== ====== ====== ===\n");
    return total_access;
}

char* SharedMemory::operator[](size_t index)
{
    return this->shared_memories[index].data();
}