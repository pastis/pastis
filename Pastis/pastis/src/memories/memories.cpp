#include "nvidia.h"
#include <cstdint>

#include "schedule/instruction.h"

#include "analyzer/analyzer.h"
#include "device/dispatcher.h"
#include "device/sm.h"
#include "device/smp.h"
#include "grid/thread.h"
#include "grid/warp.h"

#include "memories/global.h"
#include "memories/memories.h"
#include "memories/shared.h"

size_t nb_32bit_acceded(size_t size_field)
{
    switch (size_field) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
        return 1;
    case 5:
        return 2;
    case 6:
        return 4;
    default:
        return 1;
    }
}

void Memories::access(Warp* warp, SMP* smp, Analyzer& analyzer)
{
    nvidia_state_t* state = warp->getState();
    if (state) {
        MemoryType t = MemoryType(state->MEMORY_TYPE);
        uint32_t number_of_access = 0;
        if (t == Local) {
            number_of_access = this->local_memory.access(warp);
        } else if (t == Global) {
            number_of_access = this->global_memory.access(warp, analyzer, state, smp);
        } else if (t == Constant) {
            number_of_access = this->constant_memory.access(warp, analyzer, state, smp);
        } else if (t == Shared) {
            number_of_access = this->shared_memory.access(warp, state);
        }
        analyzer.access(t, MemoryOp(state->MEMORY_OP), number_of_access);
    }
}
