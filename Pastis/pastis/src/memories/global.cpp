#include <tuple>

#include <cmath>

#include "analyzer/analyzer.h"
#include "caches/cache.h"
#include "device/device.h"
#include "device/smp.h"
#include "grid/warp.h"
#include "memories/memories.h"
#include "nvidia.h"
#include "trace.h"
#include "utils/utils.h"

#include "memories/global.h"

uint32_t scheme_access[4] = { 0x00000F0F, 0x0000F0F0, 0x0F0F0000, 0xF0F00000 };

inline uint64_t get_block_address(uint64_t address)
{
    return address & (~(uint64_t)0x1F);
}

int coalesce(uint64_t* block_addresses, Warp* warp, uint32_t mask)
{
    int idx = 0;
    // for each executed thread
    // add all different block acceded by the scheme
    for (int i = 0; i < THREADS_PER_WARPS; i++) {
        if ((mask >> i) & 0x1) {
            uint64_t address = warp->threads[i]->sim->state->ADDR;
            uint64_t block_address = get_block_address(address);
            if (idx == 0) {
                block_addresses[idx] = block_address;
                idx++;
            } else if (block_addresses[idx - 1] != block_address) {
                block_addresses[idx] = block_address;
                idx++;
            }
        }
    }
    return idx;
}

// Handle global load
int global_load(SMP* smp, uint64_t address, LDGFlags flags, Cache* l1, Cache* l2, Analyzer& analyser)
{
    if (flags == LDGFlags::CG) {
        info("access flag : CG\n");
        auto [l2_answer, l2OldEntry, l2NewEntry] = l2->hit(address);
        analyser.cacheAccess(l2_answer, L2);

        if (l2_answer == Hit) { // read data from lower memory
            info("hit on l2 cache \n");
            return 135;
        }
        info("miss on l2 cache \n");
        return 195;
    } else if (flags == LDGFlags::CI || flags == LDGFlags::CA) {
        info("access flag : CA\n");
        auto [l1_answer, l1_old_entry, l1_new_entry] = l1->hit(address);
        analyser.cacheAccess(l1_answer, L1, smp->index / L1_PER_SM);
        if (l1_answer == Hit) { // if hit then mark the block as dirty
            info("hit on l1 cache \n");
            return 59;
        } else {
            info("miss on l1 cache \n");
            if (l1_old_entry.isDirty()) { // look if the replaced block if dirty
                auto [l2HasHit, l2OldEntry, l2NewEntry] = l2->hit(l1_old_entry.address);
                l2NewEntry->markDirty();
            }
            auto [l2_answer, l2_old_entry, l2_new_entry] = l2->hit(address);
            analyser.cacheAccess(l2_answer, L2);
            if (l2_answer == Hit) {
                info("hit on l2 cache \n");
                return 135;
            }
            info("miss on l2 cache \n");
            return 195;
        }
    } else {
        info("access flag : CV\n");
        return 195;
    }
}

// Handle Global Store
int global_store(SMP* smp, uint64_t address, STGFlags flags, Cache* l1, Cache* l2, Analyzer& analyser)
{
    if (flags == STGFlags::WB || flags == STGFlags::CG || flags == STGFlags::CS) {
        info( "access flag : WR | CG | CS\n");
        auto [l1_answer, l1_old_entry, l1_new_entry] = l1->hit(address);
        analyser.cacheAccess(l1_answer, L1, smp->index / L1_PER_SM);
        if (l1_answer == Hit) { // if hit then mark the block as dirty
            info("hit on l1 cache \n");
            l1_new_entry->markDirty();
            return 82;
        } else {
            info("miss on l1 cache \n");
            l1_new_entry->markDirty();
            if (l1_old_entry.isDirty()) { // look if the replaced block if dirty
                auto l2_new_entry = std::get<2>(l2->hit(l1_old_entry.address));
                l2_new_entry->markDirty();
            }

            auto [l2_answer, l2_old_entry, l2_new_entry] = l2->hit(address);
            analyser.cacheAccess(l2_answer, L2);
            if (l2_answer == Hit) { // read data from lower memory
                info("hit on l2 cache \n");
                return 159;
            }
            info("miss on l2 cache \n");
            return 218;
        }
    } else {
        l1->hit(address);
        l2->hit(address);
        return 218;
    }
}

uint32_t GlobalMemory::access(Warp* warp, Analyzer& analyser, nvidia_state_t* state, SMP* smp)
{
    uint32_t mask = warp->stack->lastMask();
    uint32_t memory_latency = 0;
    uint32_t total_access = 0;
    uint64_t block_acceded[8];

    Cache* l1 = smp->l1;
    Cache* l2 = smp->parent_device->l2Caches[0].get();

    info("=== GLOBAL ACCESS ===\n");

    // iterate over scheme access seems to true for read, not for write
    for (uint32_t scheme_access : scheme_access) {
        uint32_t access_mask = mask & scheme_access;
        size_t nb_acceded_block = 0;
        if (access_mask)
            nb_acceded_block = coalesce(block_acceded, warp, access_mask);
        total_access += nb_acceded_block;
        for (size_t j = 0; j < nb_acceded_block; j++) {
            MemoryOp op = MemoryOp(state->MEMORY_OP);
            if (op == Read) {
                uint32_t lat = global_load(smp, block_acceded[j], LDGFlags(state->CACHE_FLAG), l1, l2, analyser);
                memory_latency = std::max(memory_latency, lat);
            } else if (op == Write) {
                uint32_t lat = global_store(smp, block_acceded[j], STGFlags(state->CACHE_FLAG), l1, l2, analyser);
                memory_latency = std::max(memory_latency, lat);
            }
        }
    }
    warp->instruction_latency = memory_latency;
    return total_access;
}
