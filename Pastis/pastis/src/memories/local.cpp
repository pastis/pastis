#include "nvidia.h"

#include "memories/local.h"

#include "grid/warp.h"

#include "analyzer/analyzer.h"


uint32_t LocalMemory::access(Warp *warp)
{
    warp->instruction_latency = 0;
    return 1;
}