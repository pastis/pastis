
#include "memories/constant.h"

#include "grid/warp.h"

#include "analyzer/analyzer.h"

#include "grid/warp.h"

// function used for constant memory
unsigned int ConstantMemory::access(Warp *warp, Analyzer& analyzer, nvidia_state_t* state, SMP* smp)
{
    uint64_t address = state->ADDR;
    SM* sm = smp->parent_device->SMs[smp->sm_index].get();
    analyzer.access(Constant, Read);
    CacheAnswer op_answer = std::get<0>(sm->op_cache->hit(address));
    if (op_answer == Hit) {
        warp->dispatch_latency = 0;
        return 1;
    }
    Device* device = smp->parent_device;
    CacheAnswer l2_answer = std::get<0>(device->l2Caches[0]->hit(address));
    if (l2_answer == Hit) {
        warp->dispatch_latency = 120;
    }

    warp->dispatch_latency = 120;
    return 1;
}