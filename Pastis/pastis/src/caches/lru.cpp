#include <cmath>
#include <cstdint>
#include <tuple>

#include "caches/lru.h"
#include "trace.h"

LRU::LRU(CacheConfig& config)
    : Cache { config }
{
    this->associativity = config.associativity;
}
/*
    return
        a boolean hit = true, miss = false
        a replaced TagEntry
        a pointer to the new tag entry
 */
std::tuple<CacheAnswer, TagEntry, TagEntry*> LRU::hit(uint64_t address)
{
    size_t index_set = this->index(address);
    uint64_t tag = address >> (this->offset_width + this->index_width);
    // Compute the addressed set
    size_t base_lines = (this->nb_lines / this->nb_sets) * index_set;
    // Find in this set if the tag is present

    for (size_t i = 0; i < this->associativity; i++) {
        TagEntry* entry = &(this->tag_entries[base_lines + i]);
        if (entry->tag == tag && entry->isValid()) {
            debug( "Hit\n");
            entry->resetAge();
            return { Hit, TagEntry {}, entry };
        }
    }
    // If there is a miss
    debug( "Miss\n");
    TagEntry* te = nullptr, lastEntry {};
    for (size_t i = 0; i < this->associativity; i++) {
        TagEntry* entry = &(this->tag_entries[base_lines + i]);
        if (!entry->isValid()) {
            te = entry;
            break;
        }
    }

    if (te != nullptr) {
        debug( "Invalid entry found\n");
        te->setEntry(tag, address);
    } else {
        debug( "No invalid entry found\n");
        for (size_t i = 0; i < this->associativity; i++) {
            TagEntry* entry = &(this->tag_entries[base_lines + i]);
            if (entry->isValid() && entry->evict_first) {
                te = entry;
                break;
            }
        }

        if (te != nullptr) {
            debug( "Evict first entry found\n");
            lastEntry = *te;
            te->setEntry(tag, address);
        } else {
            te = &this->tag_entries[base_lines];
            uint64_t age = te->age;
            for (size_t i = 1; i < this->associativity; i++) {
                TagEntry* entry = &this->tag_entries[base_lines + i];
                if (entry->age > age) {
                    te = entry;
                    age = entry->age;
                }
            }
            lastEntry = *te;
            te->setEntry(tag, address);
        }
    }
    return {Miss, lastEntry, te};
}

LRU::~LRU() = default;