#include <cstdint>

#include "trace.h"

#include "caches/direct_mapped_cache.h"

DirectMapped::DirectMapped(CacheConfig& config)
    : Cache { config }
{
}

std::tuple<CacheAnswer, TagEntry, TagEntry*> DirectMapped::hit(uint64_t address)
{
    size_t indexLine = this->index(address);
    uint64_t tag = address >> (this->offset_width + this->index_width);
    TagEntry& entry = this->tag_entries[indexLine];
    if (entry.tag == tag && entry.valid) {
        return { Hit, entry, nullptr };
    } else {
        TagEntry returnEntry = entry;
        entry.setEntry(tag, address);
        return { Miss, returnEntry, nullptr };
    }
}

DirectMapped::~DirectMapped() = default;