#include "caches/tag_entry.h"

TagEntry::TagEntry(){
    this->tag = 0;
    this->address = 0;
    this->age = 0;
    this->valid = false;
    this->dirty = false;
    this->evict_first = false;
}

void TagEntry::invalid() {
    this->valid = false;
}

bool TagEntry::isValid() {
    return this->valid;
}

void TagEntry::markDirty() {
    this->dirty = true;
}

bool TagEntry::isDirty() {
    return this->dirty;
}

void TagEntry::resetAge() {
     this->age = 0;;
}

void TagEntry::incAge() {
    this->age += 1;
}


void TagEntry::setEntry(uint64_t tag,uint64_t address, bool evict_first){
    this->address = address;
    this->tag = tag;
    this->evict_first = evict_first;
    this->valid = true;
    this->dirty = false;
}