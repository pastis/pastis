#include "caches/plru.h"
#include "trace.h"
#include <cmath>
#include <cstdint>

PseudoLRU::PseudoLRU(CacheConfig& config)
    : Cache { config }
{
    this->associativity = config.associativity;
    this->trees.resize(this->nb_sets);
    for (auto& tree : this->trees) {
        tree.resize(this->associativity - 1, false);
    }
}


PseudoLRU::~PseudoLRU() = default;

std::tuple<CacheAnswer, TagEntry, TagEntry*> PseudoLRU::hit(uint64_t address)
{
    uint32_t index_set = this->index(address);
    uint64_t tag = address >> (this->offset_width + this->index_width);
    // Compute the addressed set
    uint32_t base_lines = (this->nb_lines / this->nb_sets) * index_set;
    // Create to iterator : one represents the beginning and the other represents the ending
    std::vector<bool>& tree = this->trees[index_set];
    // Find in this set if the tag is present
    for (size_t i = 0; i < this->associativity; i++) {
        TagEntry* entry = &(this->tag_entries[base_lines + i]);
        if (entry->tag == tag && entry->isValid()) {
            debug( "Hit\n");
            this->update(tree, i);
            return { Hit, TagEntry {}, entry };
        }
    }

    debug( "Miss\n");
    TagEntry *te = nullptr, lastEntry {};

    for (size_t i = 0; i < this->associativity; i++) {
        TagEntry* entry = &(this->tag_entries[base_lines + i]);
        if (entry->isValid() && entry->evict_first) {
            te = entry;
            break;
        }
    }

    if (te != nullptr) {
        debug( "Evict first entry found\n");
        lastEntry = *te;
        te->setEntry(tag, address);
    } else {
        size_t idx = this->getIndex(tree);
        this->next(tree);
        te = &this->tag_entries[idx];
        lastEntry = *te;
        te->setEntry(tag, address);
    }
    return { Miss, lastEntry, te };
}


void PseudoLRU::next(std::vector<bool>& tree)
{
    size_t index = 1;
    while ((index - 1) < tree.size()) {
        if (tree[index - 1]) {
            tree[index - 1] = false;
            index = index * 2 + 1;
        } else {
            tree[index - 1] = false;
            index = index * 2;
        }
    }
}

size_t PseudoLRU::getIndex(std::vector<bool>& tree)
{
    size_t node_idx = 1;
    while (node_idx < tree.size()) {
        if (tree[node_idx - 1]) {
            node_idx = (node_idx * 2 + 1);
        } else {
            node_idx = node_idx * 2;
        }
    }
    return node_idx % this->associativity;
}

void PseudoLRU::update(std::vector<bool>& tree, size_t line)
{
    size_t previous_node_idx = line;
    size_t node_idx = line;
    for (size_t level = this->tree_depth; level > 0; level--) {
        size_t level_idx = (std::pow(2, level) / 2) - 1;
        node_idx = node_idx / 2;
        size_t idx = level_idx + node_idx;
        if (previous_node_idx % 2 == tree[idx]) {
            break;
        }
        tree[idx] = !tree[idx];
        previous_node_idx = node_idx;
    }

    for (bool v : tree) {
        debug( "%2d", v);
    }
    debug( "\n");
}
