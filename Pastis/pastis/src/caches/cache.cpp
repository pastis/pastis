#include <cmath>
#include <memory>

#include "caches/cache.h"
#include "caches/direct_mapped_cache.h"
#include "caches/lru.h"
#include "caches/plru.h"
#include "config.h"

// Compute the index of a given address
int Cache::index(uint64_t address) const
{
    return (address << (64 - (this->offset_width + this->index_width))) >> (64 - this->index_width);
}

Cache::Cache(CacheConfig& config)
{
    this->associativity = config.associativity;
    this->cache_size = config.cache_size;
    this->line_size = config.line_size;
    this->nb_lines = (this->cache_size / this->line_size);
    this->nb_sets = (this->nb_lines / this->associativity);
    this->index_width = (uint8_t)log2(this->nb_sets);
    this->offset_width = (uint8_t)log2(this->line_size);
    this->tag_entries.resize(this->nb_lines);
}

std::unique_ptr<Cache> Cache::cacheFactory(CacheConfig& config)
{
    if (config.algorithm == PLRU) {
        return std::make_unique<PseudoLRU>(config);
    } else if (config.algorithm == CLRU) {
        return std::make_unique<LRU>(config);
    }
    return std::make_unique<DirectMapped>(config);
}