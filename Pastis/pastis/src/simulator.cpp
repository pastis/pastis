#include "simulator.h"
#include "nvidia.h"
#include "trace.h"

#include <string>
#include <memory>

Simulator::Simulator(std::string cubin_file_name)
{
    nvidia_loader_t* nv_loader;
    nvidia_platform_t* nv_platform;
    // open the cuda binary file
    nv_loader = nvidia_loader_open(cubin_file_name.c_str()); // nvidia/src/loader.c
    if (nv_loader == nullptr) {
        throw 1;
    }

    // get the start address
    this->address_start = nvidia_loader_start(nv_loader); // nvidia/src/loader.c

    // for (struct data_secs* ds = nv_loader->Data.secs; ds; ds = ds->next) {
    //     printf("%s\n", ds->name);
    // }

    nvidia_loader_close(nv_loader); // nvidia/src/loader.c
    this->address_exit = (nvidia_address_t)0xffffffffff; // not used by the simulator
    // generate the platform
    nv_platform = nvidia_new_platform(); // nvidia/src/api.c

    if (!nv_platform) {
        throw 2;
    }

    this->platform = nv_platform;
    // load the image
    nvidia_load_platform(this->platform, cubin_file_name.c_str());

    info( "Address of the constant memory: %lu (%p)\n", (uint64_t)this->constantMemory.data(),
        (void*)this->constantMemory.data());
}
