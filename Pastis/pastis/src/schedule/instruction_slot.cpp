#include "grid/warp.h"

#include "schedule/instruction.h"

#include "trace.h"

InstructionSlot::InstructionSlot()
{
    this->fill(0, nullptr, NO_UNIT, 7, 7, 0, 0, false);
}

void InstructionSlot::fill(uint64_t id, Warp* warp, Unit unit, size_t b1, size_t b2, uint32_t instruction_latency, uint32_t dispatch_latency, bool valid)
{
    this->id_instruction = id;
    this->location = DISPATCHER;
    this->operand_collection_latency = 9;
    this->warp = warp;
    this->barrier_1 = b1;
    this->barrier_2 = b2;
    this->instruction_latency = instruction_latency;
    this->dispatch_latency = dispatch_latency;
    this->unit = unit;
    this->valid = valid;
}

void InstructionSlot::reset()
{
    this->fill(0, nullptr, NO_UNIT, 7, 7, 0, 0, false);
}

void InstructionSlot::decWARBarrier()
{
    if (this->barrier_2 < 7) {
        uint8_t& barrier = this->warp->scoreboard[this->barrier_2].value;
        barrier -= (barrier > 0) ? 1 : 0;
    }
}

void InstructionSlot::decRAWBarrier()
{
    if (this->barrier_1 < 7) {
        uint8_t& barrier = this->warp->scoreboard[this->barrier_1].value;
        barrier -= (barrier > 0) ? 1 : 0;
    }
}

InstructionSlot InstructionSlot::dispatch()
{
    InstructionSlot new_is = *this;
    this->reset();

    if (new_is.unit == LSU) {
        new_is.warp->memBarPut();
    }
    debug("Instruction %lu dispatched\n", new_is.id_instruction);
    new_is.location = OPERAND_COLLECTOR;
    return new_is;
}

InstructionSlot InstructionSlot::operand_collection_done()
{
    InstructionSlot new_is = *this;
    this->reset();

    new_is.decWARBarrier();
    debug("Instruction %lu is out of the operand collector\n", new_is.id_instruction);
    if (new_is.unit == LSU) {
        new_is.location = BUFFER;
    } else {
        new_is.location = EXECUTION;
    }
    return new_is;
}

void InstructionSlot::commit()
{
    if (this->instruction_latency == 0 && this->valid) {
        if (this->unit == LSU) {
            this->warp->memBarPop();
        }
        this->decRAWBarrier();
        debug("Instruction %lu commited\n", this->id_instruction);
        this->reset();
    }
}

void InstructionSlot::stepCycle()
{
    if (this->location == DISPATCHER) {
        if (this->valid && this->dispatch_latency > 0) {
            this->dispatch_latency--;
        }
    }

    if (this->location == OPERAND_COLLECTOR) {
        if (this->valid && this->operand_collection_latency > 0) {
            debug("Instruction %lu collects its operand for one more cycle\n", this->id_instruction);
            this->operand_collection_latency--;
        }
    }

    if (this->location == EXECUTION) {
        if (this->valid && this->instruction_latency > 0) {
            this->instruction_latency--;
        }
    }
}
