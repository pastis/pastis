#include "schedule/schi.h"
#include "grid/thread.h"
#include "grid/warp.h"
#include "nvidia.h"
#include "trace.h"

SchiField::SchiField(Instruction& instr, int base_index)
{
    this->fillFromInstruction(instr, base_index);
}

SchiField::SchiField(Warp* warp, int delta)
{
    this->fillFromPC(warp, delta);
}

// get sched inst field data
// Base idx = base indices to select which instruction we want to refer to

void SchiField::fillFromInstruction(Instruction& inst, size_t base_index)
{
    nvidia_ii_t* input = inst.instr->instrinput;
    this->remaining_data = input[base_index + 5].val.uint8;
    this->barrier = input[base_index + 4].val.uint8;
    this->anti_dep = input[base_index + 3].val.uint8;
    this->true_dep = input[base_index + 2].val.uint8;
    this->yield_hint_flag = input[base_index + 1].val.uint8;
    this->number_of_cycles = input[base_index].val.uint8;
}

// get sched inst for a given pc
// delta is useful if we want the sched inst of the current or the next instruction
void SchiField::fillFromPC(Warp* warp, uint64_t delta)
{
    nvidia_state_t* state = warp->getState();
    if (state) {
        if (((state->IADDR + delta) & 0x1F) != 0) {
            uint64_t schi_pc = state->SCHI_PC;
            int index_instr = (((state->IADDR + delta) >> 3) & 0x3) - 1;
            int base_index = (2 - index_instr) * SCHED_INST_DATA_PER_INSTRUCTION;
            Instruction instr(warp, schi_pc);
            this->fillFromInstruction(instr, base_index);
        } else {
            this->remaining_data = 0;
            this->barrier = 0;
            this->anti_dep = 7;
            this->true_dep = 7;
            this->yield_hint_flag = 0;
            this->number_of_cycles = 0;
        }
    }
}

// Print scheduling instruction information
void SchiField::print()
{
    info("\tremaining data   : %d\n", this->remaining_data);
    info("\tbarrier needed   : %d\n", this->barrier);
    info("\tanti dependency  : %d\n", this->anti_dep);
    info("\ttrue dependency  : %d\n", this->true_dep);
    info("\tyield hint flag  : %d\n", this->yield_hint_flag);
    info("\tnumber of cycles : %d\n", this->number_of_cycles);
}
