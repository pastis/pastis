#include "nvidia.h"

#include "utils/exception.h"

#include "trace.h"

#include "schedule/instruction.h"

#include "grid/warp.h"

Instruction::Instruction(Warp* warp, uint64_t pc)
{
    this->instr = warp->getInst(pc);
}

Instruction::~Instruction()
{
    if (this->instr)
        nvidia_free_inst(this->instr);
}

