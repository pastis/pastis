//
// Created by michael.adalbert on 13/12/2021.
//

#include <cstdlib>

#include "pastis/pastis_memory.h"

#include "caches/cache.h"

#include "device/device.h"

void pastisMemCopy(Device& device, void* address, size_t s)
{
    uint8_t* ptr = (uint8_t*)address;
    for (size_t i = 0; i < s; i++)
        device.l2Caches[0]->hit((uint64_t)(ptr + i));
}

void pastisMalloc(void** ptr, size_t s)
{
    *ptr = aligned_alloc(256, s);
}

void pastisFree(void* ptr)
{
    free(ptr);
}