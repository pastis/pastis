#include <cmath>
#include <fstream>
#include <string>

#include "utils/exception.h"

#include "analyzer/analyzer.h"
#include "commands/manager.h"
#include "config.h"
#include "device/sm.h"
#include "grid/grid.h"
#include "pastis/parameter.h"
#include "pastis/pastis.h"
#include "simulator.h"
#include "trace.h"

void license()
{
    printf("PasTiS Copyright (C) 2022 Adalbert Michael\n"
           "This program comes with ABSOLUTELY NO WARRANTY.\n"
           "This is free software, and you are welcome to redistribute it\n"
           "under certain conditions.");
}

void copy_params(Simulator& simulator, size_t params_num, Parameter* params)
{
    void* ptr;
    std::uint64_t offset = 0;
    uint8_t* c_mem = simulator.constantMemory.data();

    for (size_t i = 0; i < params_num; i++) {
        Parameter p = params[i];
        switch (p.kind) {
        case INT32:
            ptr = c_mem + 0x140 + offset;
            simulator.affect_c_mem<int32_t>(0, 0x140 + offset, p.val.i);
            init("Writing parameter %d : %d (0x%x) at address = %ld (0x%lx) => c[0x0][0x%x]\n", i, p.val.i,
                p.val.i,
                (uint64_t)ptr, (uint64_t)ptr, 0x140 + offset);
            offset += 4;
            break;
        case INT64:
            if (offset % 8 != 0)
                offset += 4;
            ptr = c_mem + 0x140 + offset;
            simulator.affect_c_mem<int64_t>(0, 0x140 + offset, p.val.l);
            init("Writing parameter %d : %d (0x%x) at address = %ld (0x%lx) => c[0x0][0x%x]\n", i, p.val.i,
                p.val.i,
                (uint64_t)ptr, (uint64_t)ptr, 0x140 + offset);
            offset += 8;
            break;
        case FLOAT:
            ptr = c_mem + 0x140 + offset;
            simulator.affect_c_mem<float>(0, 0x140 + offset, p.val.f);
            init("Writing parameter %d : %f (0x%a) at address = %ld (0x%lx) => c[0x0][0x%x]\n", i, p.val.i,
                p.val.i,
                (uint64_t)ptr, (uint64_t)ptr, 0x140 + offset);
            offset += 4;
            break;
        case DOUBLE:
            if (offset % 8 != 0)
                offset += 4;
            ptr = c_mem + 0x140 + offset;
            simulator.affect_c_mem<double>(0, 0x140 + offset, p.val.d);
            init("Writing parameter %d : %lf (0x%la) at address = %ld (0x%lx) => c[0x0][0x%x]\n", i,
                p.val.d, p.val.d,
                (uint64_t)ptr, (uint64_t)ptr, 0x140 + offset);
            offset += 8;
            break;
        case PTR:
            if (offset % 8 != 0)
                offset += 4;
            ptr = c_mem + 0x140 + offset;
            simulator.affect_c_mem<void*>(0, 0x140 + offset, p.val.ptr);
            init("Writing parameter %d : %p (0x%lx) at address = %ld (0x%lx) => c[0x0][0x%x]\n", i,
                p.val.ptr, p.val.ptr,
                (uint64_t)ptr, (uint64_t)ptr, 0x140 + offset);
            offset += 8;
            break;
        }
    }
}

//--------------------------------------------------------------
//------------------------- copyGridSize -----------------------
//--------------------------------------------------------------

// void copyParams(nvidia_sim_t *sim, int gridDim, int blockDim, int params_num,
// parameter_t *params){
void copy_grid_size(Simulator& simulator, dim3 gridDim, dim3 blockDim)
{
    simulator.affect_c_mem<uint32_t>(0, 0x8, blockDim.x);
    simulator.affect_c_mem<uint32_t>(0, 0xC, blockDim.y);
    simulator.affect_c_mem<uint32_t>(0, 0x10, blockDim.z);
    simulator.affect_c_mem<uint32_t>(0, 0x14, gridDim.x);
    simulator.affect_c_mem<uint32_t>(0, 0x18, gridDim.y);
    simulator.affect_c_mem<uint32_t>(0, 0x1C, gridDim.z);
}

//--------------------------------------------------------------
//------------------------- simulate ---------------------------
//--------------------------------------------------------------

void simulate(Device& device, std::string cubin_file_name, dim3 gridDim, dim3 blockDim, size_t params_num, Parameter* params,
    Config& config)
{
    license();
    // Check Grid boundaries
    if (!((gridDim.x >= 1) && (gridDim.x <= pow(2, 31) - 1))) {
        std::cerr << "gridDim.x should be between 1 and 2^31-1" << std::endl;
        return;
    }
    if (!((gridDim.y >= 1) && (gridDim.y <= 65536))) {
        std::cerr << "gridDim.y should be between 1 and 65536" << std::endl;
        return;
    }
    if (!((gridDim.z >= 1) && (gridDim.z <= 65536))) {
        std::cerr << "gridDim.z should be between 1 and 65536" << std::endl;
        return;
    }
    // Check block boundaries
    if (blockDim.x * blockDim.y * blockDim.z > MAX_THREAD_BLOCK_SIZE) {
        std::cerr << "the number of thread inside a block should not be higher than 1024" << std::endl;
        return;
    }

    // change the state of the simulator
    init_trace(config);
    printf("starting simulation ...\n");
    printf("\tcuda binary file name: %s\n", cubin_file_name.c_str());
    printf("\tgridDim = (%d,%d,%d)\n", gridDim.x, gridDim.y, gridDim.z);
    printf("\tblockDim = (%d,%d,%d)\n", blockDim.x, blockDim.y, blockDim.z);
    printf("\t%ld parameters\n\n", params_num);
    // Initialise the platform (load cuda bin file)

    try {

        // Initialiaze the underlying simulator
        Simulator simulator(cubin_file_name);

        // Affect the localmemory address
        simulator.affect_c_mem<uint32_t>(0, 0x20, 0x7FFFF);

        // create the grid
        Grid grid(gridDim, blockDim, simulator);

        // create the command manager
        CommandManager cm(config.sim_mode);

        // create the analyser
        Analyzer analyzer;

        // map the grid to the device
        // this function distribute blocks and warp accross 
        // all sub devices (SMs and SMPs)
        device.map(grid);

        // Copy the parameters into the constant memory
        copy_params(simulator, params_num, params);

        // Copy grid and block(threadgroups) information into the constant memory
        copy_grid_size(simulator, gridDim, blockDim);

        // execute all blocks on all SMs unitl there are finish
        for (size_t i = 0; i < SM_PER_DEV; i++) {
            std::unique_ptr<SM>& sm = device.SMs[i];
            
            sm->dispatchBlock();
            while (sm->blocks)
                sm->step(analyzer, cm);
        }

        // Output data gathered from execution
        analyzer.printAnalysis();


        
        std::string dump_file_name = cubin_file_name.append(".dump");
        std::fstream file;
        file.open(dump_file_name);
        if (file.is_open()) {
            analyzer.dumpAnalysis(file);
            file.close();
        }
    } catch (Exception& e) {
        printf("%s\n", e.msg.c_str());
        return;
    }
}
