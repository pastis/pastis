#include <cstdint>


#include "nvidia.h"

#include "memories/memory_op.h"
#include "memories/memory_type.h"

#include "analyzer/analyzer.h"
#include "device/sm.h"
#include "device/smp.h"
#include "grid/warp.h"

#include "schedule/schi.h"
#include "trace.h"

// Create an analyser
Analyzer::Analyzer()
{
    this->g_read = this->g_write = 0;
    this->l_read = this->l_write = 0;
    this->s_read = this->s_write = 0;
    this->c_read = 0;
}

void Analyzer::postAnalysis(Warp* warp, SMP* smp)
{
    nvidia_state_t* state = warp->getState();
    if (!state) {
        return;
    }
    uint64_t current_pc = state->IADDR;

    uint32_t sm_id = smp->sm_index;
    info("=== TIMING ANALYSIS ===\n");
    info("execution on SM %d\n", sm_id);

    // each instruction take one cycle
    if (((current_pc >> 3) & 0x3) == 0) { // if it's a scheduling instruction then
        info("SCHEDULING INSTRUCTION : \n");
        for (size_t j = 1; j <= 3; j++) { //  Print information for each field
            info("=== instruction %d ===\n", j);
            SchiField field(warp, j * 8);
            field.print();
        }
        info("=== ============== ===\n");
    } else {
        if (!(warp->is_busy)) {
            SchiField s(warp, 0);
            info("=== current  instr ===\n");
            s.print();
            info("=== ============== ===\n");
        }
    }

    for (size_t i = 0; i < SM_PER_DEV; i++) {
        for (size_t j = 0; j < SMP_PER_SM; j++) {
            info("number of cycle on SM %ld smp %ld : %ld\n", i, j, this->cycles[i * SMP_PER_SM + j]);
        }
    }
    info("=== ====== ======== ===\n");
}

void Analyzer::printAnalysis()
{ // Print state of the analyser
    this->dumpAnalysis(std::cout);
}

void Analyzer::dumpAnalysis(std::ostream& out)
{
    out << "===  TIMING STATS    ===\n";
    for (size_t i = 0; i < SM_PER_DEV; i++) {
        for (size_t j = 0; j < SMP_PER_SM; j++) {
            out << "SM_" << i << " : " << this->cycles[i * SMP_PER_SM + j] << " cycles\n";
        }
    }
    out << "===  MEMORY STATS    ===\n";
    out << "Global\t: read " << this->g_read << ", write " << this->g_write << "\n";
    out << "Shared\t: read " << this->s_read << ", write " << this->s_write << "\n";
    out << "Local\t: read " << this->l_read << ", write " << this->l_write << "\n";
    out << "Constant\t: read " << this->c_read << "\n";

    out << "===  CACHE  STATS    ===\n";
    out << "---  L1     STATS    ---\n";
    for (size_t i = 0; i < SM_PER_DEV * L1_PER_SM; i++) {
        out << "SM[" << i / L1_PER_SM << "][" << i % L1_PER_SM << "] cache l1 : " << this->l1[i].hit << " hits, "
            << this->l1[i].miss << " misses\n";
    }
    out << "---  L2     STATS    ---\n";
    for (size_t i = 0; i < L2_PER_DEV; i++) {
        out << "cache l2[" << i << "] : " << this->l2[i].hit << " hits " << this->l2[i].miss << " misses\n";
    }
    out << "========================\n";
    out << std::endl;
}

void Analyzer::access(MemoryType t, MemoryOp o, unsigned long n)
{

    if (t == MemoryType::Global) {
        if (o == MemoryOp::Write)
            this->g_write += n;
        else if (o == MemoryOp::Read)
            this->g_read += n;
    } else if (t == MemoryType::Shared) {
        if (o == MemoryOp::Write)
            this->s_write += n;
        else if (o == MemoryOp::Read)
            this->s_read += n;
    } else if (t == MemoryType::Local) {
        if (o == MemoryOp::Write)
            this->l_write += n;
        else if (o == MemoryOp::Read)
            this->l_read += n;
    } else if (t == MemoryType::Constant) {
        if (o == MemoryOp::Read)
            this->c_read += n;
    }
}

void Analyzer::stepCycle(unsigned long idx_smp)
{
    this->cycles[idx_smp]++;
}

void Analyzer::cacheAccess(CacheAnswer a, CacheLevel l, unsigned long cache_idx)
{
    if (l == CacheLevel::L1) {
        if (a == CacheAnswer::Hit) {
            this->l1[cache_idx].hit++;
        } else if (a == CacheAnswer::Miss) {
            this->l1[cache_idx].miss++;
        }
    } else if (l == CacheLevel::L2) {
        if (a == CacheAnswer::Hit) {
            this->l2[cache_idx].hit++;
        } else if (a == CacheAnswer::Miss) {
            this->l2[cache_idx].miss++;
        }
    }
}
