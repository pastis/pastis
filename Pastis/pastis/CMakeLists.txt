cmake_minimum_required(VERSION 3.16)

set(CMAKE_C_COMPILER /usr/bin/gcc-11 CACHE PATH "")
set(CMAKE_CXX_COMPILER /usr/bin/g++-11 CACHE PATH "")

project(pastis C CXX)

set(CMAKE_CXX_STANDARD 20)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -O3 -std=c++20 -Wall -Wextra -Werror -Wpedantic")

include_directories("../nvidia/include")

include_directories("include")

file(GLOB CXX_SRCS_A "src/**/*.cpp")
file(GLOB CXX_SRCS_B "src/*.cpp")

add_library(pastis STATIC ${CXX_SRCS_A} ${CXX_SRCS_B})
