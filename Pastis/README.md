# XP_CUDA

## Tools

- Ocaml
- Good C Compiler ( GCC or Clang )

## Build Pastis

```shell
$ cd gliss2 && make
$ cd ../nvidia && make
$ cd ../pastis && make
```