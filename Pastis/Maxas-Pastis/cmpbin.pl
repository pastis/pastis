#!/usr/bin/perl

print "Compare deux codes binaires et indique les bits qui diffèrent.\n\n";
print "Premier code (en hexa) : ";
$bin1 = hex(<STDIN>);
print "Deuxième code (en hexa) : ";
$bin2 = hex(<STDIN>);

print "bit\t\tbin1\tbin2\n";
for ($i=63; $i>=0; $i--){
  $bit1 = ($bin1 & (1 << 63)) >> 63;
  $bit2 = ($bin2 & (1 << 63)) >> 63;
  if ($bit1 != $bit2){
    print "$i\t\t$bit1\t$bit2\n";
  }
  $bin1 = $bin1 << 1;
  $bin2 = $bin2 << 1;
}
