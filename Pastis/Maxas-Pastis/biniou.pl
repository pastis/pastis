#!/usr/bin/perl

print "Biniou: a utility to inspect binary code.\n\n";

while (1){
  print "\tc: compare deux codes en hexa\n";
  print "\te: extrait un champ de bits d'un code hexa\n";
  $com = <STDIN>;
  if ($com =~/c/){
    compare_codes();
  }
  if ($com =~/e/){
    extract();
  }
}

sub compare_codes {
  print "Premier code (en hexa) : ";
  $bin1 = hex(<STDIN>);
  print "Deuxième code (en hexa) : ";
  $bin2 = hex(<STDIN>);

  print "bit\t\tbin1\tbin2\n";
  for ($i=63; $i>=0; $i--){
    $bit1 = ($bin1 & (1 << 63)) >> 63;
    $bit2 = ($bin2 & (1 << 63)) >> 63;
    if ($bit1 != $bit2){
      print "$i\t\t$bit1\t$bit2\n";
    }
    $bin1 = $bin1 << 1;
    $bin2 = $bin2 << 1;
  }
}

sub extract {
  print "Code (en hexa) : ";
  $bin = hex(<STDIN>);
  print "Champ de bits (format : x-y, x bit de rang le plus fort, y bit de rang le plus faible) : ";
  $champ = <STDIN>;
  if ($champ =~/(\d+)-(\d+)/){
    print "\n\t";
    $upper = $1; $lower = $2;
    $val = $bin << (63-$upper);
    $nb = $upper - $lower + 1;
    for ($i = 0; $i<$nb ; $i++){
      $bit = (($val & (1 << 63)) >> 63);
      print $bit;
      $val = $val << 1;
    }
    print "\n\n";
  }
}
