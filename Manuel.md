# Guide d'utlisation du projet 

## Sommaire

- [Recuperation du projet](#recuperation-du-projet)
- [Pastis](#pastis)
    - [Compilation du simulateur](#compilation-de-pastis)
        - [Outils](#outils)
        - [Commandes](#commandes)


## Recuperation du projet
---

```shell
    git clone --recursive https://.../....git
```

---
## Pastis
---
### Compilation du Simulateur
---
#### Outils
---
Pour compiler le simulateur, vous devez avoir les prérequis suivant :
- Un compilateur C/C++ supportant le standart C++20
    - GCC >= 10 ou Clang >= 11 (non testé sur Windows)
- Un compilateur Ocaml
- L'outil de compilation cmake avec la version 3.16 minimum
    - le dépot de ubuntu 20.4 (LTS) contient cette version

---
#### Commandes
---
- Gliss2

Tout d'abord, vous devez compiler l'outil Gliss2 se trouvant dans le dossier Pastis/gliss2.

Pour compiler l'outil vous devez exécuter la commande shell suivante :
```shell
/chemin/du/depot/Pastis/gliss2: $ make
```

- nvidia

Une fois que gliss2 est compilé, nous pouvons compiler les sources du dossier nvidia qui nessécite gliss2 et un compilateur C.

Pour compiler, exécutez la commande : 
```shell
/chemin/du/depot/Pastis/nvidia: $ make
```

- pastis

Finalement nous devons compiler le simulateur (pastis ). Pour ce faire, vous devez avoir l'outils cmake et un compilateur C/C++ et exécuter les 
commandes suivantes : 


```shell
/chemin/du/depot/Pastis/pastis/ : $ cmake -B build
/chemin/du/depot/Pastis/pastis/ : $ cmake --build build -- -j4 

```

Normalement, dans le dossier build que la commande cmake a créé, vous devez avoir la librairie statique du simulateur (libpastis.a).

---
## Expérimentation
---
### Prérequis
---

Les expérimentations ont été compilé avec la version 10.2 de CUDA. Cette version est celle qui se trouve sur la jetson Tx2. Il est possible de l'obtenir sur Ubuntu en ajoutant le dépot datant de 2018 pour la version 18.04 de Ubuntu.

Sur la carte Jetson Tx2, nous avons désactivé l'interface graphique.
Si vous utilisez l'interface graphique les résultats sont moins stable (du faite qu'un process GPU doit gérer aussi l'interface graphique).

Pour compiler les expérimentation, vous devez installer notre version de maxas (Maxas-Pastis), vous devex avoir un interpréteur 
Perl pour pouvoir exécuter cette outil.

```shell 
/chemin/du/depot/pastis/Maxas-Pastis: $ perl Makefile.PL
/chemin/du/depot/pastis/Maxas-Pastis: $ make
/chemin/du/depot/pastis/Maxas-Pastis: $ sudo make install 
```

### Compilation

Pour compiler une expérimentation, vous devez vous rendre dans son dossier :

```shell
/chemin/du/depot: $ cd Experimentation/nom_de_lexperimentation
```

Ensuite vous devez simplement exécuter la commande suivante :
```shell
/chemin/du/depot/Experimentation/nom_de_lexperimenation: $ make benchmark
```

