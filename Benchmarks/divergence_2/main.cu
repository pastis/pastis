#include <cuda.h>
#include <stdio.h>


__device__ inline void update_masks(int *idx, int *masks){
    masks[*idx] = __activemask();
    (*idx)++;
    __syncthreads();
}

__global__ void test_divergence(int *tab, int *sum, int *lim, int *masks){
    int tid = threadIdx.x % 32;
    __shared__ int mask_idx[1];
    mask_idx[0] = 0;
    __syncwarp();
    *sum = 0;
    *lim = 0;
    update_masks(mask_idx, masks);
    if (tid >= 20){
        update_masks(mask_idx, masks);
        tab[tid] = tid + 1;
    }else{
        update_masks(mask_idx, masks);
        if (tid >= 16){
            update_masks(mask_idx, masks);
            for (int i = 0; i < 32; i++){
                update_masks(mask_idx, masks);
                *sum += tab[i];
            }
        }else{
            update_masks(mask_idx, masks);
            if (tid >= 8){
                update_masks(mask_idx, masks);
                for (int i = 0; i < tid; i++){
                    update_masks(mask_idx, masks);
                    *sum += tab[i];
                }
            }else{
                update_masks(mask_idx, masks);
                *lim = tab[tid];
                for (int i = 0; i < *lim; i++){
                    update_masks(mask_idx, masks);
                    *sum += tab[i];
                }
            }

            switch (tid % 4)
            {
            case 0:
                update_masks(mask_idx, masks);
                *lim = 0;
                break;
            case 1:
                update_masks(mask_idx, masks);
                *lim = 1;
                break;
            case 2:
                update_masks(mask_idx, masks);
                *lim = 2;
                break;
            case 3:
                update_masks(mask_idx, masks);
                *lim = 3;
                break;
            }
            update_masks(mask_idx, masks);
            for (int i = 1; i < 16; i = i * 2){
                update_masks(mask_idx, masks);
                if (tid % i == 0){
                    update_masks(mask_idx, masks);
                    *lim = i;
                }
            }
        }
        update_masks(mask_idx, masks);
        tab[tid] = tid + 1;
    }
    update_masks(mask_idx, masks);
}

const unsigned int nb_mask = 76;
const unsigned int nb_thread = 32;

int main()
{
    int tab[nb_thread], masks[nb_mask];
    int sum, lim;
    for (int i = 0; i < nb_thread; i++)
    {
        tab[i] = i;
    }
    for (int i = 0; i < nb_mask; i++)
    {
        masks[i] = 0;
    }

    int *tab_d, *sum_d, *lim_d, *masks_d;
    cudaMalloc((void **)&tab_d, nb_thread * sizeof(int));
    cudaMalloc((void **)&sum_d, sizeof(int));
    cudaMalloc((void **)&lim_d, sizeof(int));
    cudaMalloc((void **)&masks_d, nb_mask * sizeof(int));

    cudaMemcpy(tab_d, tab, nb_thread * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(masks_d, masks, nb_mask * sizeof(int), cudaMemcpyHostToDevice);

    test_divergence<<<1, nb_thread>>>(tab_d, sum_d, lim_d, masks_d);

    cudaMemcpy(tab, tab_d, nb_thread * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(&sum, sum_d, sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(&lim, lim_d, sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(masks, masks_d, nb_mask * sizeof(int), cudaMemcpyDeviceToHost);

    for (int i = 0; i < nb_thread; i++)
    {
        printf("%4d", tab[i]);
    }
    printf("\n");
    printf("sum : %d, lim : %d\n", sum, lim);
    for (int i = 0; i < nb_mask; i++)
    {
        for (int j = 0; j < nb_thread; j++)
        {
            printf("%d", (masks[i] >> j) & 0x1);
        }
        printf("\n");
    }

    cudaFree(tab_d);
    cudaFree(sum_d);
    cudaFree(lim_d);
    cudaFree(masks_d);
    return 0;
}
