#include <stdint.h>

#include <pastis/pastis.h>

const uint32_t nb_mask = 76;
const uint32_t nb_thread = 32;

int main()
{
    int tab[nb_thread], masks[nb_mask];
    int sum, lim;
    printf("@tab = %ld (0x%lx)\n", (uint64_t)tab, (uint64_t)tab);
    for (uint32_t i = 0; i < nb_thread; i++)
        tab[i] = i;
    for (uint32_t i = 0; i < nb_mask; i++)
        masks[i] = 0;

    Tx2Config jetson;

    Device device(jetson);

    Parameter params[4];
    params[0].kind = PTR;
    params[0].val.ptr = (void*)tab;
    params[1].kind = PTR;
    params[1].val.ptr = &sum;
    params[2].kind = PTR;
    params[2].val.ptr = &lim;
    params[3].kind = PTR;
    params[3].val.ptr = masks;
    dim3 gridDim(1), blockDim(nb_thread);
    printf("Calling simulator\n");
    simulate(device, "build/main.cubin", gridDim, blockDim, 4, params, jetson);
    for (uint32_t i = 0; i < nb_thread; i++) {
        printf("%4d", tab[i]);
    }
    printf("\n");
    printf("sum : %d, lim : %d\n", sum, lim);
    for (uint32_t i = 0; i < nb_mask; i++) {
        for (uint32_t j = 0; j < nb_thread; j++)
            printf("%d", (masks[i] >> j) & 0x1);
        printf("\n");
    }
}
