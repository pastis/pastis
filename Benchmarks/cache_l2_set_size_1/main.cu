#include <cuda.h>
#include <cuda_runtime.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define L2_SIZE 524288
#define L2_LINE_SIZE 64
#define L2_LINE_NUMS 8192
// On veut charger set_size + 1 ligne dans le set
__global__ void kernel(volatile uint8_t *input, uint32_t *timers, size_t stride_line, size_t set_size)
{
    uint32_t start, v, end;
    size_t i;
    extern __shared__ uint32_t s_array[];
    // Chargement de la première ligne de cache
    start = clock();
    v = input[0];
    end = clock();
    s_array[0] = end - start;
    // Ensuite on charche plusieurs qui devrait se trouver dans ce set 
#pragma unroll(1)
    for (i = 1; i <= set_size; i++)
    {
        start = clock();
        v = input[i * L2_LINE_SIZE * stride_line];
        end = clock();
        s_array[i] = end - start;
    }
    // Ensuite on charche plusieurs qui devrait se trouver dans ce set 
    start = clock();
    v = input[0];
    end = clock();
    s_array[set_size+1] = end - start;
#pragma unroll(1)
    for (i = 0; i <= set_size + 1; i++)
    {
        timers[i] = s_array[i];
    }
}
void cuda_check_error()
{
    cudaError_t err;
    err = cudaGetLastError();
    printf("%s : %s\n", cudaGetErrorName(err), cudaGetErrorString(err));
}

size_t pgcd(size_t a, size_t b)
{
    if (b) return pgcd(b, a % b);
    return a;  
}

int main()
{
    uint32_t *h_timer, *d_timer;
    uint8_t *d_input;
    size_t size = 1024 * 1024 * 1024;
    size_t bytes_input = size * sizeof(uint8_t);
    size_t bytes_timer = (L2_LINE_NUMS + 2) * sizeof(uint32_t);
    size_t stride, it, set_size;
    FILE *fp = fopen("result.csv", "w+");
    if (!fp)
        exit(1);

    h_timer = (uint32_t *)malloc(bytes_timer);
    if (!h_timer)
        exit(1);

    cudaMalloc((void **)&d_timer, bytes_timer);

    fprintf(fp, "  stride,set size");
    for (it = 0; it < (L2_LINE_NUMS + 2); it++)
    {
        fprintf(fp, ",%5lu", it);
    }
    fprintf(fp, "\n");

    for (set_size = 1; set_size <= L2_LINE_NUMS; set_size++)
    {
        if(pgcd(L2_LINE_NUMS,set_size) != set_size) continue;
        
        cudaMalloc((void **)&d_input, bytes_input);
        stride =  (L2_LINE_NUMS / set_size);
        printf("-- Kernel Start\n");
        kernel<<<1, 1, (set_size + 2) * sizeof(uint32_t), 0>>>(d_input, d_timer, stride, set_size);
        cuda_check_error();
        cudaMemcpy(h_timer, d_timer, bytes_timer, cudaMemcpyDeviceToHost);
        cudaFree(d_input);

        fprintf(fp, "%8lu,%8lu", stride, set_size);
        for (it = 0; it < (set_size + 2); it++)
        {
            fprintf(fp, ",%5u", h_timer[it]);
        }
        fprintf(fp, "\n");
        printf("-- fin : stride = %lu, set size = %lu\n", stride, set_size);
    }
    fclose(fp);

    cudaFree(d_timer);
    free(h_timer);

    return 0;
}
