#include <cstdint>
#include <pastis/pastis.h>

int main(int argc, char** argv)
{

    if (argc != 3) {
        exit(1);
    }

    uint32_t byte_size = 32 * sizeof(uint32_t);
    uint32_t size_acces = strtoul(argv[1], NULL, 10);
    uint32_t thread_num = strtoul(argv[2], NULL, 10);

    uint32_t* indicies;
    pastisMalloc((void**)&indicies, byte_size);

    uint32_t bank_needed = size_acces / 32;
    uint32_t thread_per_group = (32 / bank_needed);

    for (uint32_t i = 0; i < 32; i++) {
        uint32_t decalage_groupe = i / thread_per_group;
        switch (i % thread_per_group) {
        case 0:
            indicies[i] = 0 + decalage_groupe * bank_needed;
            break;
        case 1:
            indicies[i] = 32 + decalage_groupe * bank_needed;
            break;
        case 3:
            indicies[i] = 64 + decalage_groupe * bank_needed;
            break;
        case 4:
            indicies[i] = 96 + decalage_groupe * bank_needed;
            break;
        case 5:
            indicies[i] = 32 + decalage_groupe * bank_needed + bank_needed;
            break;
        case 6:
            indicies[i] = 0 + decalage_groupe * bank_needed + bank_needed;
            break;
        }
    }

    Tx2Config jetson;

    Device device(jetson);

    Parameter parameters[2];
    parameters[0].kind = PTR;
    parameters[0].val.ptr = (void*)indicies;
    parameters[1].kind = INT32;
    parameters[1].val.i = byte_size;

    dim3 grid_dim(1);
    dim3 bloc_dim(thread_num);

    pastisMemCopy(device, indicies, byte_size);

    simulate(device, "build/main.cubin", grid_dim, bloc_dim, 2, parameters, jetson);

    pastisFree(indicies);

    return 0;
}