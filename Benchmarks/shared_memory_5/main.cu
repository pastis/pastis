#include <cstdint>
#include <cstdio>
#include <cuda.h>
#include <cuda_runtime.h>

__global__ void kernel(uint32_t* indicies, uint32_t* time, uint32_t size_acces)
{
    extern __shared__ int32_t sh[];
    uint32_t start, end;
    uint32_t idx = threadIdx.x;
    uint32_t bank_loc = indicies[idx];
    volatile int32_t* ptr = sh + bank_loc;
    if (size_acces == 32) {
        int32_t tmp;
        start = clock();
        tmp = *ptr;
        end = clock();
        *ptr = tmp;
    } else if (size_acces == 64) {
        int64_t tmp;
        int64_t* ptr64 = (int64_t*)ptr;
        start = clock();
        tmp = *ptr64;
        end = clock();
        *ptr64 = tmp;
    } else if (size_acces == 128) {
        long2 tmp;
        long2* ptr64 = (long2*)ptr;
        start = clock();
        tmp = *ptr64;
        end = clock();
        *ptr64 = tmp;
    }
    time[idx] = end - start;
}

int main(int argc, char** argv)
{
    uint32_t indicies[32], time[32];

    uint32_t *d_idx, *d_time;

    uint32_t size_acces = atoi(argv[1]);
    uint32_t thread_num = atoi(argv[2]);

    for (uint32_t i = 0; i < thread_num; i++) {
        uint32_t bank_needed = size_acces / 32;
        uint32_t thread_per_group = (32 / bank_needed);

        switch (i % thread_per_group) {
        case 0:
            indicies[i] = 0;
            break;
        case 1:
            indicies[i] = 32;
            break;
        case 2:
            indicies[i] = 64;
            break;
        case 3:
            indicies[i] = 96;
            break;
        case 4:
            indicies[i] = 32 + bank_needed;
            break;
        case 5:
            indicies[i] = 0 + bank_needed;
            break;
        default:
            indicies[i] = 0;
            break;
        }
    }

    cudaMalloc((void**)&d_idx, 32 * sizeof(uint32_t));
    cudaMalloc((void**)&d_time, 32 * sizeof(uint32_t));
    cudaMemcpy(d_idx, indicies, sizeof(uint32_t) * 32, cudaMemcpyHostToDevice);

    kernel<<<1, thread_num, sizeof(int32_t) * 128>>>(d_idx, d_time, size_acces);
    cudaMemcpy(time, d_time, 32 * sizeof(uint32_t), cudaMemcpyDeviceToHost);

    cudaError_t err;

    err = cudaGetLastError();
    printf("%s : %s\n", cudaGetErrorName(err), cudaGetErrorString(err));

    for (uint32_t i = 0; i < thread_num; i++) {
        printf("%d \n", time[i]);
    }

    cudaFree(d_idx);
    cudaFree(d_time);
    return 0;
}
