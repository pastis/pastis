#include <cstdint>
#include <cstdio>

#include <pastis/pastis.h>

int main(int argc, char** argv)
{
    uint32_t *indicies, *time;

    if (argc != 3) {
        exit(0);
    }

    uint32_t size_acces = atoi(argv[1]);
    uint32_t thread_num = atoi(argv[2]);

    pastisMalloc((void**)&indicies, sizeof(uint32_t) * 32);
    pastisMalloc((void**)&time, sizeof(uint32_t) * 32);

    for (uint32_t i = 0; i < thread_num; i++) {
        uint32_t bank_needed = size_acces / 32;
        uint32_t thread_per_group = (32 / bank_needed);

        switch (i % thread_per_group) {
        case 0:
            indicies[i] = 0;
            break;
        case 1:
            indicies[i] = 32;
            break;
        case 2:
            indicies[i] = 64;
            break;
        case 3:
            indicies[i] = 96;
            break;
        case 4:
            indicies[i] = 32 + bank_needed;
            break;
        case 5:
            indicies[i] = 0 + bank_needed;
            break;
        default:
            indicies[i] = 0;
            break;
        }
    }

    Tx2Config jetson;
    Device device(jetson);

    pastisMemCopy(device, indicies, 32 * sizeof(uint32_t));

    Parameter parameters[3];
    parameters[0].kind = PTR;
    parameters[0].val.ptr = (void*)indicies;
    parameters[1].kind = PTR;
    parameters[1].val.ptr = (void*)time;
    parameters[2].kind = INT64;
    parameters[2].val.l = size_acces;

    dim3 grid_dim(1);
    dim3 bloc_dim(thread_num);

    simulate(device, "./build/main.cubin", grid_dim, bloc_dim, 3, parameters, jetson);

    for (uint32_t i = 0; i < thread_num; i++) {
        printf("%d \n", time[i]);
    }

    pastisFree(indicies);
    pastisFree(time);
    return 0;
}
