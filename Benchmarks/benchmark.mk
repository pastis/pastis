build/main.cpp4.ii: $(FILE)
	$(DIR_GUARD)
	gcc -E -x c++ -D__CUDACC__ -D__NVCC__ "-I$(CUDA_PATH)/bin/../targets/$(HOST_ARCH)-linux/include" $(CUDACC_DEFINE) $(CUDA_API_DEFINE) -include "cuda_runtime.h" "$(FILE)" -o "build/main.cpp4.ii" 

build/main.cudafe1.cpp: build/main.cpp4.ii $(FILE) build/main.ptx
	$(DIR_GUARD)
	cudafe++ --c++14 --allow_managed --parse_templates --gen_c_file_name "$@" --stub_file_name "build/main.cudafe1.stub.c" --module_id_file_name "build/main.module_id" "build/main.cpp4.ii" 

build/main.o: build/main.cudafe1.cpp build/main.fatbin.c
	$(DIR_GUARD)
	gcc -D__CUDA_ARCH__=$(CUDA_ARCH) -c -x c++  -DCUDA_DOUBLE_MATH_FUNCTIONS "-I$(CUDA_PATH)/bin/../targets/$(HOST_ARCH)-linux/include" "build/main.cudafe1.cpp" -o "$@" -I.

build/a_dlink.cubin: build/main.o
	$(DIR_GUARD)
	nvlink --arch=sm_$(CUDA_SM) --register-link-binaries="build/a_dlink.reg.c" "-L$(CUDA_PATH)/bin/../targets/$(HOST_ARCH)-linux/lib/stubs" "-L$(CUDA_PATH)/bin/../targets/$(HOST_ARCH)-linux/lib" -cpu-arch=$(HOST_ARCH_UPERCASE) "build/main.o"  -lcudadevrt  -o "build/a_dlink.cubin"

build/main.fatbin.c: build/main.ptx build/main.cubin
	$(DIR_GUARD)
	fatbinary -64 --cicc-cmdline="-ftz=1 -prec_div=1 -prec_sqrt=1 -fmad=1" "--image3=kind=elf,sm=$(CUDA_SM),file=build/main.cubin" "--image3=kind=ptx,sm=$(CUDA_SM),file=build/main.ptx" --embedded-fatbin="build/main.fatbin.c"
build/a_dlink.fatbin.c: build/a_dlink.cubin
	$(DIR_GUARD)
	fatbinary -64 --cicc-cmdline="-ftz=1 -prec_div=1 -prec_sqrt=1 -fmad=1" -link "--image3=kind=elf,sm=$(CUDA_SM),file=build/a_dlink.cubin" --embedded-fatbin="build/a_dlink.fatbin.c"

build/a_dlink.o: build/a_dlink.fatbin.c
	gcc -c -x c++ -DFATBINFILE="\"build/a_dlink.fatbin.c\"" -DREGISTERLINKBINARYFILE="\"build/a_dlink.reg.c\"" -I. -D__NV_EXTRA_INITIALIZATION= -D__NV_EXTRA_FINALIZATION= -D__CUDA_INCLUDE_COMPILER_INTERNAL_HEADERS__ "-I$(CUDA_PATH)/bin/../targets/$(HOST_ARCH)-linux/include" $(CUDACC_DEFINE) $(CUDA_API_DEFINE) "$(CUDA_PATH)/bin/crt/link.stub" -o "build/a_dlink.o" 

benchmark:  build/a_dlink.o build/main.o
	g++ -Wl,--start-group "build/a_dlink.o" "build/main.o" "-L$(CUDA_PATH)/bin/../targets/$(HOST_ARCH)-linux/lib/stubs" "-L$(CUDA_PATH)/bin/../targets/$(HOST_ARCH)-linux/lib" -lcudadevrt -lcudart_static -lrt -lpthread -ldl -Wl,--end-group -o "benchmark"

build/pre_main.cubin: build/main.ptx
	$(DIR_GUARD)
	ptxas -arch=sm_$(CUDA_SM) -m64 $(PTX_FLAGS) "$<"  -o "build/pre_main.cubin"

build/main.cpp1.ii: $(FILE)
	$(DIR_GUARD)
	gcc -D__CUDA_ARCH__=$(CUDA_ARCH) -E -x c++ -DCUDA_DOUBLE_MATH_FUNCTIONS -D__CUDACC__ -D__NVCC__ "-I$(CUDA_PATH)/bin/../targets/$(HOST_ARCH)-linux/include" $(CUDA_API_DEFINE) $(CUDACC_DEFINE) -include "cuda_runtime.h" "$(FILE)" -o "build/main.cpp1.ii" 

build/main.ptx: $(FILE) build/main.cpp1.ii
	$(DIR_GUARD)
	cicc --c++14 --orig_src_file_name " $(FILE)" --allow_managed --unsigned_chars -arch compute_$(CUDA_SM) -ftz=0 -prec_div=1 -prec_sqrt=1 -fmad=1 --include_file_name "build/main.fatbin.c" -tused -nvvmir-library "/usr/local/cuda/bin/../nvvm/libdevice/libdevice.10.bc" --gen_module_id_file --module_id_file_name "build/main.module_id" --gen_c_file_name "build/main.cudafe1.c" --stub_file_name "build/main.cudafe1.stub.c" --gen_device_file_name "build/main.cudafe1.gpu"  "build/main.cpp1.ii" -o "$@"

main.sass: build/pre_main.cubin
	maxas.pl -e $< main.sass

ifeq ($(EDITED_SASS_EXISTS), 1)
build/main.cubin: benchmark.sass build/pre_main.cubin
	maxas.pl -p $< build/main_new.sass
	maxas.pl -i -n build/main_new.sass build/pre_main.cubin $@
	nvdisasm -hex $@ > main.cubin.dump
else
build/main.cubin: main.sass
	maxas.pl -p $< build/main_new.sass
	maxas.pl -i -n build/main_new.sass build/pre_main.cubin $@
	nvdisasm -hex $@ > main.cubin.dump
endif

clean_benchmark:
	rm -rf build benchmark

