#include <cuda.h>
#include <cuda_runtime.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

inline __device__ void load_line(volatile uint8_t *input, uint32_t *timers,
                                 uint32_t size) {
  uint32_t start, end, value;
#pragma unroll(1)
  for (uint32_t j = 0; j < size; j++) {
    start = clock();
    value = input[j];
    end = clock();
    timers[j] = end - start;
  }
}

__global__ void kernel(volatile uint8_t *input, uint32_t *timers, uint32_t size,
                       uint32_t nb_warps, uint32_t warp_order) {
  uint32_t warp_id;
  size_t idx = threadIdx.x, i;
  extern __shared__ uint32_t shared_array[];

  __syncthreads();
#pragma unroll(1)
  for (i = 0; i < nb_warps; i++) {
    // Chaque premier thread de chaque warp va charger "size" octets
    warp_id = (warp_order >> ((nb_warps - 1 - i) * 4)) & 0xF;
    if (idx == warp_id * warpSize)
      load_line(input, shared_array + size * i, size);
    __syncthreads();
  }

  if (idx == 0) {
#pragma unroll(1)
    for (i = 0; i < size * nb_warps; i++)
      timers[i] = shared_array[i];
  }
}

int main(int argc, char **argv) {
  if (argc != 2) {
    fprintf(stderr,
            "Error : one parameter are required -> warps order (0x4321)\n");
    exit(1);
  }

  uint8_t *d_data;
  uint32_t *h_timers, *d_timers;
  uint32_t warp_order = strtoul(argv[1], NULL, 16);
  size_t size = 128;
  size_t nb_warps = 4;
  size_t data_bytes = size * sizeof(uint8_t);
  size_t timers_bytes = size * sizeof(uint32_t) * nb_warps;
  size_t i, j;

  h_timers = (uint32_t *)malloc(timers_bytes);
  if (!h_timers) {
    fprintf(stderr, "Error : cannot allocate memory for host timers\n");
    exit(1);
  }

  cudaMalloc((void **)&d_data, data_bytes);
  cudaMalloc((void **)&d_timers, timers_bytes);

  kernel<<<1, size, timers_bytes>>>(d_data, d_timers, size, nb_warps,
                                    warp_order);

  cudaMemcpy(h_timers, d_timers, timers_bytes, cudaMemcpyDeviceToHost);
  cudaFree(d_data);
  cudaFree(d_timers);
  FILE *fp;
  fp = fopen("result.txt", "w+");
  if (fp) {
    fprintf(fp, "%6s", "word");
    for (i = 0; i < nb_warps; i++) {
      fprintf(fp, ",%5s%u", "w",
              (warp_order >> ((nb_warps - 1 - i) * 4)) & 0xF);
    }
    fprintf(fp, "\n");

    for (i = 0; i < size; i++) {
      fprintf(fp, "%6lu", i);
      for (j = 0; j < nb_warps; j++) {
        fprintf(fp, ",%6u", h_timers[size * j + i]);
      }
      fprintf(fp, "\n");
    }
    fclose(fp);
  }
  free(h_timers);

  return 0;
}