#include <cstdint>
#include <cstdio>
#include <cstdlib>

#include <pastis/pastis.h>

int main()
{
    uint8_t* data;
    uint32_t* timers;
    size_t size = 12 * 1024;
    size_t bytes_data = size * sizeof(int8_t);
    size_t bytes_timers = size * sizeof(int32_t);
    size_t index;

    Tx2Config jetson(false,0,FAST);

    Device device(jetson);

    pastisMalloc((void**)&timers, bytes_timers);

    pastisMalloc((void**)&data, bytes_data);

    Parameter params[3];
    params[0].kind = PTR;
    params[0].val.ptr = (void*)data;
    params[1].kind = PTR;
    params[1].val.ptr = (void*)timers;
    params[2].kind = INT64;
    params[2].val.l = size;

    dim3 gridDim, blockDim;
    gridDim.x = 1;
    gridDim.y = 1;
    gridDim.z = 1;
    blockDim.x = 1;
    blockDim.y = 1;
    blockDim.z = 1;

    simulate(device, (char*)"./build/main.cubin", gridDim, blockDim, 3, params, jetson);

    FILE* fp;
    fp = fopen("resultats-sim.txt", "w+");
    if (fp) {
        for (index = 0UL; index < size; index++) {
            fprintf(fp, "%6lu\t%6u\n", index, timers[index]);
        }
        fclose(fp);
    }
    
    pastisFree(timers);
    pastisFree(data);

    return 0;
}
