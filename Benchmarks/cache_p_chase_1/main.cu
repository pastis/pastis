#include <cuda.h>
#include <cuda_runtime.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

__global__ void kernel(int8_t *input, uint32_t *timers, size_t size)
{
    size_t index;
    uint32_t start, end,delta, time, v;
    extern __shared__ int8_t *shared_memory;
    void **s_params;
    int8_t *ptr_s_input , *s_input, **param_input, *reg_input;
    int8_t *ptr_input;
    uint32_t *ptr_s_timers, *s_timers,*ptr_timers, **param_timers, *reg_timers;
    size_t *param_size, reg_size;


    s_input = nullptr;
    s_timers = (uint32_t*)(s_input + size);
    s_params = (void **)(s_timers + size);
    param_input = (int8_t**)s_params;
    param_timers = (uint32_t**)(s_params + 1);
    param_size = (size_t *)(param_timers + 2);
    // We write and read parameter onto and from the shared memory
    asm volatile("st.shared.u64 [%0], %1;\n\t" :: "l"(param_input) , "l"(input) : "memory");
    asm volatile("st.shared.u64 [%0], %1;\n\t" :: "l"(param_timers) , "l"(timers) : "memory");
    asm volatile("st.shared.u64 [%0], %1;\n\t" :: "l"(param_size) , "l"(size) : "memory");
    asm volatile("ld.shared.u64 %0, [%1];\n\t" : "=l"(reg_input) : "l"(param_input) : "memory");
    asm volatile("ld.shared.u64 %0, [%1];\n\t" : "=l"(reg_timers) : "l"(param_timers) : "memory");
    asm volatile("ld.shared.u64 %0, [%1];\n\t" : "=l"(reg_size) : "l"(param_size) : "memory");

#pragma unroll(1)
    for (index = 0U; index < reg_size; index++)
    {
        ptr_input = reg_input + index;
        ptr_s_input = s_input + index;
        ptr_s_timers = s_timers + index;
        asm volatile("mov.u32 %0, %%clock;\n\t" : "=r"(start));
        asm volatile("ld.global.nc.u8 %0, [%1];\n\t" : "=r"(v) : "l"(ptr_input) : "memory");
        asm volatile("mov.u32 %0, %%clock;\n\t" : "=r"(end));
        delta = end - start;
        asm volatile("st.shared.u32 [%0], %1;\n\t" :: "l"(ptr_s_timers), "r"(delta) : "memory");
        asm volatile("st.shared.s8 [%0], %1;\n\t" :: "l"(ptr_s_input), "r"(v) : "memory");
    }

#pragma unroll(1)
    for (index = 0U; index < reg_size; index++)
    {
        ptr_timers = reg_timers + index;
        ptr_s_timers = s_timers + index;
        asm volatile("ld.shared.u32 %0, [%1];\n\t" : "=r"(time) : "l"(ptr_s_timers) : "memory");
        asm volatile("st.global.u32 [%0], %1;\n\t" :: "l"(ptr_timers), "r"(time) : "memory");
    }
}

int main()
{
    int8_t *d_data;
    uint32_t *d_timers, *h_timers;
    size_t size = 8 * 1024;
    size_t bytes_data = size * sizeof(int8_t);
    size_t bytes_timers = size * sizeof(int32_t); 
    size_t index;
    h_timers = (uint32_t *)malloc(bytes_timers);
    if (!h_timers)
        exit(1);


    cudaMalloc((void **)&d_data, bytes_data);
    cudaMalloc((void **)&d_timers, bytes_timers);

    kernel<<<1, 1, bytes_timers + bytes_data + 24>>>(d_data, d_timers, size);

    cudaMemcpy(h_timers, d_timers, bytes_timers, cudaMemcpyDeviceToHost);
    cudaFree(d_timers);
    cudaFree(d_data);

    FILE *fp;
    fp = fopen("resultats.txt", "w+");
    if (fp)
    {
        for (index = 0UL; index < size; index++)
        {
            fprintf(fp, "%6lu\t%6u\n", index, h_timers[index]);
        }
        fclose(fp);
    }
    free(h_timers);

    return 0;
}