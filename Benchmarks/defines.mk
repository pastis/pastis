# Benchmark defines

CUDA_MAJOR=10
CUDA_MINOR=2
CUDA_ARCH=620
CUDA_VER_BUILD=162
CUDA_SM=62
CUDA_PATH=/usr/local/cuda

CUDACC_DEFINE=-D__CUDACC_VER_BUILD__=$(CUDA_VER_BUILD) -D__CUDACC_VER_MAJOR__=$(CUDA_MAJOR) -D__CUDACC_VER_MINOR__=$(CUDA_MINOR)
CUDA_API_DEFINE=-D__CUDA_API_VER_MAJOR__=$(CUDA_MAJOR) -D__CUDA_API_VER_MINOR__=$(CUDA_MINOR)

HOST_ARCH=$(shell uname -p)
HOST_ARCH_UPERCASE = $(shell uname -p | tr '[:lower:]' '[:upper:]')
FILE?=main.cu

EDITED_SASS_EXISTS=$(shell [ -e benchmark.sass ] && echo 1 || echo 0 )

DIR_GUARD = @if [ ! -d $(@D) ]; then echo "mkdir $(@D)"; mkdir -p $(@D); fi

# Simulation defines

CXXFLAGS=-I../../Pastis/pastis/include -I../../Pastis/nvidia/include -I../../Pastis/nvidia/src -Og -DPASTIS -Wall -Wextra -g
LIBADD=-L../../Pastis/pastis/build -lpastis -L../../Pastis/nvidia/src -lnvidia -lm

SRC=$(wildcard *.cpp)
OBJ=$(SRC:.cpp=.o)

EXE=simulation 
