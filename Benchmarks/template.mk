include ../defines.mk

all: benchmark simulation

include ../benchmark.mk

include ../simulator.mk

clean: clean_simulation clean_benchmark

dist_clean: clean
	rm -rf main.sass main_new.sass