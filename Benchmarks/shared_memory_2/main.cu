#include <cuda.h>
#include <cuda_runtime.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

__global__ void kernel(uint32_t* banc_locs, int size_acces)
{
    extern __shared__ int32_t sh[64];
    uint32_t idx = threadIdx.x;
    uint32_t bank_loc = banc_locs[idx];
    volatile int32_t *ptr = sh + bank_loc; 

    if (size_acces == 32) {
        int32_t tmp;
        tmp = *ptr;
        *ptr = tmp;
    } else if (size_acces == 64) {
        int64_t tmp;
        int64_t* ptr64 = (int64_t*)ptr;
        tmp = *ptr64;
        *ptr64 = tmp;
    } else if (size_acces == 128) {
        long2 tmp;
        long2* ptr64 = (long2*)ptr;
        tmp = *ptr64;
        *ptr64 = tmp;
    }
}

int main(int argc, char** argv)
{
    uint32_t indicies[32], *d_indicies;

    uint32_t byte_size = 32 * sizeof(int);
    uint32_t size_acces = atoi(argv[1]);

    for (uint32_t i = 0; i < 32; i++) {
        switch (i) {
        case 0:
            indicies[i] = 0;
            break;
        case 1:
            indicies[i]  = 32;
            break;
        case 2:
            indicies[i]  = 0 + (size_acces / 32);
            break;
        case 3:
            indicies[i]  = 32 + (size_acces / 32);
            break;
        }
    }

    cudaMalloc((void**)&d_indicies, byte_size);

    kernel<<<1, 4>>>(d_indicies, size_acces);

    cudaFree(d_indicies);

    return 0;
}