#include <cstdint>
#include <pastis/pastis.h>

int main(int argc, char** argv)
{

    if (argc != 2) {
        exit(1);
    }

    uint32_t byte_size = 32 * sizeof(uint32_t);
    uint32_t size_acces = strtoul(argv[1], NULL, 10);

    uint32_t* indicies;
    pastisMalloc((void**)&indicies, byte_size);

    for (uint32_t i = 0; i < 32; i++) {
        switch (i) {
        case 0:
            indicies[i] = 0;
            break;
        case 1:
            indicies[i] = 32;
            break;
        case 2:
            indicies[i] = 0 + (size_acces / 32);
            break;
        case 3:
            indicies[i] = 32 + (size_acces / 32);
            break;
        }
    }

    Tx2Config jetson;

    Device device(jetson);

    Parameter parameters[2];
    parameters[0].kind = PTR;
    parameters[0].val.ptr = (void*)indicies;
    parameters[1].kind = INT32;
    parameters[1].val.i = byte_size;

    dim3 grid_dim(1);
    dim3 bloc_dim(4);

    pastisMemCopy(device, indicies, byte_size);

    simulate(device, "build/main.cubin", grid_dim, bloc_dim, 2, parameters, jetson);

    pastisFree(indicies);

    return 0;
}