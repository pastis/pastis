#include <cstdint>
#include <cstdio>
#include <pastis/pastis.h>

int main(int argc, char** argv)
{
    if (argc != 4) {
        printf("usage : <prog> mask bits multiplier\n");
        return 1;
    }
    uint32_t mask = strtoul(argv[1], NULL, 16);
    uint32_t bits = strtoul(argv[2], NULL, 10);
    uint32_t mult = strtoul(argv[3], NULL, 10);

    if (bits != 32 && bits != 64 && bits != 128) {
        printf("bits value should be etheir 32, 64 or 128\n");
        return 1;
    }

    uint32_t timer_byte_size = sizeof(uint32_t) * 32;

    uint32_t* d_timer;

    pastisMalloc((void**)&d_timer, timer_byte_size);

    Tx2Config jetson;

    Device device(jetson);

    dim3 grid_dim(1), block_dim(32);

    Parameter parameters[4];
    parameters[0].kind = PTR;
    parameters[0].val.ptr = (void*)d_timer;
    parameters[1].kind = INT32;
    parameters[1].val.i = bits;
    parameters[2].kind = INT32;
    parameters[2].val.i = mask;
    parameters[3].kind = INT32;
    parameters[3].val.i = mult;

    simulate(device, "build/main.cubin", grid_dim, block_dim, 4, parameters, jetson);

    for (int i = 0; i < 32; i++) {
        printf("thread %d : %d\n", i, d_timer[i]);
    }

    pastisFree(d_timer);
    return 0;
}