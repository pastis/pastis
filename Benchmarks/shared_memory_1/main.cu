#include <cuda.h>
#include <cuda_runtime.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

__global__ void kernel(uint32_t* time, uint32_t bits, uint32_t mask, uint32_t mul)
{
    __shared__ int data[1024];
    uint32_t idx, start, end;
    volatile int32_t* ptr;
    idx = threadIdx.x;
    ptr = data + (idx * mul * (bits / 32));

    start = 0;
    end = 2;

    if (mask >> idx & 1) {
        if (bits == 32) {
            int32_t tmp;
            start = clock();
            tmp = *ptr;
            end = clock();
            *ptr = tmp;
        } else if (bits == 64) {
            int64_t tmp;
            int64_t* ptr64 = (int64_t*)ptr;
            start = clock();
            tmp = *ptr64;
            end = clock();
            *ptr64 = tmp;
        } else if (bits == 128) {
            long2 tmp;
            long2* ptr128 = (long2*)ptr;
            start = clock();
            tmp = *ptr128;
            end = clock();
            *ptr128 = tmp;
        }
    }
    time[idx] = end - start;
}

int main(int argc, char** argv)
{
    if (argc != 4) {
        printf("usage : <prog> bits mask multiplier\n");
        return 1;
    }
    uint32_t mask = strtoul(argv[1], NULL, 16);
    uint32_t bits = strtoul(argv[2], NULL, 10);
    uint32_t mult = strtoul(argv[3], NULL, 10);

    if (bits != 32 && bits != 64 && bits != 128) {
        printf("bits value should be etheir 32, 64 or 128\n");
        return 1;
    }

    uint32_t* timer;

    unsigned int timer_byte_size = sizeof(uint32_t) * 32;

    timer = (uint32_t*)malloc(timer_byte_size);
    if (!timer)
        goto free_timer;

    uint32_t* d_timer;

    cudaMalloc((void**)&d_timer, timer_byte_size);

    kernel<<<1, 32>>>(d_timer, bits, mask, mult);

    cudaMemcpy(timer, d_timer, timer_byte_size, cudaMemcpyDeviceToHost);

    cudaFree(d_timer);

    for (int i = 0; i < 32; i++) {
        printf("thread %d : %d\n", i, timer[i]);
    }

free_timer:
    free(timer);

    return 0;
}
